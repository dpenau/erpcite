@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">
    </div>
    <div>
        <h3 class="font-weight-bold">Listado de Materiales <a href="articulo/create">
                <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo Material</button></a><a>
                <button id="exportar_pdf" class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Exportar
                    PDF</button></a>
        </h3>

        {{-- <div class="row"> --}}
            <div class="col-md-4 col-md-offset-4 col-xs-8">
                <input type="hidden" id="texto_busq" class="form-control" value=""
                    placeholder="Buscar Descripcion de materiales...">

            </div>
            {{-- <div class="col-md-4 col-md-offset-4 col-xs-4">
                <button type="button" id="boton_busq" class="bttn-unite bttn-md bttn-primary" name="button">Buscar</button>
            </div>
        </div> --}}
        <div id="tablas">

            <div id="filtros" class="row" style="width:99.5%;margin-top:2%;margin-left:0.28%;margin-button:5%;">
                <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                    <h4>Materias Primas</h4>
                    <div class='custom-control custom-checkbox'>
                        <input type='checkbox' class='custom-control-input' id='all_mp'><label class='custom-control-label'
                            for='all_mp'>Todos</label>
                    </div>
                    <div id="mat" class="" style="height:100px; overflow-y:scroll">
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                    <h4>Insumos</h4>
                    <div class='custom-control custom-checkbox'>
                        <input type='checkbox' class='custom-control-input' id='all_in'><label class='custom-control-label'
                            for='all_in'>Todos</label>
                    </div>
                    <div id="ins" class="" style="height:100px; overflow-y:scroll">
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                    <h4>Suministro</h4>
                    <div class='custom-control custom-checkbox'>
                        <input type='checkbox' class='custom-control-input' id='all_sum'><label class='custom-control-label'
                            for='all_sum'>Todos</label>
                    </div>
                    <div id="sum" class="" style="height:100px; overflow-y:scroll">
                    </div>
                </div>
            </div>

            {{-- <button id="cambiar_busq" class="bttn-unite bttn-md bttn-warning float-left mr-sm-5"
                style="margin:10px">Busqueda por descripcion</button> --}}
        </div>
        {{-- <button id="ocultar" class="bttn-unite bttn-md bttn-warning float-right mr-sm-5"
            style="margin:10px">Ocultar</button> --}}
        <br>
        <div id="buscando" align="center" style="display:none; margin:40px;">
            <div class="alert alert-success" role="alert">
                Obteniendo materiales...
            </div>
        </div>
        <div class="x_content table-responsive">
            <table id="tabla_material" class="display" style=" width:100%">
                <thead>
                    <tr align="center">

                        <th>Codigo</th>
                        <th>Categoria</th>
                        <th>Subcategoria</th>
                        <th>Descripción del Material</th>
                        <th>Stock</th>
                        <th>Costo unitario</th>
                        <th>Unidad de Compra</th>
                        <th>Serie</th>
                        <th>Editar</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody align="center">
                </tbody>
            </table>
        </div>

        <!-- EDITAR MATERIAL/ARTICULO CON TALLAS-->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Editar material por Tallas</h4>
                    </div>
                    <div class="modal-body">
                        {{ Form::Open(['action' => ['MaterialController@update', '1'], 'method' => 'patch']) }}

                        <input type="text" id="cod_mat" style="display:none" name="cod_material">
                        <div class="row">
                            <div class="form-group  col-md-6 col-md-offset-6 col-xs-12" id="serie-container">
                                <label for="total_orden_compra">Serie:</label>
                                <select id="serie" required='required' name="serie" class="custom-select">
                                    <option value="" selected disabled>Serie</option>
                                    @foreach ($serie as $ser)
                                        <option value="{{ $ser->cod_serie }}">{{ $ser->nombre_serie }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group  col-md-6 col-md-offset-6 col-xs-12" id="tallas-pisos">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group  ">
                                            <label for="total_orden_compra">Talla Minima:</label>
                                            <input type="text" onkeypress="return isNumberKey(event)" maxlength="2"
                                                name="talla_inicial" id="t_i" maxlength="2" class="form-control "
                                                disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  ">
                                            <label for="total_orden_compra">Talla Maxima:</label>
                                            <input type="text" onkeypress="return isNumberKey(event)" maxlength="2"
                                                name="talla_final" id="t_f" maxlength="2" class="form-control "
                                                disabled="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total_orden_compra">Descripcion del material:</label>
                            <input type="text" id="descripcion" class="form-control" maxlength="70" name="descripcion">
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <label for="">Categoria--Subcategoria</label>
                                <select name="subcategoria" id="cat" class="custom-select" required>
                                    <option value="" disabled>Escoja uno</option>
                                    @foreach ($subcategorias as $sub)
                                        <option value="{{ $sub->cod_subcategoria }}">
                                            {{ $sub->nom_categoria }}--{{ $sub->nom_subcategoria }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Stock Minimo:</label>
                                <input type="text" class="form-control" maxlength="11" id="stock_min" name="stock_minimo"
                                    onkeypress="return isNumberKey(event)">
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Stock maximo:</label>
                                <input type="text" class="form-control" maxlength="11" id="stock_max" name="stock_maximo"
                                    onkeypress="return isNumberKey(event)">
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Costo Total:</label>
                                <input onkeypress="return isNumberKey(event)" type="text" name="costo_con_igv" maxlength="9"
                                    id="total_costo" required="required" class="form-control ">
                            </div>

                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Costo sin IGV:</label>
                                <input step='any' maxlength="9" type="number" id="costo_sin_igv" required="required"
                                    name="costo_sin_igv" required="required" class="form-control" disabled="true">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="">Unidad de compra</label>
                                <select name="unidad_compra" id="unidad_c" class="custom-select" required>
                                    <option value="" disabled>Escoja uno</option>
                                    @foreach ($unidad_medida as $unid)
                                        <option value="{{ $unid->cod_unidad_medida }}">{{ $unid->descrip_unidad_medida }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Ubicación:</label>
                                <input step='any' type="text" id="ubicacion_material"
                                       name="ubicacion_material" class="form-control">
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="confirmar" class="bttn-unite bttn-md bttn-primary">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                    {{ Form::Close() }}
                </div>
            </div>
        </div>



        <!-- EDITAR MATERIAL/ARTICULO NORMAL -->
        <div class="modal fade" id="myModalNormal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Editar material Normal</h4>
                    </div>
                    <div class="modal-body">
                        {{ Form::Open(['action' => ['MaterialController@update', '1'], 'method' => 'patch']) }}

                        <input type="text" id="cod_mat_normal" style="display:none" name="cod_material_normal">

                        <div class="form-group">
                            <label for="total_orden_compra">Descripcion del material:</label>
                            <input type="text" id="descripcion_normal" class="form-control" maxlength="70"
                                name="descripcion_normal">
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <label for="">Categoria--Subcategoria</label>
                                <select name="subcategoria_normal" id="cat_normal" class="custom-select" required>
                                    <option value="" disabled>Escoja uno</option>
                                    @foreach ($subcategorias as $sub)
                                        <option value="{{ $sub->cod_subcategoria }}">
                                            {{ $sub->nom_categoria }}--{{ $sub->nom_subcategoria }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Stock Minimo:</label>
                                <input type="text" class="form-control" maxlength="11" id="stock_min_normal"
                                    name="stock_minimo_normal" onkeypress="return isNumberKey(event)">
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Stock maximo:</label>
                                <input type="text" class="form-control" maxlength="11" id="stock_max_normal"
                                    name="stock_maximo_normal" onkeypress="return isNumberKey(event)">
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Costo Total:</label>
                                <input onkeypress="return isNumberKey(event)" type="text" name="costo_con_igv_normal"
                                    maxlength="9" id="total_costo_normal" required="required" class="form-control ">
                            </div>

                            <div class="form-group  col-md-6">
                                <label for="total_orden_compra">Costo sin IGV:</label>
                                <input step='any' maxlength="9" type="text" id="costo_sin_igv_normal" required="required"
                                    name="costo_sin_igv_normal" required="required" class="form-control" disabled="true">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="">Unidad de compra</label>
                                <select name="unidad_compra_normal" id="unidad_c_normal" class="custom-select" required>
                                    <option value="" disabled>Escoja uno</option>
                                    @foreach ($unidad_medida as $unid)
                                        <option value="{{ $unid->cod_unidad_medida }}">{{ $unid->descrip_unidad_medida }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Ubicación:</label>
                                <input step='any' type="text" id="ubicacion_material_normal"
                                       name="ubicacion_material_normal" class="form-control">
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="confirmar_normal"
                            class="bttn-unite bttn-md bttn-primary">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                    {{ Form::Close() }}
                </div>
            </div>
        </div>

        <!-- EXPORTAR MATERIAL/ARTICULO -->
        <div class="modal fade" id="exportar_modal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Exportar en PDF</h4>
                    </div>
                    <div class="modal-body">
                        <select id="exportar_subcategoria">
                            <option value="" disabled selected>Seleccione una subcategoria</option>
                            @foreach ($subcategorias as $sub)
                                <option value="{{ $sub->cod_subcategoria }}-{{ $sub->nom_subcategoria }}">
                                    {{ $sub->nom_subcategoria }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button id="exportar_click" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                        <button class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- DESHABILITAR MATERIAL/ARTICULO -->
        <div class="modal fade" id="modal_accion" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titulo"></h4>
                    </div>
                    <div class="modal-body">
                        {{                         Form::Open(['action' => ['MaterialController@destroy', '2'], 'method' => 'delete']) }}
                        <p id="mensaje"></p>
                        <input type="text" id="cod_mat_ac" style="display:none" name="email">
                        <input type="text" id="cod_acc" style="display:none" name="accion">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                    {{ Form::Close() }}
                </div>
            </div>
        </div>
        <!-- DESTRUIR MATERIAL/ARTICULO -->
        <div class="modal fade" id="modal_destroy" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Destruir el material</h4>
                    </div>
                    <div class="modal-body">
                        {{                         Form::Open(['action' => ['MaterialController@eliminar', '2'], 'method' => 'post']) }}
                        <p>Al destruir el material perdera toda infomracion incluyendo sus movimientos en el kardex</p>
                        <div class="form-check">
                            <input type="checkbox" name="todo" class="form-check-input" id="todo" value="t">
                            <label class="form-check-label" for="todo">Destruir toda la serie</label>
                        </div>
                        <input type="text" id="cod_mat_destroy" style="display:none" name="codigo">
                        <input type="text" id="nombre_destroy" style="display:none" name="nombre">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                    {{ Form::Close() }}
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var pb = 1;
                var data = <?php echo $materiales; ?>;
                var lista = [];

                var idSerie = -1;
                let serieObject = null;
                var serieTemp = null;
                var series = <?php echo $serie; ?>;
                var t = $("#tabla_material").DataTable({
                    'columnDefs': [{
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }],
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                    }
                });
                $(document).on("keypress", function(event) {
                    if (event.which == 13) {
                        $("#boton_busq").trigger("click")
                    }
                });


                $('#total_costo').keyup(function() {
                    var aux = $(this).val() / 1.18;

                    $('#costo_sin_igv').val(aux.toFixed(2));
                    var igvaux = $('#total_costo').val() - $('#costo_sin_igv').val();

                });


                $('#total_costo_normal').keyup(function() {
                    var aux = $(this).val() / 1.18;

                    $('#costo_sin_igv_normal').val(aux.toFixed(2));
                    var igvaux = $('#total_costo_normal').val() - $('#costo_sin_igv_normal').val();

                });

                $('#confirmar').on('click', function() {
                    $('#costo_sin_igv').prop('disabled', false);
                    $('#t_i').prop('disabled', false);
                    $('#t_f').prop('disabled', false);
                });

                $('#confirmar_normal').on('click', function() {
                    $('#costo_sin_igv_normal').prop('disabled', false);
                });



                function llenar_tallas(val) {
                    //$("#modal-data").empty();
                    var id = val;
                    var index = buscarSerie(id);
                    serieObject = series[index];

                    $("#t_i").val(serieObject.tallaInicial);
                    $("#t_f").val(serieObject.tallaFinal);
                }

                function buscarSerie(id) {

                    var index = -1;
                    var filteredObj = series.find(function(item, i) {

                        if (item.cod_serie == id) {
                            index = i;
                            return index;
                        }
                    });
                    return index;
                }

                $("#exportar_pdf").click(function() {
                    $("#exportar_modal").modal({
                        show: true
                    });
                });
                $("#exportar_click").click(function() {
                    let valor = $("#exportar_subcategoria").val()
                    console.log("valor",valor);
                    window.open("../Logistica/pdf/materiales/" + valor)
                })
                $("#boton_busq").on('click', function() {
                    if (lista.length == 0) {
                        t.rows().remove().draw();
                        var texto = $("#texto_busq").val().toUpperCase();
                        if (texto != "") {
                            $("#buscando").toggle("fast");
                            $.each(data, function(key, value) {
                                let valor = value.descrip_material.toUpperCase();
                                if (valor.indexOf(texto) != -1) {
                                    if (value.t_moneda == 0) {
                                        var moneda = "soles";
                                    } else {
                                        var moneda = "dolares"
                                    }
                                    if (value.estado_material == 1) {
                                        var boton = '<a class="b_des" id="' + value.cod_material +
                                            '"><button class="bttn-unite bttn-md bttn-danger " ><i class="fas fa-trash-alt"></i></button></a>'
                                        var color = '<div style="width:5px; height:5px"></div>'
                                    } else {
                                        var boton = '<a class="b_act" id="' + value.cod_material +
                                            '"><button class="bttn-unite bttn-md bttn-success " ><i class="fas fa-check-circle"></i></button></a>'
                                        var color =
                                            '<div style="width:20px; height:20px" class="bg-danger text-danger">z</div>'
                                    }
                                    if (value.stock_minimo != null) {
                                        t.row.add([
                                            value.cod_material,
                                            value.nom_categoria,
                                            value.nom_subcategoria,
                                            value.descrip_material,
                                            "min: " + value.stock_minimo + "<br>max: " + value
                                            .stock_maximo,
                                            "S/." + value.costo_con_igv_material.toFixed(2),
                                            value.descrip_unidad_medida,
                                            serietable,
                                            '<a class="openBtn" id="' + value.cod_material +
                                            '"><button class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button></a>',
                                            boton,
                                        ]).draw();
                                    } else {
                                        t.row.add([
                                            value.cod_material,
                                            value.nom_categoria,
                                            value.nom_subcategoria,
                                            value.descrip_material,
                                            "-",
                                            "S/." + value.costo_con_igv_material.toFixed(2),
                                            value.descrip_unidad_medida,
                                            serietable,
                                            '<a class="openBtn" id="' + value.cod_material +
                                            '"><button class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button></a>',
                                            boton,
                                        ]).draw();
                                    }
                                    $("#tabla_material").on('click', 'a.openBtn', function() {
                                        var id = $(this).attr("id");
                                        var index = buscar(id);
                                        if (data[index].cod_serie != null) {
                                            modal_editar_talla(index);
                                        } else {
                                            modal_editar_normal(index);
                                        }

                                    });
                                    $("#tabla_material").on('click', 'a.b_des', function() {
                                        var id = $(this).attr("id");
                                        var index = buscar(id);
                                        modal_accion(index, 0);
                                    });
                                    $("#tabla_material").on('click', 'a.b_act', function() {
                                        var id = $(this).attr("id");
                                        var index = buscar(id);
                                        modal_accion(index, 1);
                                    });
                                    $("#tabla_material").on('click', 'a.b_destroy', function() {
                                        var id = $(this).attr("id");
                                        var index = buscar(id);

                                        modal_destroy(id);
                                    })
                                }
                            })
                            $("#buscando").toggle("fast");
                        }
                    } else {
                        alert("Deseleccionar busqueda por subcategoria")
                        $("#texto_busq").val("");
                    }
                    $("#texto_busq").val("");
                })
                @foreach ($subcategorias as $sub)
                    @if ($sub->cod_categoria == 634)
                        $("#sum").append("<div class='custom-control custom-checkbox'>"
                            +"<input type='checkbox' class='custom-control-input c_su' id='{{ $sub->cod_subcategoria }}'>"
                            +"<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                            +"</div>")
                    @endif
                    @if ($sub->cod_categoria == 306)
                        $("#ins").append("<div class='custom-control custom-checkbox'>"
                            +"<input type='checkbox' class='custom-control-input c_in' id='{{ $sub->cod_subcategoria }}'>"
                            +"<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                            +"</div>")
                    @endif
                    @if ($sub->cod_categoria == 969)
                        $("#mat").append("<div class='custom-control custom-checkbox'>"
                            +"<input type='checkbox' class='custom-control-input c_mp' id='{{ $sub->cod_subcategoria }}'>"
                            +"<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                            +"</div>")
                    @endif
                    $("#{{ $sub->cod_subcategoria }}").off('change');
                    $("#{{ $sub->cod_subcategoria }}").on('change',function(){
                    if(pb!=0)
                    {
                    $("#buscando").toggle("fast");
                    }
                    if($("#texto_busq").val()=="")
                    {
                    if(lista.length==0)
                    {
                    t.rows()
                    .remove()
                    .draw()
                    }
                    var texto=$(this).attr('id');
                    if($(this).is(":checked"))
                    {
                    $.each( data, function( key, value ) {
                    var serietable="-";
                    if(value.cod_serie==null){
                    serietable="-";
                    }else{
                    var index2 = buscarSerie(value.cod_serie);
                    serieTemp = series[index2];
                    if(serieTemp){
                    serietable=serieTemp.tallaInicial+"-"+serieTemp.tallaFinal;
                    }else{
                    serietable='-';
                    }
                    }
                    if(value.t_moneda==0)
                    {
                    var moneda="soles";
                    }
                    else {
                    var moneda="dolares"
                    }
                    if (value.estado_material==1) {
                    var boton='<a class="b_des" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-danger "><i class="fas fa-trash-alt"></i></button></a>'
                    var color='<div style="width:5px; height:5px"></div>'
                    }
                    else {
                    var boton='<a class="b_act" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-success "><i class="fas fa-check-circle"></i></button></a>'
                    var color='<div style="width:20px; height:20px" class="bg-danger text-danger">z</div>'
                    }
                    if(value.cod_subcategoria==texto)
                    {

                    lista.push({cod:value.cod_material,cod_sub:value.cod_subcategoria});

                    if (value.stock_minimo != null) {
                    t.row.add( [
                    value.cod_material,
                    value.nom_categoria,
                    value.nom_subcategoria,
                    value.descrip_material,
                    "min: "+value.stock_minimo+"<br>max: "+value.stock_maximo,
                    "S/."+value.costo_con_igv_material.toFixed(2),
                    value.descrip_unidad_medida,

                    serietable,
                    '<a class="openBtn" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button></a>',
                    boton,
                    ] ).draw();
                    } else {
                    t.row.add( [
                    value.cod_material,
                    value.nom_categoria,
                    value.nom_subcategoria,
                    value.descrip_material,
                    "-",
                    "S/."+value.costo_con_igv_material.toFixed(2),
                    value.descrip_unidad_medida,

                    serietable,
                    '<a class="openBtn" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button></a>',
                    boton,
                    ] ).draw();
                    }

                    $("#tabla_material").on('click','a.openBtn',function(){

                    var id = $(this).attr("id");
                    var index=buscar(id);
                    if(data[index].cod_serie!=null)
                    modal_editar_talla(index);
                    else
                    modal_editar_normal(index);

                    });
                    $("#tabla_material").on('click','a.b_des',function(){
                    var id = $(this).attr("id");
                    var index=buscar(id);
                    modal_accion(index,0);
                    });
                    $("#tabla_material").on('click','a.b_act',function(){
                    var id = $(this).attr("id");
                    var index=buscar(id);
                    modal_accion(index,1);
                    });
                    $("#tabla_material").on('click','a.b_destroy',function(){
                    var id=$(this).attr("id");
                    var index=buscar(id);
                    modal_destroy(index);
                    })
                    }
                    });
                    }
                    else {
                    $.each( lista, function( key, value ) {
                    console.log(lista);
                    console.log(texto)
                    if(value.cod_sub==texto)
                    {
                    t.rows(function(idx,data,node){
                    console.log(data);
                    return data[0]==value.cod;
                    })
                    .remove()
                    .draw()
                    lista=$.grep(lista,function(value){
                    return value.cod_sub!=texto
                    })
                    }

                    });
                    }
                    }
                    else {
                    $(this).prop("checked",false);
                    alert("borre el texto de busqueda por descripcion")
                    }
                    if(pb!=0)
                    {
                    $("#buscando").toggle("fast");
                    }
                    });
                @endforeach
                $("#all_mp").on("change", function() {
                    pb = 0;
                    $("#buscando").toggle("fast");
                    var mp = $(".c_mp");
                    if ($(this).prop("checked") == true) {
                        mp.prop("checked", true);
                        mp.trigger('change');
                    } else {

                        mp.prop("checked", false);
                        mp.trigger('change');
                    }
                    $("#buscando").toggle("fast");
                    pb = 1;
                });
                $("#all_in").on("change", function() {
                    pb = 0;
                    $("#buscando").toggle("fast");
                    var ins = $(".c_in");
                    if ($(this).prop("checked") == true) {
                        ins.prop("checked", true);
                        ins.trigger('change');
                    } else {

                        ins.prop("checked", false);
                        ins.trigger('change');
                    }
                    $("#buscando").toggle("fast");
                    pb = 1;
                });
                $("#all_sum").on("change", function() {
                    pb = 0;
                    $("#buscando").toggle("fast");
                    var sum = $(".c_su");
                    if ($(this).prop("checked") == true) {
                        sum.prop("checked", true);
                        sum.trigger('change');
                    } else {

                        sum.prop("checked", false);
                        sum.trigger('change');
                    }
                    pb = 1;
                    $("#buscando").toggle("fast");
                });
                $("#cambiar_busq").on('click', function() {
                    if ($(this).text() == "Busqueda por subcategoria") {
                        $(this).text("Busqueda por descripcion")
                    } else {
                        $(this).text("Busqueda por subcategoria")

                    }
                    $("#filtros").toggle("fast");
                    $("#busq").toggle("fast");
                })
                $("#ocultar").on('click', function() {
                    if ($(this).text() == "Ocultar") {
                        $(this).text("Mostrar")
                    } else {
                        $(this).text("Ocultar")

                    }
                    $("#tablas").toggle("fast");
                })

                function modal_destroy(index) {
                    $("#cod_mat_destroy").val(data[index].cod_material);
                    $("#nombre_destroy").val(data[index].descrip_material)
                    $("#modal_destroy").modal({
                        show: true
                    })
                }

                function modal_accion(index, acc) {

                    switch (acc) {
                        case 0:
                            $("#titulo").text("Desactivar Material");
                            $("#mensaje").text("¿Está seguro que desea eliminar este material?");
                            break;
                        case 1:
                            $("#titulo").text("Activar Material");
                            $("#mensaje").text("Esta seguro que desa activar el material ?");
                            break;
                        default:
                    }
                    $("#cod_mat_ac").val(data[index].cod_material);
                    $("#cod_acc").val(acc);
                    $("#modal_accion").modal({
                        show: true
                    });
                }

                function modal_editar_talla(index) {

                    var ser = data[index].cod_serie;

                    $("#serie option[value='" + ser + "']").prop("selected", true);
                    llenar_tallas(ser);
                    $("#cod_mat").val(data[index].cod_material);
                    $("#t_i").val(serieObject.tallaInicial);
                    $("#t_f").val(serieObject.tallaFinal);
                    $("#descripcion").val(data[index].descrip_material)
                    var sub = data[index].cod_subcategoria
                    $("#cat option[value='" + sub + "']").prop("selected", true)
                    $("#stock_max").val(data[index].stock_maximo);
                    $("#stock_min").val(data[index].stock_minimo);
                    var u_m = data[index].unidad_medida;
                    $("#unidad_m option[value='" + u_m + "']").prop("selected", true)
                    var u_c = "92970";
                    $("#unidad_c option[value='" + u_c + "']").prop("selected", true);
                    $("#total_costo").val(data[index].costo_con_igv_material);
                    $("#costo_sin_igv").val(data[index].costo_sin_igv_material);
                    $("#ubicacion_material").val(data[index].ubicacion);
                    $("#factor").val(data[index].factor_equivalencia);
                    $("#myModal").modal({
                        show: true
                    });
                }

                function modal_editar_normal(index) {
                    $("#cod_mat_normal").val(data[index].cod_material);

                    $("#descripcion_normal").val(data[index].descrip_material)
                    var sub = data[index].cod_subcategoria
                    $("#cat_normal option[value='" + sub + "']").prop("selected", true)
                    $("#stock_max_normal").val(data[index].stock_maximo);
                    $("#stock_min_normal").val(data[index].stock_minimo);
                    var u_m = data[index].unidad_medida;
                    $("#unidad_m_normal option[value='" + u_m + "']").prop("selected", true)
                    var u_c = data[index].unidad_compra;
                    $("#unidad_c_normal option[value='" + u_c + "']").prop("selected", true);
                    $("#total_costo_normal").val(data[index].costo_con_igv_material);
                    $("#costo_sin_igv_normal").val(data[index].costo_sin_igv_material);
                    $("#ubicacion_material_normal").val(data[index].ubicacion);
                    $("#factor_normal").val(data[index].factor_equivalencia);
                    $("#myModalNormal").modal({
                        show: true
                    });
                }

                function buscar(id) {
                    var index = -1;
                    var filteredObj = data.find(function(item, i) {
                        if (item.cod_material == id) {
                            index = i;
                            return index;
                        }
                    });
                    return index;
                }
            });
        </script>
    @endsection
