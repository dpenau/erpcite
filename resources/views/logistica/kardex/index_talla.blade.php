@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">

    </div>
    <div>
        <h3 class="font-weight-bold">Listado de Materiales por Talla en Almacen</h3>
        <div>
            <div>
                <a href="{{ url('logistica/kardex/salida_talla') }}">
                    <button class="bttn-unite bttn-md bttn-primary float-right mr-sm-5 ">Salida de varios Materiales
                    </button>
                </a>
            </div>
        </div>
        <a class="#" id="#" href="#"
           data-target="#modal-reporte-talla" data-toggle="modal">

            <button type="button" class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Historial de Movimientos
            </button>
        </a>
    </div>
    @include('logistica.kardex.modalReporteTalla')
    <div id="tablas">
        <div id="filtros" class="row" style="width:100%;padding:5px">
            <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                <h4>Materias Primas</h4>
                <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' id='all_mp'><label class='custom-control-label'
                                                                                           for='all_mp'>Todos</label>
                </div>
                <div id="mat" class="" style="height:100px; overflow-y:scroll">
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                <h4>Insumos</h4>
                <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' id='all_in'><label class='custom-control-label'
                                                                                           for='all_in'>Todos</label>
                </div>
                <div id="ins" class="" style="height:100px; overflow-y:scroll">
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                <h4>Suministro</h4>
                <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' id='all_sum'><label class='custom-control-label'
                                                                                            for='all_sum'>Todos</label>
                </div>
                <div id="sum" class="" style="height:100px; overflow-y:scroll">
                </div>
            </div>
        </div>
        <div id="busq" style="width:100%;display:none">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" id="texto_busq" class="form-control" value=""
                           placeholder="Buscar Descripcion de material...">
                </div>
                <div class="col-md-2">
                    <button type="button" id="boton_busq" class="bttn-unite bttn-md bttn-primary"
                            name="button">Buscar
                    </button>
                </div>
            </div>
        </div>
        <button id="cambiar_busq" class="bttn-unite bttn-md bttn-warning float-right mr-sm-5" style="margin:10px">
            Busqueda
            por descripcion
        </button>
    </div>
    <button id="ocultar" class="bttn-unite bttn-md bttn-warning float-left mr-sm-5" style="margin:10px">Ocultar</button>

    <br>
    <div id="buscando" align="center" style="display:none; margin:40px;">
        <div class="alert alert-success" role="alert">
            Obteniendo materiales...
        </div>
    </div>
    <br>
    <br>

    @if ($minimo > 0)
        <div style=" margin:40px;">
            <div class="alert alert-warning" role="alert">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" align="center" id="headingThree">
                            <h2 class="mb-0">
                                <button class="btn btn-link col lapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                    Posee {{ $minimo }} materiale(s) por debajo del STOCK MINIMO
                                </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                             data-parent="#accordionExample">
                            <div class="card-body text-black">
                                @foreach ($mat_minimo as $mat)
                                    <div class="material_peligro">
                                        <a style="cursor:pointer">{{ $mat }}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ($maximo > 0)
        <div style=" margin:40px;">
            <div class="alert alert-danger" role="alert">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" align="center" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Posee {{ $maximo }} materiale(s) por encima del STOCK MAXIMO
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                             data-parent="#accordionExample">
                            <div class="card-body text-black">
                                @foreach ($mat_maximo as $mat)
                                    <div class="material_peligro">
                                        <a style="cursor:pointer">{{ $mat }}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="">
        <button id="stock_high_filter" class="badge badge-warning">Stock Alto</button>
        <button id="stock_low_filter" class="badge badge-danger">Stock Bajo</button>
    </div>
    @foreach ($valores as $val)
        <?php $cambio_dolar = $val->valor; ?>
    @endforeach
    <div class="x_content table-responsive">
        <table id="kardex_tabla" class="display">
            <thead>
            <tr>
                <th>Codigo</th>
                <th>Subcategoria</th>
                <th>Descripcion del Material</th>
                <th>Nivel de Stock</th>
                <th>T1</th>
                <th>T2</th>
                <th>T3</th>
                <th>T4</th>
                <th>T5</th>
                <th>T6</th>
                <th>T7</th>
                <th>Stock Actual</th>
                <th>Unidad de compra</th>
                <th>Costo Unitario (S/.)</th>
                <th>Costo Total (S/.)</th>
                <th>Almacén - Ubicación</th>
                <th>Salida</th>
                <th>Devolucion</th>
            </tr>
            </thead>
            <tbody>


            </tbody>
            <tfoot>
            <th colspan="14">Total en Almacen sin IGV: <BR>
                Total en Almacen con IGV:
            </th>
            <th colspan="2" id="totales"></th>
            <th>-</th>
            <th>-</th>
            </tfoot>
        </table>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
         id="modal-reporte-actual">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <a data-target="#modal-reporte-subcategoria" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-primary">Subcategoria</button>
                        </a>
                    </div>
                    <div class="form-group ">
                        <a data-target="#modal-reporte-almacen" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-primary">Almacen</button>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
         id="modal-reporte-almacen">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex: Almacen</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Almacenes:</label>
                        <div class="col-md-6">
                            <select required id="almacen_actual" name="accion" class="custom-select">
                                <option value="" selected disabled>Almacen</option>
                                @foreach ($almacen_normal as $al)
                                    <option value="{{ $al->cod_almacen }}">{{ $al->nom_almacen }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a target="_blank" id="reporte_almacen">
                        <button class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    </a>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
         id="modal-reporte-subcategoria">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex: Subcategoria</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
                        <div class="col-md-6">
                            <select required id="subcategoria_actual" name="accion" class="custom-select">
                                <option value="" selected disabled>Subcategoria</option>
                                @foreach ($subcategoria as $ar)
                                    <option value="{{ $ar->cod_subcategoria }}">{{ $ar->nom_subcategoria }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a target="_blank" id="reporte_actual">
                        <button class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    </a>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
         id="modal-reporte-actual">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
                        <div class="col-md-6">
                            <select required id="subcategoria_actual" name="accion" class="custom-select">
                                <option value="" selected disabled>Subcategoria</option>
                                @foreach ($subcategoria as $ar)
                                    <option value="{{ $ar->cod_subcategoria }}">{{ $ar->nom_subcategoria }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a target="_blank" id="reporte_actual">
                        <button class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    </a>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal_salida" role="dialog">
        <div class="modal-dialog modal-lg" style="width:120%;">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title">Salida de materiales</h4>
                </div>
                <div class="modal-body">
                    {{ Form::Open(['action' => ['KardexMatController@update', '2'], 'method' => 'PATCH']) }}
                    <div class="form-group row">
                        <div class="row">
                            <label for="name" class="col-md-5 col-form-label text-md-right">Codigo del Material:</label>
                            <div class="col-md-1">
                                <input class="cod_mat" type="text" disabled>
                                <input class="cod_mat" type="text" name="cod_mat_buscar" style="display:none">
                            </div>
                        </div>
                        <div class="row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Area a entregar:</label>
                            <div class="col-md-6">
                                <select id="area_sal" required name="area" class="custom-select">
                                    <option value="" selected disabled>Area</option>
                                    @foreach ($area as $ar)
                                        <option value="{{ $ar->cod_area }}">{{ $ar->descrip_area }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
                        <div class="col-md-6">
                            <textarea id="sub_categoria" type="text" value="" required disabled></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion del
                            material:</label>
                        <div class="col-md-6">
                            <textarea id="descrip_material" type="text" value="" required disabled></textarea>
                        </div>
                    </div>
                    <div class="x_content table-responsive" id="div_tabla_salida">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="costo_con_igv" name="costo_con_igv">
                    <input type="hidden" id="costo_sin_igv" name="costo_sin_igv">
                    <input type="hidden" id="tipoAccion" name="tipoAccion" value="2" class="form-control">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
                {{ Form::Close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_devolucion" role="dialog">
        <div class="modal-dialog modal-lg" style="width:120%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Devolucion de material</h4>
                </div>
                <div class="modal-body">
                    {{Form::Open(['action' => ['KardexMatController@store', '2'], 'method' => 'POST']) }}
                    <div class="form-group row">
                        <label for="name" class="col-md-5 col-form-label text-md-right">Codigo del Material:</label>
                        <div class="col-md-6">
                            <input class="dev_cod_mat" type="text" disabled>
                            <input class="dev_cod_mat" type="text" name="devolucion_codmat" id="devolucion_codmat_id"
                                   style="display:none">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
                        <div class="col-md-6">
                            <textarea id="devolucion_subcategoria" type="text" value="" required disabled></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion del
                            material:</label>
                        <div class="col-md-6">
                            <textarea id="devolucion_descripcion" type="text" value="" disabled></textarea>
                        </div>
                    </div>
                    <div class="x_content table-responsive" id="div_tabla_devolucion">
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" id="devolucion_costo_con_igv" name="devolucion_costo_con_igv">
                    <input type="hidden" id="devolucion_costo_sin_igv" name="devolucion_costo_sin_igv">
                    <input type="hidden" id="tipoAccion" name="tipoAccion" value="2" class="form-control">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
                {{ Form::Close() }}
            </div>
        </div>
    </div>
    <style>
        .button-md {
            border-color: #212121;
            background-color: #B0BEC5;

        }

        .button-mo {
            border-color: #212121;
            background-color: #FFFFFF;
        }

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;

        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;

        }

        #inBox {
            margin-left: 2%;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#collapseThree").collapse('hide')
            $("#collapseTwo").collapse('hide')
            const table_kardex = $('#kardex_tabla').DataTable({
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            var pb = 1;
            var totales = 0;
            var data = <?php echo $kardex_total; ?>;
            var kardex_total_talla = <?php echo $kardex_total_talla; ?>;
            var lista = [];
            let subcat = <?php echo $subcategoria; ?>;
            let items_marked = []
            let filter_applied = false
            const cambio_dolares = "{{ $cambio_dolar }}";
            $("#categoria").change(function () {

                const subcat_listado = subcat.filter(datas => datas.cod_categoria == this.value);

                $('#material').empty();
                $('#checkboxes').empty();
                $('#material').append(` <option value="" selected disabled>Subcategoria</option>`);
                for (let i = 0; i < subcat_listado.length; i++) {

                    $('#checkboxes').append(`<input name='materiales' type="checkbox" value="${subcat_listado[i].cod_subcategoria}">
                                            ${subcat_listado[i].nom_subcategoria}</input><br>
                                    `);
                }


            });
            $("#guardarReporteTalla").on("click", function () {
                let checkboxes = document.querySelectorAll('input[name="materiales"]:checked');
                let values = [];
                checkboxes.forEach((checkbox) => {
                    values.push(checkbox.value);
                    $('#auxiliar').append(` <input name='subcatego_t[]' type='hidden' value="${checkbox.value}">`);
                });

            })
            $("#reporte_actual").on("click", function () {
                var cod = $("#subcategoria_actual").val()
                cod += "-" + $("#subcategoria_actual option:selected").text();
                window.open("../pdf/kardex_actual/" + cod)
            })
            $("#reporte_almacen").on("click", function () {
                var cod = $("#almacen_actual").val()
                cod += "-" + $("#almacen_actual option:selected").text();
                window.open("../pdf/reporte_almacen/" + cod)
            })
            $(document).on("keypress", function (event) {
                if (event.which === 13) {
                    $("#boton_busq").trigger("click")
                }
            })
            $("#boton_busq").on('click', function () {
                const costo = 0;
                const total = 0;
                let totales = 0;
                if (lista.length === 0) {
                    table_kardex.rows()
                        .remove()
                        .draw()
                    var texto = $("#texto_busq").val().toUpperCase();
                    if (texto !== "") {
                        $("#buscando").toggle("fast");
                        $.each(data, function (key, value) {
                            let valor = value.descrip_material.toUpperCase();
                            if (valor.indexOf(texto) !== -1) {
                                totales = totales + total;
                                table_kardex.row.add([
                                    value.cod_material,
                                    value.nom_subcategoria,
                                    value.descrip_material,
                                    "<p>Min: " + value.stock_minimo + "</p><p>Max: " + value
                                        .stock_maximo + "</p>",
                                    value.stock_total,
                                    value.descrip_unidad_medida,
                                    costo,
                                    total.toFixed(2),
                                    "<p>" + value.nom_almacen + " - " + value.lugar_almacenaje + "</p>",
                                    '<a class="salida" id="' + value.cod_material +
                                    '"><button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-sign-out-alt"></i></button></a>',
                                    '<a class="devolucion" id="' + value.cod_material +
                                    '"><button class="bttn-unite bttn-md bttn-primary  "><i class="fas fa-sign-in-alt"></i></button></a>'
                                ]).draw();
                                $("#kardex_tabla").on('click', 'a.salida', function () {
                                    var id = $(this).attr("id");
                                    var index = buscar(id);
                                    modal_salida(index);
                                });
                                $("#kardex_tabla").on('click', 'a.devolucion', function () {
                                    var id = $(this).attr("id");
                                    var index = buscar(id);
                                    modal_devolucion(index);
                                });
                                let igv = totales * 1.18
                                $("#totales").html("S/. " + totales.toFixed(2) + " <br>S/. " + igv
                                    .toFixed(2))
                            }

                        })
                        $("#buscando").toggle("fast");
                    } else {
                        $("#totales").html("S/. " + 0 + " <br>S/. " + 0)
                    }
                } else {
                    alert("Deseleccionar busqueda por subcategoria")
                    $("#texto_busq").val("");
                }
            })

            @foreach ($subcategoria as $sub)
            @if ($sub->cod_categoria == 634)
            $("#sum").append("<div class='custom-control custom-checkbox'>"
                + "<input name='checks[]' type='checkbox' class='custom-control-input c_su' id='{{ $sub->cod_subcategoria }}'>"
                + "<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                + "</div>")
            @endif
            @if ($sub->cod_categoria == 306)
            $("#ins").append("<div class='custom-control custom-checkbox'>"
                + "<input name='checks[]' type='checkbox' class='custom-control-input c_in' id='{{ $sub->cod_subcategoria }}'>"
                + "<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                + "</div>")
            @endif
            @if ($sub->cod_categoria == 969)
            $("#mat").append("<div class='custom-control custom-checkbox'>"
                + "<input name='checks[]' type='checkbox' class='custom-control-input c_mp' id='{{ $sub->cod_subcategoria }}'>"
                + "<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                + "</div>")
            @endif
            $("#{{ $sub->cod_subcategoria }}").off('change');


            $("#{{ $sub->cod_subcategoria }}").on('change', function () {
                if (pb !== 0) {
                    $("#buscando").toggle("fast");
                }
                if (filter_applied) {
                    table_kardex.rows().remove().draw()
                    filter_applied = false
                }
                if ($("#texto_busq").val() === "") {
                    if (lista.length === 0) {
                        table_kardex.rows().remove().draw()
                        totales = 0;
                    }
                    const texto = $(this).attr('id');
                    if ($(this).is(":checked")) {
                        const total = 10;
                        $.each(data, function (key, value) {
                            let cont;
                            let j;
                            let part1;
                            let part2 = "";
                            let part3;
                            if (value.cod_subcategoria === texto) {
                                totales = totales + value.costo_total_con_igv;
                                lista.push({
                                    cod: value.cod_material,
                                    cod_sub: value.cod_subcategoria,
                                    cod_total: total
                                });
                                items_marked.push(value.cod_subcategoria)
                                if (value.stock_minimo == null) {
                                    part1 = "<tr>" +
                                        "<td>" + value.cod_material + "</td>" +
                                        "<td>" + value.nom_subcategoria + "</td>" +
                                        "<td>" + value.descrip_material + "</td>" +
                                        "<td> - </td>";
                                    cont = 0;
                                    kardex_total_talla.find(function (itemTalla, j) {
                                        if (itemTalla.cod_kardex === value.id_kardex) {
                                            if(itemTalla.cantidad === 0) {

                                                part2 = part2 + "<td>T" + (itemTalla.talla) + "<br><div style='color: #C70039'>" + itemTalla.cantidad + "</div></td>";
                                                cont++;
                                            } else {
                                                part2 = part2 + "<td>T" + (itemTalla.talla) + "<br>" + itemTalla.cantidad + "</td>";
                                                cont++;
                                            }
                                        }
                                    });
                                    for (j = 0; j < (7 - cont); j++) {
                                        part2 = part2 + "<td></td>";
                                    }
                                    let warehouse_location
                                    if (value.ubicacion === null) {
                                        warehouse_location = ""
                                    } else {
                                        warehouse_location = " - " + value.ubicacion
                                    }
                                    part3 = "<td>" + value.stock_actual + "</td>" +
                                        "<td>" + value.descrip_unidad_compra + "</td>" +
                                        "<td>S/. " + value.costo_con_igv_material.toFixed(2) + "</td>" +
                                        "<td>S/. " + value.costo_total_con_igv + "</td>" +
                                        "<td>" + value.nom_almacen + warehouse_location + "</td>" +
                                        '<td><a class="salida" id="' + value.cod_material + '"><button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-sign-out-alt"></i></button></a></td>' +
                                        '<td><a class="devolucion" id="' + value.cod_material + '"><button class="bttn-unite bttn-md bttn-primary  "><i class="fas fa-sign-in-alt"></i></button></a></td><tr>';
                                    const tr = $(part1 + part2 + part3);
                                    table_kardex.row.add(tr[0]).draw();
                                } else {
                                    part1 = "<tr>" +
                                        "<td>" + value.cod_material + "</td>" +
                                        "<td>" + value.nom_subcategoria + "</td>" +
                                        "<td>" + value.descrip_material + "</td>" +
                                        "<td>" + value.stock_minimo + "-" + value.stock_maximo + "</td>";
                                    cont = 0;
                                    kardex_total_talla.find(function (itemTalla, j) {
                                        if (itemTalla.cod_kardex === value.id_kardex) {
                                            if(itemTalla.cantidad === 0) {
                                                part2 = part2 + "<td>T" + (itemTalla.talla) + "<br><div style='color: #C70039'>" + itemTalla.cantidad + "</div></td>";
                                                cont++;
                                            } else {
                                                part2 = part2 + "<td>T" + (itemTalla.talla) + "<br>" + itemTalla.cantidad + "</td>";
                                                cont++;
                                            }
                                        }
                                    });
                                    for (j = 0; j < (7 - cont); j++) {
                                        part2 = part2 + "<td></td>";
                                    }
                                    let warehouse_location
                                    if (value.ubicacion === null) {
                                        warehouse_location = ""
                                    } else {
                                        warehouse_location = " - " + value.ubicacion
                                    }
                                    let stock_actual = value.stock_actual
                                    if (value.stock_actual === 0) {
                                        stock_actual = '<div style="color: #C70039">' + value.stock_actual + '</div>'
                                    }
                                    part3 = "<td>" + stock_actual + "</td>" +
                                        "<td>" + value.descrip_unidad_compra + "</td>" +
                                        "<td>S/. " + value.costo_con_igv_material.toFixed(2) + "</td>" +
                                        "<td>S/. " + value.costo_total_con_igv + "</td>" +
                                        "<td>" + value.nom_almacen + warehouse_location + "</td>" +
                                        '<td><a class="salida" id="' + value.cod_material + '"><button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-sign-out-alt"></i></button></a></td>' +
                                        '<td><a class="devolucion" id="' + value.cod_material + '"><button class="bttn-unite bttn-md bttn-primary  "><i class="fas fa-sign-in-alt"></i></button></a></td><tr>';
                                    const tr = $(part1 + part2 + part3);
                                    table_kardex.row.add(tr[0]).draw();
                                }
                            }
                        });
                        let igv = totales * 1.18
                        $("#totales").html("S/. " + totales.toFixed(2) + " <br>S/. " + igv.toFixed(2))
                    } else {
                        $.each(lista, function (key, value) {
                            if (value.cod_sub === texto) {
                                totales = totales - value.cod_total
                                table_kardex.rows(function (idx, data, node) {
                                    return data[0] === value.cod;
                                }).remove().draw()
                                lista = $.grep(lista, function (value) {
                                    return value.cod_sub !== texto
                                })
                                const idx = items_marked.indexOf(value.cod_sub)
                                if (idx > -1) {
                                    items_marked.splice(idx, 1)
                                }
                            }
                        });
                        let igv = totales * 1.18
                        $("#totales").html("S/. " + totales.toFixed(2) + " <br>S/. " + igv.toFixed(2))
                    }
                } else {
                    $(this).prop("checked", false);
                    alert("Desactive busqueda por descripcion")
                }

                if (pb !== 0) {
                    $("#buscando").toggle("fast");
                }
            });
            @endforeach

            $("#kardex_tabla").on('click', 'a.salida', function () {
                var id = $(this).attr("id");
                var index = buscar(id);
                modal_salida(index);
            });
            $("#kardex_tabla").on('click', 'a.devolucion', function () {
                var id = $(this).attr("id");
                var index = buscar(id);
                modal_devolucion(index);
            });

            $("#all_mp").on("change", function () {
                pb = 0;
                $("#buscando").toggle("fast");
                var mp = $(".c_mp");
                if ($(this).prop("checked") === true) {
                    mp.prop("checked", true);
                    mp.trigger('change');
                } else {
                    mp.prop("checked", false);
                    mp.trigger('change');
                }
                $("#buscando").toggle("fast");
                pb = 1;
            });
            $("#all_in").on("change", function () {
                pb = 0;
                $("#buscando").toggle("fast");
                var ins = $(".c_in");
                if ($(this).prop("checked") === true) {
                    ins.prop("checked", true);
                    ins.trigger('change');
                } else {
                    ins.prop("checked", false);
                    ins.trigger('change');
                }
                $("#buscando").toggle("fast");
                pb = 1;
            });
            $("#all_sum").on("change", function () {
                pb = 0;
                $("#buscando").toggle("fast");
                var sum = $(".c_su");
                if ($(this).prop("checked") === true) {
                    sum.prop("checked", true);
                    sum.trigger('change');
                } else {
                    sum.prop("checked", false);
                    sum.trigger('change');
                }
                pb = 1;
                $("#buscando").toggle("fast");
            });
            $("#ocultar").on('click', function () {
                if ($(this).text() === "Ocultar") {
                    $(this).text("Mostrar")
                } else {
                    $(this).text("Ocultar")
                }
                $("#tablas").toggle("fast");
            })
            $("#cambiar_busq").on('click', function () {
                if ($(this).text() === "Busqueda por subcategoria") {
                    $(this).text("Busqueda por descripcion")
                } else {
                    $(this).text("Busqueda por subcategoria")

                }
                $("#filtros").toggle("fast");
                $("#busq").toggle("fast");
            })

            //Diego
            //Draw Function
            const drawRow = (arr_to_draw, type, cont) => {
                arr_to_draw.forEach(value => {
                    let actual_value_marker
                    if (type === 0) {
                        actual_value_marker = '<div style="width:20px; height:20px; display: inline" class="bg-warning text-warning">' + cont + '</div>'
                    } else {
                        actual_value_marker = '<div style="width:20px; height:20px; display: inline" class="bg-danger text-danger">' + cont + '</div>'
                    }
                    cont += 1
                    let min_max_stock
                    if (value.stock_minimo !== null) {
                        min_max_stock = "<p>Min: " + value.stock_minimo + "</p><p>Max: " + value.stock_maximo + "</p>"
                    } else {
                        min_max_stock = "-"
                    }
                    const part1 = "<tr>" +
                        "<td>" + actual_value_marker + " " + value.cod_material + "</td>" +
                        "<td>" + value.nom_subcategoria + "</td>" +
                        "<td>" + value.descrip_material + "</td>" +
                        "<td>" + min_max_stock + "</td>";
                    let newCont = 0;
                    let part2 = ""
                    kardex_total_talla.find(function (itemTalla, j) {
                        if (itemTalla.cod_kardex === value.id_kardex) {
                            if(itemTalla.cantidad === 0) {
                                part2 = part2 + "<td>T" + (itemTalla.talla) + "<br><div style='color: #C70039'>" + itemTalla.cantidad + "</div></td>";
                                newCont++;
                            } else {
                                part2 = part2 + "<td>T" + (itemTalla.talla) + "<br>" + itemTalla.cantidad + "</td>";
                                newCont++;
                            }
                        }
                    });
                    for (let j = 0; j < (7 - newCont); j++) {
                        part2 = part2 + "<td></td>";
                    }
                    let stock_actual = value.stock_actual
                    if (value.stock_actual === 0) {
                        stock_actual = '<div style="color: #C70039">' + value.stock_actual + '</div>'
                    }
                    let warehouse_location
                    if (value.ubicacion === null) {
                        warehouse_location = ""
                    } else {
                        warehouse_location = " - " + value.ubicacion
                    }
                    const part3 = "<td>" + stock_actual + "</td>" +
                        "<td>" + value.descrip_unidad_compra + "</td>" +
                        "<td>S/. " + value.costo_con_igv_material.toFixed(2) + "</td>" +
                        "<td>S/. " + value.costo_total_con_igv + "</td>" +
                        "<td>" + value.nom_almacen + warehouse_location + "</td>" +
                        '<td><a class="salida" id="' + value.cod_material + '"><button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-sign-out-alt"></i></button></a></td>' +
                        '<td><a class="devolucion" id="' + value.cod_material + '"><button class="bttn-unite bttn-md bttn-primary  "><i class="fas fa-sign-in-alt"></i></button></a></td><tr>';
                    const tr = $(part1 + part2 + part3);
                    table_kardex.row.add(tr[0]).draw();
                    $("#kardex_tabla").on('click', 'a.salida', function () {
                        const id = $(this).attr("id");
                        const index = buscar(id);
                        modal_salida(index);
                    });
                    $("#kardex_tabla").on('click', 'a.devolucion', function () {
                        const id = $(this).attr("id");
                        const index = buscar(id);
                        modal_devolucion(index);
                    });
                    let igv = totales * 1.18
                    $("#totales").html("S/. " + totales.toFixed(2))
                })
            }

            //Filter functionality Diego
            $("#stock_high_filter").on('click', function () {
                filter_applied = true
                table_kardex.rows().remove().draw()
                const arr_higher = data.filter(value => value.stock_actual >= value.stock_maximo && items_marked.includes(value.cod_subcategoria) && value.stock_maximo !== null)
                let cont = 10
                drawRow(arr_higher, 0, cont)
            })
            $("#stock_low_filter").on('click', function () {
                filter_applied = true
                table_kardex.rows().remove().draw()
                const arr_lower = data.filter(value => value.stock_actual <= value.stock_minimo && items_marked.includes(value.cod_subcategoria) && value.stock_minimo !== null)
                let cont = 10
                drawRow(arr_lower, 2, cont)
            })

            //End Diego

            function buscar(id) {
                let index = -1;
                data.find(function (item, i) {
                    if (item.cod_material === id) {
                        index = i;
                        return index;
                    }
                });
                return index;
            }

            function modal_salida(index) {

                let partA = "";
                let partB2 = "";
                let partB = "<tr><th>Stock Actual</th>";
                let partC = "<tr><th>Cantidad de Salida</th>";
                $("#div_tabla_salida").empty();
                $("#div_tabla_salida").append(
                    '<table id="tabla_salida" class="display" width="90%"><thead><tr id="cabezera_tabla_salida"><th>Tallas</th></tr></thead><tbody></tbody></table>'
                );

                let cont = 0;
                kardex_total_talla.find(function (itemTalla, j) {
                    if (itemTalla.cod_kardex === data[index].id_kardex) {
                        partA = partA + "<th>T" + itemTalla.talla + "</th>";
                        partB = partB + "<td>" + itemTalla.cantidad + "</td>";
                        partC = partC + "<td><input name='salida_talla[]' type='hidden' value='" + itemTalla.talla + "'><input name='salida_tallas[]' id='salida-tallas" + cont + "' type='number' step='0.01' value='0' min='0' max='" + itemTalla.cantidad + "' style='width: 60px;'></td>";
                        partB2 = "<td>" + itemTalla.stock_actual + "</td><td>Par</td>";
                        cont++;
                    }
                });

                partA = partA + "<th>Total</th><th>Unidad de compra</th>";
                partB = partB + partB2 + "</tr>";
                partC = partC +
                    "<td><input name='salida_total' id='salida_total' type='hidden' value='0'><input name='salida_total_vista' id='salida_total_vista' type='number' value='0'  style='width: 60px;' disabled></td><td>Par</td></tr>";
                $("#cabezera_tabla_salida").append(partA);
                const table_kardex_salida = $('#tabla_salida').DataTable({
                    'columnDefs': [
                        {
                            targets: '_all',
                            orderable: false,
                            checkboxes: {
                                selectRow: true
                            }
                        },
                    ],
                    order: [[0, 'desc']],
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                    }
                });
                const tr2 = $(partB);
                const tr3 = $(partC);
                table_kardex_salida.row.add(tr2[0]).draw();
                table_kardex_salida.row.add(tr3[0]).draw();
                $(".cod_mat").val(data[index].cod_material)
                $("#descrip_material").val(data[index].descrip_material)
                $(".stock_tot").val(data[index].stock_total)
                $("#sub_categoria").val(data[index].nom_subcategoria)
                $("#unidad_compra").val(data[index].descrip_unidad_medida)
                $("#costo_con_igv").val(data[index].costo_con_igv_material);
                $("#costo_sin_igv").val(data[index].costo_sin_igv_material);
                $("#modal_salida").modal({
                    show: true
                })

                for (let i = 0; i < cont; i++) {
                    $('#salida-tallas' + i).change(function () {
                        var suma_salida_total = 0;
                        $('input[name^="salida_tallas"]').each(function () {
                            suma_salida_total = suma_salida_total + 1 * $(this).val();
                            $('#salida_total_vista').val(suma_salida_total);
                            $('#salida_total').val(suma_salida_total);
                        });
                    });
                }
            }

            function modal_devolucion(index) {
                var partA = "";
                var partB2 = "";
                var partB = "<tr><th>Stock Actual</th>";
                var partC = "<tr><th>Cantidad de Devolucion</th>";
                $("#div_tabla_devolucion").empty();
                $("#div_tabla_devolucion").append(
                    '<table id="tabla_devolucion" class="display" width="90%"><thead><tr id="cabezera_tabla_devolucion"><th>Tallas</th></tr></thead><tbody></tbody></table>'
                );

                let cont = 0;
                kardex_total_talla.find(function (itemTalla, j) {

                    if (itemTalla.cod_kardex === data[index].id_kardex) {
                        partA = partA + "<th>T" + itemTalla.talla + "</th>";
                        partB = partB + "<td>" + itemTalla.cantidad + "</td>";
                        partC = partC + "<td><input name='devolucion_talla[]' type='hidden' value='" + itemTalla.talla + "'><input name='devolucion_tallas[]' id='devolucion-tallas" + cont + "' type='number' min='0' step='0.01' value='0' style='width: 60px;'></td>";
                        partB2 = "<td>" + itemTalla.stock_actual + "</td><td>Par</td>";
                        cont++;
                    }
                });

                partA = partA + "<th>Total</th><th>Unidad de compra</th>";
                partB = partB + partB2 + "</tr>";
                partC = partC +
                    "<td><input name='devolucion_total' id='devolucion_total' type='hidden' value='0'><input name='devolucion_total_vista' id='devolucion_total_vista' type='number' step='0.01' value='0' style='width: 60px;' disabled></td><td>Par</td></tr>";

                $("#cabezera_tabla_devolucion").append(partA);
                const table_kardex_devolucion = $('#tabla_devolucion').DataTable({
                    'columnDefs': [
                        {
                            targets: '_all',
                            orderable: false,
                            checkboxes: {
                                selectRow: true
                            }
                        },
                    ],
                    order: [[0, 'desc']],
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                    }
                });
                const tr2 = $(partB);
                const tr3 = $(partC);
                table_kardex_devolucion.row.add(tr2[0]).draw();
                table_kardex_devolucion.row.add(tr3[0]).draw();


                $(".dev_cod_mat").val(data[index].cod_material);
                $("#devolucion_subcategoria").val(data[index].cod_material)
                $("#devolucion_descripcion").val(data[index].descrip_material);
                $("#devolucion_costo_con_igv").val(data[index].costo_con_igv_material);
                $("#devolucion_costo_sin_igv").val(data[index].costo_sin_igv_material);
                $("#modal_devolucion").modal({
                    show: true
                });

                for (let i = 0; i < cont; i++) {
                    $('#devolucion-tallas' + i).change(function () {
                        var suma_devolucion_total = 0;
                        $('input[name^="devolucion_tallas"]').each(function () {
                            suma_devolucion_total = suma_devolucion_total + 1 * $(this).val();
                            $('#devolucion_total_vista').val(suma_devolucion_total);
                            $('#devolucion_total').val(suma_devolucion_total)
                        });
                    });
                }
            }
        });
    </script>
@endsection
