<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-desactive-{{$m->codigo_importaciones}}">
{!!Form::open(array('url'=>'logistica/accion_importacion','method'=>'PUT','autocomplete'=>'off'))!!}
      {{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar Gasto {{$m->descripcion_gasto}}</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="codigo" value="{{$m->codigo_importaciones}}">
				<input type="text" style="display:none" name="detalle" value="{{$codigo_detalle}}">
        <input type="text" style="display:none" name="descrip" value="{{$nombre}}">
        <input type="text" style="display:none" name="estado" value="0">
        <p>Esta seguro que desea desactivar el gasto?</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
