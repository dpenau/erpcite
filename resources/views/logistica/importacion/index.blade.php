@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div>
<h3 class="font-weight-bold">Listado de Gastos {{$nombre}}<a href="#" data-target="#agregar" data-toggle="modal">
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo Gasto</button></a>
</h3>
</div>
@include('logistica.importacion.modal_agrega')
<h2>Gastos en Soles (S/.)</h2>
<div class="x_content table-responsive">
  <table id="example" class="display" style=" width:100%">
    <thead>
      <tr>
        <th>Descripción de Gasto</th>
        <th>Tipo de Moneda</th>
        <th>Importe</th>
        <th>Editar</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
      <?php $total_soles=0;?>
      @foreach($soles as $m)
      @if($m->estado_gasto==0)
      <tr class="bg-danger">
      @else
      <tr>
      @endif

        <td>{{$m->descripcion_gasto}}</td>
        <td>Soles</td>
        <td>S/.{{$m->importe_gasto}}</td>

        <td>
           <a href="" data-target="#modal-edit-{{$m->codigo_importaciones}}" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
        </td>
        <td>
          @if($m->estado_gasto==1)
          <?php $total_soles=$total_soles+$m->importe_gasto?>
           <a href="" data-target="#modal-desactive-{{$m->codigo_importaciones}}" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
          @else
          <a href="" data-target="#modal-active-{{$m->codigo_importaciones}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
          @endif
        </td>
      </tr>
      @include('logistica.importacion.modal_editar')
      @include('logistica.importacion.modal_eliminar')
      @include('logistica.importacion.modal_activar')
      @endforeach
    </tbody>
    <tfoot>
      <th colspan="2">Total:</th>
      <th colspan="3">S/. {{$total_soles}}</th>
    </tfoot>
  </table>
</div>
<h2>Gastos en Dolares ($)</h2>
<div class="x_content table-responsive">
  <table id="example2" class="display" style=" width:100%">
    <thead>
      <tr>
        <th>Descripción de Gasto</th>
        <th>Tipo de Moneda</th>
        <th>Importe</th>
        <th>Editar</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
      <?php $total_dolares=0;?>
      @foreach($dolares as $m)
      @if($m->estado_gasto==0)
      <tr class="bg-danger">
      @else
      <tr>
      @endif
        <td>{{$m->descripcion_gasto}}</td>
        <td>Dolares</td>
        <td>${{$m->importe_gasto}}</td>

        <td>
           <a href="" data-target="#modal-edit-{{$m->codigo_importaciones}}" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
        </td>
        <td>
          @if($m->estado_gasto==1)
          <?php $total_dolares=$total_dolares+$m->importe_gasto?>
           <a href="" data-target="#modal-desactive-{{$m->codigo_importaciones}}" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
          @else
          <a href="" data-target="#modal-active-{{$m->codigo_importaciones}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
          @endif
        </td>
      </tr>
      @include('logistica.importacion.modal_editar')
      @include('logistica.importacion.modal_eliminar')
      @include('logistica.importacion.modal_activar')
      @endforeach
    </tbody>
    <tfoot>
      <th colspan="2">Total:</th>
      <th colspan="3">$ {{$total_dolares}}</th>
    </tfoot>
  </table>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready( function () {

} );
</script>
@endsection
