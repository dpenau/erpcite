@extends ('layouts.admin')
@section('contenido')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nueva Orden de Compra- Materiales por Tallas</h2>
                    <div class="clearfix"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['url' => 'logistica/orden_compras/store/', 'method' => 'POST', 'autocomplete' => 'off']) !!}

                {{ Form::token() }}
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="fecha">
                            <label for="total_orden_compra">Fecha:</label>
                             <input type="date" required="required" id="fecha" name="fecha1" value="{{$fechaActual->format('Y-m-d')}}" disabled>
                             <input type="hidden" name="fecha2" value="{{$fechaActual->format('Y-m-d:h-m-s')}}">
                        </label>
                    </div>

                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12">
                      <label for="total_orden_compra">Orden de Compra Nª:</label>
                      <input type="text" required="required" id="ordenCompraN" name="codigo_orden" value="{{$ordenCompraN}}" disabled >
                    </div>
                </div>
                <div class="row" style="margin-bottom:-1%">
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12" >
                      <label for="total_orden_compra" >Proveedor:</label>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                      <label for="total_orden_compra">Tipo de Compra:</label>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12"  id="dias_credito_div">
                    <label for="total_orden_compra">Días de crédito:</label>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                    </div>
                </div>
                <div class="row" style="margin-bottom:-1%">
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                        <select required id="proveedor" name="proveedor" class="custom-select" placeholder="Seleccione proveedor">
                            <option value="" disabled selected>Proveedor</option>
                            @foreach ($proveedores as $item)
                              <option value="{{ $item->RUC_proveedor}}" >{{ $item->nom_proveedor}}</option>
                             @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                        <select required id="tipo_compra" name="tipo_compra" class="custom-select">
                            <option value="" disabled selected>Tipo de Compra</option>
                            <option value="1">Credito</option>
                            <option value="0">Contado</option>
                        </select>
                    </div>

                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12"  id="dias_credito_div2">
                      <input type="number"  id="dias_credito" name="dias_credito" >
                    </div>


                    <!--MODAL-->
                    <div class="form-group  col-md-3 col-md-offset-3 col-xs-12" id="modal-data" >
                    <button class="bttn-unite bttn-md bttn-warning" type="button" data-target="#modal-create" data-toggle="modal" data-pro="$aux" id="modalCreate" >Agregar Materiales</button>
                    </div>
                </div>
                <div class="x_content table-responsive">
                  <h3 class="font-weight-bold">Listado de Materiales
                  </h3>


                  <table id="customersTABLA"  class="display">
                      <thead>
                          <tr>
                              <th>Cod. Material</th>
                              <th>Descripción</th>
                              <th>T1</th>
                              <th>T2</th>
                              <th>T3</th>
                              <th>T4</th>
                              <th>T5</th>
                              <th>T6</th>
                              <th>T7</th>
                              <th>Cantidad Total</th>
                              <th>Unidad Compra</th>
                              <th>Valor Unitario</th>
                              <th>Valor Total</th>
                              <th>Eliminar</th>
                          </tr>
                      </thead>
                      <tbody id="materialSelec2">
                      </tbody>
                  </table>
              </div>


                <div class="row" style='margin-top : 5%;'>
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                            <label   style='width : 130%;'>
                                <label  style='margin-right : 0.5%;'>Sub Total:</label>
                                    S/.<input type="number" step='any'  style='width : 55%;' id="subTotalval" name="subTotalval" disabled="true">

                            </label>
                        </div>
                </div>

                <div class="row" style='margin-top : -0.8%;'>
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                            <label   style='width : 130%;'>
                                <label  style='margin-right : 1%;'>IGV (18%):</label>
                                    S/.<input type="number" step='any'  style='width : 55%;' id="igvSubtotal" name="igvSubtotal" disabled="true">
                            </label>
                        </div>
                </div>
                <!-- CANTIDAD TOTAL Y FALTANTE-->
                <input type="hidden" step='any'  style='width : 55%;' id="cantidad_total_controller" name="cantidad_total_controller" >
                <input type="hidden" step='any'  style='width : 55%;' id="cantidad_faltante_controller" name="cantidad_faltante_controller" >
                  <!-- -->
                <div class="row" style='margin-top : -0.8%;'>
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                            <label   style='width : 130%;'>
                                <label  style='margin-right : 7.5%;'>Total:</label>
                                    S/.<input type="number" step='any'  style='width : 55%;' id="idTotal" name="idTotal"  disabled="true">
                            </label>
                        </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="text" id="obervaciones" name="observaciones" maxlength="2000" class="form-control">
                        <input type="hidden" id="tipoDetalle" name="tipoDetalle" value="0" class="form-control">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <a href="{{ url('logistica/orden_compras') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
                        <button type="submit" id="guardar"
                            class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" value="GuardarTalla">Guardar</button>
                    </div>
                </div>


                {!! Form::close() !!}


                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script >



                </script>
                <script>

                    $(document).ready(function() {
                        if($('#proveedor').val()==null){
                                    alert("ELIJA UN PROVEEDOR PARA PODER AGREGAR MATERIALES");

                        }
                        var t = $('#customersTABLA').DataTable( {
                                    "lengthMenu": [[100, -1], [100, "All"]],
                                    'columnDefs': [
                                    {
                                        'targets': 0,
                                        'checkboxes': {
                                        'selectRow': true
                                        }
                                    }
                                    ],
                                    "language": {
                                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                                    }
                                    });
                        var data =<?php echo $proveedores; ?>;
                        var categoria =<?php echo $categorias; ?>;
                        var cateNomb =<?php echo $categoria; ?>;
                        var materiales =<?php echo $materiales; ?>;
                        var datatemp = [];
                        var costoTotalMat=0;
                        var subTotal=0;
                        var cantidadTot=0;
                        $("#consultar").click(function() {
                            var ruc = $("#ruc").val();
                            $("#consultar").text("Consultando...");
                            $.ajax({
                                url: "consulta/ruc/" + ruc,
                                Type: 'get',
                                dataType: "json",
                                success: function(datos) {
                                    $("#consultar").text("Consultar");
                                    if (datos[0] != "nada") {
                                        $("#razon_social").val(datos[0]);
                                        $("#direccion1").val(datos[1])
                                    } else {
                                        alert("RUC no existente")
                                    }
                                }
                            })
                        });


                                //ELIMINANDO FILA DE MATERIAL EN ORDEN DE COMPRA

                                $('#customersTABLA').on( 'click', 'button', function () {
                                    var indice = datatemp.indexOf($(this).attr('name'));
                                        datatemp.splice(indice,1);
                                        t
                                            .row($(this).parents('tr'))
                                            .remove()
                                            .draw();

                                        var sumaTotal=0;
                                        $('input[name^="costo_total"]').each(function()
                                        {
                                            sumaTotal = sumaTotal + 1*$(this).val();
                                            $("#subTotalval").val(sumaTotal);
                                            $("#idTotal").val((sumaTotal*1.18).toFixed(2));
                                            $("#igvSubtotal").val( ($("#idTotal").val() - $("#subTotalval").val()).toFixed(2));
                                        });
                                        var cantidadT=0;
                                        $('input[name^="cantidad_talla"]').each(function()
                                        {
                                            cantidadT = cantidadT + 1*$(this).val();
                                            $("#cantidad_total_controller").val(cantidadT);
                                        });
                                        console.log(datatemp);
                                } );
                        $('#guardar').on('click', function() {

                            $('#dias_credito_div').show();
                            $('#dias_credito').val(0);
                            $('#ordenCompraN').prop('disabled',false);
                            $('#subTotalval').prop('disabled',false);
                            $('#igvSubtotal').prop('disabled',false);
                            $('#idTotal').prop('disabled',false);


                        });

                        $('#tipo_compra').on('change',function(){
                            var id = $(this).val();

                            if(id==0){

                                $('#dias_credito_div').hide();
                                $('#dias_credito_div2').hide();
                                $('#dias_credito').val(null);
                            }
                            else if(id==1){
                                $('#dias_credito_div').show();
                                $('#dias_credito_div2').show();
                                $('#dias_credito').prop('required', true);
                            }
                            var index=buscar(id);
                            modal_editar(index);
                         });
                        $('#proveedor').on('change',function(){

                            t.rows().remove().draw();
                            datatemp = [];

                            var id = $(this).val();
                            var index=buscar(id);
                            $("#modal-data").empty();
                            modal_editar(index);
                         });
                         $("#categoria").change(function() {
                            let valor = $("#categoria").val();
                            $("#subcategoria").empty();
                            $("#materiales").empty();
                            generar_opciones_subcategoria(valor);
                        });

                        function modal_editar(index)  {

                            datatemp = [];
                            var nombre=data[index].nom_proveedor;
                            var catego=data[index].proveedor_categoria;
                            var subcatego=data[index].RUC_proveedor;

                            var index=buscarCategoria(catego);
                            if(index!=-1){
                              var nombreCategoria=categoria[index];
                            } else{
                                var nombreCategoria="Sin categoria";
                            }
                            var orden = $("#ordenCompraN").attr('value');

                            $("#nombre").val(nombre);
                            $("#catego").val(catego);
                            $("#orden").val(orden);
                            $("#categoria").val(nombreCategoria);
                            $("#modal-data").append("<button class='bttn-unite bttn-md bttn-warning' type='button' data-target='#modal-create' data-toggle='modal' data-pro="+nombre+"  data-cod="+catego+" data-orden="+orden+" data-categoria="+categoria+" id='modalCreate' >Agregar Materiales</button>");
                            $("#categoria").empty();
                            $("#materiales").empty();
                            $("#subcategoria").empty();

                                generar_opciones_categoria(catego);



                            $("#modalCreate").click(function() {

                                $("#materiales").empty();
                                $("#subcategoria").empty();
                                $("#subcategoria").append("<option value='' selected disabled>Subcategoria</option>");
                                generar_opciones_subcategoria(subcatego);
                            //$("#materiales input[type=checkbox]").prop("checked", false);
                            });

                            $("#agregar").click(function() {


                                $("#materiales input[type=checkbox]").removeAttr("disabled");

                                var total_temp=0;
                                var aux1 = 0;
                                $.each(materiales,function(key,value){


                                        var index=0;
                                        var part1;
                                        var part2="";
                                        var part3;

                                        var partb1;
                                        var partb2;
                                        var partb3;

                                            $('input[name^="material_lista"]').each(function(){

                                            codigo_temporal = $(this).val();
                                            var seleccionado = $(this).is(':checked');
                                            if(seleccionado && value.cod_material == codigo_temporal && !(datatemp.find(element => element == value.cod_material))){

                                            var auxiliar = 1*value.tallaInicial;
                                            var aux2 = 0;
                                            part1 = "<tr id='"+value.cod_material+"'><td><input name='nombre[]' type='hidden'value='"+value.cod_material+"' >"+value.cod_material+"</td>"+
                                            "<td><input name='descripcion[]' type='hidden' value='"+value.descrip_material+"' >"+value.descrip_material+"</td>";

                                            for(let i=0;i<7;i++){

                                                if(1*value.tallaInicial+i<=value.tallaFinal){
                                                        part2 = part2 + "<td>T"+(1*value.tallaInicial+i)+"<br><input name='tallasFinal"+value.cod_material +"[]'  id='value"+aux1+aux2+"'    type='hidden'> <input required name='tallasFinal2"+value.cod_material +"[]' type='number' min='0' step='0.01' id='inp"+value.cod_material+aux1+aux2+"'  value='0' style='width : 110%;'></td>";
                                                }
                                                else{
                                                    part2 = part2 + "<td></td>";
                                                }
                                                aux2++;
                                                auxiliar = auxiliar +1;
                                            }

                                            part3 = "<td style='width: 8%'><input name='cantidad_talla[]' type='number'step='any'  id='cantidadMat"+value.cod_material+"'style='width : 90%;' readonly ></td>"+
                                            "<td style='width: 8%'><input name='unidad_compra[]' type='hidden'value='"+value.unidad_compra+"'>"+value.descrip_unidad_compra+"</td>"+
                                            "<td style='width: 8%'>S/.<input name='costo_sin_igv[]' type='hidden'value='"+value.costo_sin_igv_material+"'>"+value.costo_sin_igv_material+"</td>"+
                                            "<td style='width: 8%'>S/.<input name='costo_total[] costo_total_sum' id='cantidadTotalMat"+value.cod_material+"' type='hidden'><input type='text' value='0'  id='cantidadTotalMat2"+value.cod_material+"' disabled='true'  style='width : 70%;'></td>"+
                                            "<td><button class='bttn-unite bttn-md bttn-danger'><i class='fas fa-trash-alt'></i></button></td></tr>";

                                            const tr = $(part1 + part2 + part3 );


                                            t.row.add(tr[0]).draw();
                                            var valorTotalTalla;
                                            var valorInp;
                                            var valorInpAnt;
                                            var IDaux = "";
                                            var IDaux2 = "";
                                            for(var j=0;j<7;j++){
                                                $('#inp'+value.cod_material+aux1+j).change(function () {

                                                    IDaux2 = $(this).attr("id");
                                                    if(valorTotalTalla == null){
                                                        valorInp = $(this).val();
                                                        $("#cantidadMat"+value.cod_material).val(valorInp)*1;
                                                        valorTotalTalla= $("#cantidadMat"+value.cod_material).val();
                                                        valorInpAnt = $(this).val();
                                                        $("#value"+aux1+"1").val("hola");
                                                        var datos = "";

                                                        var aux=0;
                                                        var datosfinalultimo = [];

                                                        $('input[name^="tallasFinal2'+value.cod_material+'"]').each(function()
                                                        {
                                                            datosfinalultimo.push(aux);
                                                            datosfinalultimo[aux]=$(this).val();
                                                            aux++;
                                                        });

                                                        aux = 0;
                                                        $('input[name^="tallasFinal'+value.cod_material+'"]').each(function()
                                                        {
                                                            $(this).val(datosfinalultimo[aux]+"-"+(1*value.tallaInicial+aux));
                                                            aux++;

                                                        });

                                                    }

                                                    if( IDaux == $(this).attr("id")){
                                                        valorInp = $(this).val();

                                                        //console.log("InpAnt " +valorInpAnt);
                                                        //console.log("ValorTotal " +valorTotalTalla);
                                                        valorTotalTalla=valorTotalTalla - valorInpAnt ;
                                                        valorTotalTalla=valorTotalTalla*1;
                                                        valorTotalTalla=valorTotalTalla+$(this).val()*1;
                                                        $("#cantidadMat"+value.cod_material).val(valorTotalTalla)*1;
                                                        valorTotalTalla= $("#cantidadMat"+value.cod_material).val();
                                                        valorInpAnt = $(this).val();

                                                        $("#value"+aux1+"1").val("hola");
                                                        //arregloTallas[varIncrement]=valorInpAnt;


                                                        var datos = "";

                                                        var aux=0;
                                                        var datosfinalultimo = [];

                                                        $('input[name^="tallasFinal2'+value.cod_material+'"]').each(function()
                                                        {
                                                            datosfinalultimo.push(aux);
                                                            datosfinalultimo[aux]=$(this).val();
                                                            aux++;
                                                        });

                                                        aux = 0;
                                                        $('input[name^="tallasFinal'+value.cod_material+'"]').each(function()
                                                        {
                                                            $(this).val(datosfinalultimo[aux]+"-"+(1*value.tallaInicial+aux));
                                                            aux++;
                                                        });

                                                    }else{
                                                        valorInp = $(this).val();

                                                        valorTotalTalla=valorTotalTalla - valorInpAnt ;
                                                        valorTotalTalla=valorTotalTalla*1;
                                                        valorTotalTalla=valorTotalTalla+$(this).val()*1;
                                                        $("#cantidadMat"+value.cod_material).val(valorTotalTalla)*1;
                                                        valorTotalTalla= $("#cantidadMat"+value.cod_material).val();
                                                        $("#value"+aux1+"1").val("hola");

                                                        var datos = "";

                                                        var aux=0;
                                                        var datosfinalultimo = [];

                                                        $('input[name^="tallasFinal2'+value.cod_material+'"]').each(function()
                                                        {
                                                            datosfinalultimo.push(aux);
                                                            datosfinalultimo[aux]=$(this).val();
                                                            aux++;
                                                        });

                                                        aux = 0;
                                                        $('input[name^="tallasFinal'+value.cod_material+'"]').each(function()
                                                        {
                                                            $(this).val(datosfinalultimo[aux]+"-"+(1*value.tallaInicial+aux));
                                                            aux++;
                                                        });

                                                    }
                                                    IDaux = $(this).attr("id");
                                                    sumaTallaTotal(50);
                                                });
                                                $('#inp'+value.cod_material+aux1+j).click(function () {
                                                    valorInpAnt = $(this).val();

                                                 });
                                                 $('#inp'+value.cod_material+aux1+j).select(function () {
                                                    valorInpAnt = $(this).val();

                                                 });
                                            }
                                            var valortotal;
                                            var sumTotal;
                                            var sumCantidadTotal;
                                            var valorCantidadTotal;

                                            function sumaTallaTotal(valorAux){
                                                costoTotalMat=valorAux;

                                                $("#cantidadTotalMat"+value.cod_material).val(($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material).toFixed(2));

                                                $("#cantidadTotalMat2"+value.cod_material).val(($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material).toFixed(2));


                                                if(valortotal==null){
                                                    subTotal=subTotal+($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material);
                                                    valortotal = $("#cantidadTotalMat2"+value.cod_material).val();
                                                    cantidadTot=cantidadTot+($("#cantidadMat"+value.cod_material).val()*1);
                                                    valorCantidadTotal=$("#cantidadMat"+value.cod_material).val()*1;
                                                    $("#subTotalval").val(subTotal.toFixed(2));
                                                    $("#cantidad_total_controller").val(cantidadTot);
                                                    $("#cantidad_faltante_controller").val(cantidadTot);
                                                    $("#idTotal").val((subTotal*1.18).toFixed(2));
                                                    $("#igvSubtotal").val( ($("#idTotal").val() - $("#subTotalval").val()).toFixed(2));

                                                }

                                                else{

                                                    sumTotal = ($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material);
                                                    sumCantidadTotal=$("#cantidadMat"+value.cod_material).val()*1;
                                                    subTotal= subTotal- valortotal+ sumTotal;
                                                    cantidadTot=cantidadTot-valorCantidadTotal+sumCantidadTotal;
                                                    valorCantidadTotal=$("#cantidadMat"+value.cod_material).val()*1;
                                                    valortotal = $("#cantidadTotalMat2"+value.cod_material).val();
                                                    $("#cantidad_total_controller").val(cantidadTot);
                                                    $("#cantidad_faltante_controller").val(cantidadTot);
                                                    $("#subTotalval").val(subTotal.toFixed(2));
                                                    $("#idTotal").val((subTotal*1.18).toFixed(2));
                                                    $("#igvSubtotal").val( ($("#idTotal").val() - $("#subTotalval").val()).toFixed(2));

                                                }
                                            }
                                            datatemp.push(value.cod_material);
                                            $("#costototal").empty();
                                            aux1++;
                                        } });


                                        index++;


                                });


                            });

                            cargaDatos();
                        }

                        function cargaDatos(){

                           $( "#categoria" ).select(function() {
                            let valor=$("#categoria").val();
                            $("#subcategoria").empty();
                            generar_opciones_subcategoria(valor);
                        });
                        }

                        function buscar(id)  {

                            var index = -1;
                            var filteredObj = data.find(function(item, i){

                            if(item.RUC_proveedor == id){
                                index = i;
                                return index;
                            }
                            });
                            return index;
                        }
                        function buscarMaterial(id)  {

                         var index = -1;
                         var filteredObj = data.find(function(item, i){

                         if(item.cod_material == id){
                             index = i;
                             return index;
                         }
                         });
                         return index;
                     }
                        function buscarCategoria(id)  {

                         var index = -1;
                         var filteredObj = categoria.find(function(item, i){

                         if(item.cod_categoria == id){
                             index = i;
                             return index;
                         }
                         });
                         return index;
                        }
                        var lista_materiales=[];
                        var cod_materiales=[];

                        $( "#subcategoria" ).change(function() {
                            let valor=$("#subcategoria").val();
                            generar_opciones_materiales(valor);
                            $("#materiales").empty();
                        });
                        $('body').on('click','#materiales input[type=checkbox]',function(event){
                               if( $(this).is(':checked')){
                                $(this).attr("disabled", true);
                               }
                         });

                        function generar_opciones_materiales(valor) {
                            $.ajax({
                                url: "consulta/obtener_mat_talla/"+valor,
                                success: function(html){
                                    $.each(html,function(key,value){
                                        var codigo=value.cod_material;
                                        var nombre=value.descrip_material;

                                        if (datatemp.find(element => element == value.cod_material)) {

                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' checked disabled>" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        } else {
                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' >" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        }

                                    });
                                }
                            });

                        };
                        function generar_opciones_categoria(valor) {
                                $.ajax({
                                        url: "consulta/obtener_cat/"+valor,
                                        success: function(html){

                                            $.each(html,function(key,value){
                                                var codigo=value.cod_categoria;
                                                var nombre=value.nom_categoria;

                                                $("#categoria").append(" <option value='"+codigo+"'>"+nombre+"</option>");
                                            });
                                        }
                                    });

                                };
                        function generar_opciones_subcategoria(valor) {
                         //  $("#subcategoria").append("<option value='' selected disabled>Subcategoria</option>");
                            $.ajax({
                                    url: "consulta/obtener_subcat/"+valor,
                                    success: function(html){

                                        $.each(html,function(key,value){
                                            var codigo=value.cod_subcategoria;
                                            var nombre=value.nom_subcategoria;

                                            $("#subcategoria").append(" <option value='"+codigo+"'>"+nombre+"</option>");
                                            });
                                        }
                                    });
                                };
                            });
                </script>
            </div>
        </div>
    </div>
    <style>

  #customersTABLA {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  #customersTABLA td {
    width: 4%
  }
  #customersTABLA td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;

  }

  #customersTABLA tr:nth-child(even){background-color: #f2f2f2;}

  #customersTABLA tr:hover {background-color: #ddd;}

  #customersTABLA th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;

    color: black;
  }
    </style>
    <div class="modal fade"  id="modal-create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header float-right">
                <h5>Agregar Materiales por Tallas</h5>
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i> </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-3 col-md-offset-3 col-xs-12 ">
                        <label for="total_orden_compra">Orden de compra Nª:</label>

                    </div>
                    <div class="form-group col-md-3 col-md-offset-3 col-xs-12 ">

                        <input type="text" id="orden" name="orden_compra" maxlength="100" class="form-control" disabled />
                    </div>
                    <div class="form-group col-md-2 col-md-offset-2 col-xs-12">
                        <label for="total_orden_compra">Proveedor:</label>
                   </div>
                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                        <input type="text" id="nombre" name="" maxlength="100" class="form-control" disabled />
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Categoría del Producto:</label>
                        <select required="required" id="categoria" name="tipo_categoria" class="custom-select">
                            <option value="" disabled>Categoria</option>

                        </select>
                    </div>
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Subcategoría del Producto:</label>
                        <select required="required" id="subcategoria" name="tipo_subcategoria" class="custom-select">
                            <option value="" disabled>Subategoria</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                        <label for="materiales">Materiales</label>
                        <div id="materiales" style="overflow-y:scroll;height:150px">
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer" style="margin-right: 70%;">
            <button type="submit" id="agregar"
                            class="bttn-unite bttn-md bttn-success" data-dismiss="modal">Agregar</button>

                                <button type="button"
                        class="bttn-unite bttn-md bttn-danger " data-dismiss="modal">Cancelar</button>

			</div>
        </div>
    </div>

    </div>

    @push('scripts')
        <script>
            $('#liAlmacen').addClass("treeview active");
            $('#liCategorias').addClass("active");

        </script>
    @endpush
@endsection
