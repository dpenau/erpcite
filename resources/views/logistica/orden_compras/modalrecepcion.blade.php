<div class="modal fade" id="modal-recepcion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
            <div class="modal-header float-right">
                <h5>Recepcion de Materiales Normales</h5>
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i> </div>
            </div>
            {{ Form::Open(['action' => ['OrdenCompraController@update','1'], 'method' => 'patch']) }}
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Orden de compra:</label>
                        <input type="text" id="orden_compra" name="orden_compra" maxlength="100" class="form-control" disabled>
                    </div>
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Fecha de emisión:</label>
                        <input type="text" name="fecha_emision" class="form-control " id="fecha_emision">
                    </div>
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Fecha de Recepcion:</label>
                        <?php $fcha = date("Y-m-d");?>
                        <input type="date" name="fecha_recepcion" class="form-control " value="<?php echo $fcha;?>">
                    </div>
                </div>
                <div>
                    <table class="table table-bordered" id="tabla_recepcion">
                        <thead>
                            <tr>
                                <th scope="col">Codigo Material</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Cantidad Pedida</th>
                                <th scope="col">Cantidad Recibida</th>
								<th scope="col">Cantidad Faltante</th>
								<th scope="col">Unidad de compra</th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                </div>
				<div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="textarea" id="obervaciones" name="observaciones" maxlength="2000" class="form-control">
                        <input type="hidden" id="tipoDetalle" name="tipoDetalle" value="1" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
				 <button type="submit" class="btn btn-primary" id="guardarRecepcion">Guardar Cambios</button> 
				 <button  type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button> 
                 <a class="btn" id="recepcionar_todo" href="{{url('logistica/orden_compras')}}">Recepcionar Todo</a>
				 <!--<button href="{{url('logistica/orden_compras')}}" type="button" class="btn btn-primary" id="recepcionar_todo">Recepcionar Todo</button> -->
			</div>
            {!! Form::Close() !!}
        </div>
    </div>
</div>