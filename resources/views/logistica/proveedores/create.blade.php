@extends ('layouts.admin')
@section('contenido')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nuevo Proveedor</h2>
                    <div class="clearfix"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['url' => 'logistica/proveedores', 'method' => 'POST', 'autocomplete' => 'off']) !!}
                {{ Form::token() }}
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="tipo_categoria">Categoria:</label>
                        <select required="required" id="categoria" name="tipo_categoria" class="custom-select">
                            <option value="" selected disabled>Categoria</option>
                            @foreach ($categoria as $cat)
                                <option value="{{ $cat->cod_categoria }}">{{ $cat->nom_categoria }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="materiales">Materiales Principales:</label>
                        <div id="materiales" style="overflow-y:scroll;height:150px">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">RUC Proveedor:</label>
                        <input pattern=".{11,}" title="Longitud incorrecta" onkeypress="return isNumberKey(event)"
                            maxlength="11" required="required" type="text" id="ruc" name="RUC_proveedor" required="required"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Razon Social:</label>
                        <input maxlength="50" id="razon_social" required="required" type="text" name="nomb_proveedor"
                            required="required" class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Direccion 1:</label>
                        <textarea class="form-control" maxlength="100" id="direccion1" rows="3" name="dir_proveedor"
                            required="required"></textarea>
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Direccion 2 (Tienda):</label>
                        <textarea class="form-control" maxlength="100" rows="3" name="tienda_proveedor"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Celular del proveedor:</label>
                        <input onkeypress="return isNumberKey(event)" maxlength="9" type="text" name="tele_proveedor"
                            required="required" class="form-control ">
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Correo de Proveedor:</label>
                        <input type="email" name="correo_proveedor" maxlength="50" class="form-control ">
                    </div>
                </div>
                <label for="">Contactos:</label>
                <div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto:</label>
                        <input type="text" name="nom_contacto" maxlength="100" class="form-control ">
                    </div>
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto:</label>
                        <input onkeypress="return isNumberKey(event)" maxlength="9" type="text" name="tel_contacto"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-3  col-xs-12">
                        <label for="total_orden_compra">Correo de contacto:</label>
                        <input type="email" name="correo_contacto" maxlength="50" class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto 2:</label>
                        <input type="text" name="nom_contacto2" maxlength="100" class="form-control ">
                    </div>
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto 2:</label>
                        <input onkeypress="return isNumberKey(event)" maxlength="9" type="text" name="tel_contacto2"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="total_orden_compra">Correo de contacto 2:</label>
                        <input type="email" name="correo_contacto2" maxlength="50" class="form-control ">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <a href="{{ url('logistica/proveedores') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
                        <button type="submit"
                            class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>

    <script>
        $(document).ready(function() {

            $("#filtro").keyup(function() {
                var valor = $("#filtro").val();
                if (lista_materiales.length == 0) {
                    alert("Seleccione una categoria")
                    $("#filtro").val("");
                } else {
                    var tmp = lista_materiales;
                    console.log("temporal")
                    console.log(tmp)
                    if (valor != "") {
                        for (var i = 0; i < tmp.length; i++) {
                            if (tmp[i].indexOf(valor) == -1) {
                                $("#d" + cod_materiales[i]).hide();
                            } else {
                                $("#d" + cod_materiales[i]).show();
                            }
                        }
                    } else {
                        for (var i = 0; i < tmp.length; i++) {
                            $("#d" + cod_materiales[i]).show();
                        }
                    }
                }
            })

            var lista_materiales = [];
            var cod_materiales = [];

            $("#categoria").change(function() {
                //console.log("Ingreso categoria"); //Flag
                let valor = $("#categoria").val();
                $("#materiales").empty();
                generar_opciones(valor);
            });

            function generar_opciones(valor) {
                //console.log(valor); //id_category
                $.ajax({
                    url: "consulta/obtener_material/" + valor,
                    type: "GET",
                    success: function(html) {
                        $("#materiales").append("<h6>Seleccione Material</h6>");
                        $.each(html, function(key, value) {
                            const codigo = value.cod_subcategoria;
                            const nombre = value.nom_subcategoria;
                            //console.log(codigo + "-" + nombre); //id + name from subcategories

                            $("#materiales").append("<input id='" + codigo +
                                "' type='checkbox' name='subcategoria[]' value='" + codigo +
                                "' >" +
                                "<label for='" + codigo + "'>" + nombre + "</label></br>");
                        });
                        $("#materiales").append("</div>");
                    }
                });
            };
        })
    </script>
@endsection
