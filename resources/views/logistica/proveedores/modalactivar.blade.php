<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-active-{{$pro->RUC_proveedor}}">
{{Form::Open(array('action'=>array('ProveedorController@destroy',$pro->RUC_proveedor
),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
					<h4 class="modal-title">Activar Proveedor</h4>
				<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>

			</div>
			<div class="modal-body">
				<p>¿Está seguro que desea activar este proveedor?</p>
				<input type="text" style="display:none" name="email" value="{{$pro->RUC_proveedor}}">
        <input type="text" style="display:none" name="estado" value="1">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
