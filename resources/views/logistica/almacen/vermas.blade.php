<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-vermas-{{$trab->cod_almacen}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">{{$trab->nom_almacen}}</h4>
			</div>
			<div class="modal-body">
		
        <h6>Encargado: {{$trab->nombres." ".$trab->apellido_paterno." ".$trab->apellido_materno}}</h6>
        <h6>Categoria: {{$trab->nom_categoria}}</h6>
			</div>
	
		<div class="modal-footer">
				
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
			</div>
	</div>
</div>
