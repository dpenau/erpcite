@extends ('layouts.admin')
@section ('contenido')
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nueva Coleccion de Calzado</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'Produccion/coleccion','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                    <div class="row">
                      <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Nombre de Coleccion</label>
                         <input type="text" id="nombreLinea" name="nombre" required="required" maxlength="35" class="form-control ">
                      </div>
                    </div>
                        <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                            <a href="{{ url('Produccion/lineas_calzado') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-5">Cancelar</button>
                              <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button></a>
                            </div>
                          </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
@endsection
