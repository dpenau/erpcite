<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$op->cod_modelo}}">
	{{Form::Open(array('action'=>array('ModelosCalzadoController@destroy',$op->cod_modelo
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Modelo</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Eliminar el Modelo</p>
				<input type="text" style="display:none" name="email" value="{{$op->cod_modelo}}">
        <input type="text" style="display:none" name="estado" value="0">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
