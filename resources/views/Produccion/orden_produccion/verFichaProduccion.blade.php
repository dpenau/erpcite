@extends ('layouts.admin')
@section ('contenido')
<!--2
   Desarrollado por
   Jesús Albino Calderón
   México
   ITSZO && UNSA
-->

<script type="text/javascript">
var datos =<?php echo $datos;?>;
console.log(datos);

</script>

<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<div>
    <h1 class="font-weight-bold">Ficha de Orden de Producción
    <!--a href="{s{ route('Produccion/orden_produccion') }}" target='blank' --> <a href="#"></a>
                        <button class="bttn-unite bttn-md btn-dark float-right mr-sm-4">
                        <i class="fa fa-arrow-circle-o-left"></i></button>
                    </a>
                    <a href="/pedido/ficha_produccion/pdf/{{$datos[0]->codigo_modelo}}" target='blank'>
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Descargar Ficha</button></a>
            <a href="/pedido/ficha_produccion/pdf/{{$datos[0]->codigo_modelo}}" target='blank'>
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Descargar ORDEN</button></a>
    </h1>
    <hr> <hr>
</div>
<p>
    <h4><b>Modelo: </b>{{$datos[0]->codigo_modelo}}
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <b>Código de Orden de Producción:  </b>{{$datos[0]->codigo_orden_pedido_produccion}}  </h4>

    <br>

    <h4><b>Colección: </b> {{$datos[0]->nombre_coleccion}}</h4>
    <br>

    <!--este no se si esta bien porque son varias hormas las
    que se nesesitan para la orden de produccion
    o se colocan todas las utilizadas -->
    <h4><b>Horma: </b> {{$datos[0]->descrip_material}}</h4>
    <br>

    <h4><b>Linea: </b> {{$datos[0]->nombre_linea}}</h4>
    <br>

    <h4><b>Serie: </b> {{$datos[0]->nombre_serie}}</h4>
    <br>

    <h6 align="center">
    <table class="table table-striped" size="10%">
            <tr>
            @for($i=$datos[0]->tallaInicial;$i<=$datos[0]->tallaFinal;$i++)
				<td>{{$i}}</td>
            @endfor
            </tr>
            <tr>
            <?php $tallas=explode(",",$datos[0]->cantidades); ?>
            @foreach($tallas as $tal)
                <td>{{$tal}}</td>
            @endforeach
            </tr>
		</table>
    <br>

    <br>

    <h4 align="center">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <th>Componentes</th>
                </tr>
            </table> 
            <div style="background-color:#E15A3D" >
                CORTE
            </div>
            <div id="corte_com"></div>
            <div style="background-color:#E15A9D" >
                HABILITADO
            </div>
            <div id="habilitado_com"></div>
            <div style="background-color:#F9E855" >
                APARADO
            </div>
            <div id="aparado_com"></div>
            <div style="background-color:#F9E895" >
                ALISTADO
            </div>
            <div id="alistado_com"></div>
            <div style="background-color:#7CD3D2" >
                MONTAJE
            </div>
            <div id="montaje_com"></div>
            <div style="background-color:#8CD37C" >
                ACABADO
            </div>
            <div id="acabado_com"></div>
        </div>
        <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                <th>Especificaciones</th>
                </tr>
            </table>
            <div style="background-color:#E15A3D" >
                CORTE
            </div>
            <div id="corte_es"></div>
            <div style="background-color:#E15A9D" >
                HABILITADO
            </div>
            <div id="habilitado_es"></div>
            <div style="background-color:#F9E855" >
                APARADO
            </div>
            <div id="aparado_es"></div>
            <div style="background-color:#F9E895" >
                ALISTADO
            </div>
            <div id="alistado_es"></div>
            <div style="background-color:#7CD3D2" >
                MONTAJE
            </div>
            <div id="montaje_es"></div>
            <div style="background-color:#8CD37C" >
                ACABADO
            </div>
            <div id="acabado_es"></div>
        </div>
    </div>

</p>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        var materiales =<?php echo $componenetes;?>;
        var operaciones =<?php echo $operaciones;?>;
        llenar_tabla()

        function llenar_tabla(){
            $.each(materiales,function(i,val){
                var area=val.descrip_area;
                switch(area)
                {
                    case "P-Corte":$("#corte_com").append("<p>"+val.descrip_material+"</p>")
                    break;
                    case "P-Habilitado":$("#habilitado_com").append("<p>"+val.descrip_material+"</p>")
                    break;
                    case "P-Aparado":$("#aparado_com").append("<p>"+val.descrip_material+"</p>")
                    break;
                    case "P-Alistado":$("#alistado_com").append("<p>"+val.descrip_material+"</p>")
                    break;
                    case "P-Montaje":$("#montaje_com").append("<p>"+val.descrip_material+"</p>")
                    break;
                    case "P-Acabado":$("#acabado_com").append("<p>"+val.descrip_material+"</p>")
                    break;
                }
            })
            $.each(operaciones,function(i,val){
                var area=val.descrip_area;
                switch(area)
                {
                    case "P-Corte":$("#corte_es").append("<p>"+val.operacion+"</p>")
                    break;
                    case "P-Habilitado":$("#habilitado_es").append("<p>"+val.operacion+"</p>")
                    break;
                    case "P-Aparado":$("#aparado_es").append("<p>"+val.operacion+"</p>")
                    break;
                    case "P-Alistado":$("#alistado_es").append("<p>"+val.operacion+"</p>")
                    break;
                    case "P-Montaje":$("#montaje_es").append("<p>"+val.operacion+"</p>")
                    break;
                    case "P-Acabado":$("#acabado_es").append("<p>"+val.operacion+"</p>")
                    break;
                }
            })
        }
    });
</script>


@endsection
