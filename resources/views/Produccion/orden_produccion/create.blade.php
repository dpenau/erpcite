@extends ('layouts.admin')
@section ('contenido')

<script type="text/javascript">
  let v_ordenesProd = @json($ordenesProd);
</script>
<div class="preloader"></div>
<div>
  <h3 class="font-weight-bold">Nueva Orden de Produccion</h3>
</div>

<div class="col">

  <h4>Orden de pedido:{{$codigoOrdenPed}}</h4>

</div>

<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr>
        <th>Modelo</th>
        <th>Cantidad</th>
        <th>Serie</th>
        <th>Tallas</th>
        <th>Estado</th>
      </tr>
    </thead>

    <tbody>
      <?php
      $f = 0;
      ?>
      @foreach($ordenesProd as $ordenesProd)
      <tr>
        <td>{{$ordenesProd->codigo_modelo}}
        </td>
        <td class="tot{{$ordenesProd->codigo}}"> {{ $sumaSubTall[$f]}} pares
        </td>
        <td> {{$ordenesProd->nombre_serie}}
        </td>
        <td>
          <table class="table table-bordered">
            <thead>
              <th scope="col">{{$ordenesProd->tallaInicial}}</th>
              @for ($j = $ordenesProd->tallaInicial+1; $j<$ordenesProd->tallaFinal; $j++)
                <th scope="col">{{$j}}</th>
              @endfor
                <th scope="col">{{$ordenesProd->tallaFinal}}</th>
            </thead>
            <tbody>
              <tr>
                @for ($k =0; $k<$cantidad_tallas[$f]; $k++)
                <td class="{{$ordenesProd->codigo}}" scope="col">{{$Cant[$f][$k]}}</td>
                @endfor
              </tr>

              </tbody>

  </table>
  @if ( $ordenesProd->tipo_urgencia == 0)
  <td><span class="fa fa-bell" style="font-size:48px;color:green"></span></td>
  @else
  <td><span class="fa fa-bell" style="font-size:48px;color:red"></span></td>
  @endif
  </td>

  <?php
  $f = $f + 1;
  ?>
  @endforeach


  </tr>
  </tbody>
  </table>
</div>

<br>

<h3>Generar Orden de Producción</h3>
<br>

<div class="table-responsive">
  <table class="table table-fixed">
    <tbody>
      <tr>
        <td>
          <h4>Cliente:</h4>
        </td>
        <td>
          <div class="form-group">
            <input type="text" name="nombre_cliente" class="form-control" id="" value="{{$cliente[0]->codigo_cliente}}" readonly>
          </div>
        </td>
        <td>
          <h4>Fecha Creacion:</h4>
        </td>
        <td>
          <h4>{{date('Y-m-d')}}</h4>
        </td>
        <td>
          <h4>Fecha Entrega:</h4>
        </td>
        <td>
          <h4>
            <input type="date" id="fecha_entrega" name="" value="">
          </h4>
        </td>
      </tr>
      <tr>
        <td>
          <h4>Cantidad: </h4>
        </td>
        <td>
          <div class="form-group">
            <input type="text" id="cantidadTotal" name="cantidadTotal" class="form-control"  readonly>
          </div>
        </td>
        <td>
          <h4>Total: </h4>
        </td>
        <td>
          <div class="form-group">
            <input type="text" id="total_tallas" name="total_tallas" class="form-control"  readonly>
            <input type="text"  style="display:none" id="codigo_modelo" readonly>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <h4>Modelo: </h4>
        </td>
        <td>
          <select id="selectModel" class="custom-select" name="selectModel">
            <option value="default" selected >Escoger Modelo</option>
            <script type="text/javascript">
              for (var i = 0; i < v_ordenesProd.length; i++) {
                document.write(`<option value="${v_ordenesProd[i].codigo_modelo}">${v_ordenesProd[i].codigo_modelo}</option>`);
              }
            </script>
          </select>
        </td>
        <td>

        </td>
        <td>

        </td>
      </tr>
      <tr>
        <td>
          <h4>Tallas: </h4>
        </td>
        <td colspan="3">
          <div id="tablaTallasCantidades" class="table-responsive">

          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>

<!--Begin opciones-->
<!--Begin contenedor primera fila opciones-->

<br>
<!--End contenedor primera fila opciones-->
<!--Begin contenedor segunda fila opciones-->

<!--End contenedor segunda fila opciones-->
<!--Begin contenedor tercera fila opciones-->
<div class="ln_solid"></div>
<div class="form-group">
  <div class="col-md-12 col-sm-6 col-xs-12">
    <button id="generarProduccion" type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Realizado</button>
    <a href="/Produccion/orden_produccion/{{$codigoOrdenPed}}"><button class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Atras</button></a>
  </div>
</div>
<!--End contenedor tercera fila opciones-->
<!--End opciones-->
<div id="result"></div>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js "></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    var t=$("#tabla_reporte_clientes").DataTable();
    var lista=[];

    let selectedModelIndex = -1;
    var suma=0;
    function sumarCantidades() {
      var sum = 0;
      // we use jQuery each() to loop through all the textbox with 'price' class
      // and compute the sum for each loop
      $('.talla_val').each(function() {
        sum += Number($(this).attr("placeholder"));
      });

      // set the computed value to 'totalPrice' textbox
      $('#total_tallas').attr("placeholder",sum);
    }

    function obtenerArrayTallasDescripcion() {
      let arrayTallasDesc = [];
      $('.talla_desc').each(function() {
        arrayTallasDesc.push(Number($(this).val()));
      });
      return arrayTallasDesc;
    }

    function obtenerArrayTallasValores() {
      let arrayTallasVal = [];
      $('.talla_val').each(function() {
        arrayTallasVal.push(Number($(this).val()));
      });
      return arrayTallasVal;
    }

    $(document).on("change, keyup, input", '.talla_val', sumarCantidades);
    function limpiar()
    {
      $("#total_tallas").val("");
      suma=0;
      lista=[]
    }
    $("#selectModel").change(function() {
      
      //document.getElementById("loc").innerHTML = "" + document.getElementById("selectModel").value;
      limpiar()
      var horma="";

      let contentHmtl = `<table class="table table-bordered tabla_tallas">
            <thead>
              <tr>`;
      let kardexCod_kardexTallas = new Array();
      for (var i = 0; i < v_ordenesProd.length; i++) {
        if (v_ordenesProd[i].codigo_modelo === $("#selectModel option:selected").text()) {
          
          horma=v_ordenesProd[i].descrip_material;
          $("#codigo_modelo").val(v_ordenesProd[i].codigo)
          for (var j = v_ordenesProd[i].tallaInicial; j <= v_ordenesProd[i].tallaFinal; j++) {
            contentHmtl += `<th scope="col"><input type="number" class="talla_desc" value="` + j + `" disabled/></th>`;
          }
          contentHmtl += `</tr>
            </thead>
            <tbody>
              <tr>`;
              
          selectedModelIndex = i;
          let cantidadadesPedido = v_ordenesProd[selectedModelIndex].cantidades;
          let cantidadesOrdenes=v_ordenesProd[selectedModelIndex].cantidades_ord_prod;
          if (cantidadesOrdenes==null) {
            cantidadesOrdenes= [];
            cantidadadesPedido = cantidadadesPedido.split(',');
            for (var k = 0; k < cantidadadesPedido.length; k++) {
                  let cantidad=0;
                  cantidad=parseFloat(cantidadadesPedido[k]);
                  contentHmtl += `<td class="talla_c"><input type="number" class="talla_val" id='`+k+`' placeholder='`+cantidad+`' /></td>`;
            }
          }
          else {
            if (cantidadesOrdenes == cantidadadesPedido) {
              $("#selectModel").val($("#selectModel option:first").val());
              $("#selectModel").trigger("change");
            }
            else {
              cantidadesOrdenes= cantidadesOrdenes.split(',');
              cantidadadesPedido = cantidadadesPedido.split(',');
              if (cantidadesOrdenes.length==0) {
                for (var k = 0; k < cantidadadesPedido.length; k++) {
                  let cantidad=0;
                  cantidad=parseFloat(cantidadadesPedido[k]);
                  contentHmtl += `<td class="talla_c"><input type="number" class="talla_val" id='`+k+`' placeholder='`+cantidad+`' /></td>`;
                }
              }
              else {
                for (var k = 0; k < cantidadadesPedido.length; k++) {
                  let cantidad=0;
                  cantidad=parseFloat(cantidadadesPedido[k])-parseFloat(cantidadesOrdenes[k]);
                  contentHmtl += `<td class="talla_c"><input type="number" class="talla_val" id='`+k+`' placeholder='`+cantidad+`' /></td>`;
                }
              }
              break; // salir
            }
          }
        }
      }
      

      contentHmtl += `</tr>
            </tbody>
          </table>
          <style media="screen">
          .talla_val {
            width: 100%;
          }
          .talla_desc{
            width: 100%;
          }

          </style>`;
      $("#tablaTallasCantidades").html(contentHmtl);
      $('.talla_val').each(function() {
        $(this).on("keyup",function(){
          if($(this).val()>0)
          {
            var maximo=parseFloat($(this).attr("placeholder"));
            var val=parseFloat($(this).val());
            if (maximo<$(this).val()) {
              $(this).val("")
              var id=$(this).attr("id");
              if(typeof lista[id]==="undefined" || lista[id]=="undefined") {
              }
              else {
                var valor_ant=parseFloat(lista[id])
                suma=suma-valor_ant;
                lista[id]="undefined";
                $("#total_tallas").val(suma);
              }
            }
            else {
              var valor=$(this).val();
              valor=parseFloat(valor);
              var id=$(this).attr("id");
              if(typeof lista[id]==="undefined" || lista[id]=="undefined"){
                suma=suma+valor
                lista[id]=valor;
              }
              else {
                var valor_ant=parseFloat(lista[id]);
                suma=suma-valor_ant;
                lista[id]=valor;
                suma=suma+valor;
              }
              $("#total_tallas").val(suma);
            }
          }
          else {
            $(this).val("")
            var id=$(this).attr("id");
            if(typeof lista[id]==="undefined" || lista[id]=="undefined") {
            }
            else {
              var valor_ant=parseFloat(lista[id])
              suma=suma-valor_ant;
              lista[id]="undefined";
              $("#total_tallas").val(suma);
            }
          }
        })
        $(this).on("click",function(){
          if($(this).val()<=0)
          {
            $(this).val("")
            var id=$(this).attr("id");
            if(typeof lista[id]==="undefined" || lista[id]=="undefined") {
            }
            else {
              var valor_ant=parseFloat(lista[id])
              suma=suma-valor_ant;
              lista[id]="undefined";
              $("#total_tallas").val(suma);
            }
          }
          else {
            var maximo=parseFloat($(this).attr("placeholder"));
            var val=parseFloat($(this).val());
            if (maximo<$(this).val()) {
              $(this).val("")
              var id=$(this).attr("id");
              if(typeof lista[id]==="undefined" || lista[id]=="undefined") {
              }
              else {
                var valor_ant=parseFloat(lista[id])
                suma=suma-valor_ant;
                lista[id]="undefined";
                $("#total_tallas").val(suma);
              }
            }
            else {
              var valor=$(this).val();
              valor=parseFloat(valor);
              var id=$(this).attr("id");
              if(typeof lista[id]==="undefined" || lista[id]=="undefined"){
                suma=suma+valor
                lista[id]=valor;
              }
              else {
                var valor_ant=parseFloat(lista[id]);
                suma=suma-valor_ant;
                lista[id]=valor;
                suma=suma+valor;
              }
              $("#total_tallas").val(suma);

            }
          }
        })
      });
      sumarCantidades();
      obtener_hormas(horma);
    });
    function obtener_hormas(codigo)
    {
      if(codigo=="")
      {
        $("#cantidadTotal").val("")
      }
      else {
        $.ajax({
          url:"/Produccion/orden_produccion/obt_hormas/"+codigo,
          success:function(datos)
          {
            $("#cantidadTotal").val(datos)
          }
        })
      }
    }
    function obtener_nueva_lista()
    {
      var cod="{{$codigoOrdenPed}}"
      $.ajax({
        url:"/Produccion/orden_produccion/act_lista/"+cod,
        success:function(datos)
        {
          v_ordenesProd=datos;
        }
      })
    }
    function actualizar_pedidos(valor)
    {
      obtener_nueva_lista();
      let cantidades_iniciales=$("."+valor)
      let total=$(".tot"+valor);
      let nuevas=$(".talla_val");

      var sum=0;
      for (var i = 0; i < cantidades_iniciales.length; i++) {
        let actual=parseFloat(cantidades_iniciales[i].innerHTML);
        let nuevo;
        if (nuevas[i].value=="") {
          nuevo=parseFloat(0);
        }
        else {
          nuevo=parseFloat(nuevas[i].value);
        }
        let actualizado=actual-nuevo
        cantidades_iniciales[i].innerHTML=actualizado;
        sum=sum+actualizado;
      }
      total[0].innerHTML=sum+" pares";
      $("#selectModel").val($("#selectModel option:first").val());
      $("#selectModel").trigger("change");
      limpiar()
    }
    $("#generarProduccion").click(function(e) {
      var total_de_tallas=parseFloat($('#total_tallas').val())
      var total_hormas=parseFloat($('#cantidadTotal').val())
      var codigo_pedido="{{$codigoOrdenPed}}";
      var fecha=$("#fecha_entrega").val();
      console.log(fecha)
      var envio=v_ordenesProd[selectedModelIndex].cantidades_ord_prod;
      if ((total_de_tallas <= total_hormas) && (total_de_tallas!="") && fecha!="") {
        let arrayTallasValores = obtenerArrayTallasValores();
        let cantidadesDetProduccion = arrayTallasValores.join(','); // convert array to string with separator ','
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/Produccion/orden_produccion/create",
          type: "post",
          data: {
            hormas_totales:$("#cantidadTotal").val(),
            codigo_serie : v_ordenesProd[selectedModelIndex].codigo,
            cantidadesDetProduccion : cantidadesDetProduccion,
            cantidadesPedido:envio,
            orden_pedido:codigo_pedido,
            tipo_urgencia:v_ordenesProd[selectedModelIndex].tipo_urgencia,
            fecha:fecha
          },
          success: function(response) {
            document.getElementById("result").innerHTML = `<div class="alert alert-success"><h3>Registrado</h3></div>`;
            setTimeout(function(){
              document.getElementById("result").innerHTML ='';
            }, 2000);
            let valor=response
            actualizar_pedidos(valor)
          },
          error: function() {
            document.getElementById("result").innerHTML = `<div class="alert alert-danger"><h3>No se registro !</h3></div>`;
            setTimeout(function(){
              document.getElementById("result").innerHTML ='';
            }, 2000);
          }
        });

      } else {
        alert("Debe ingresar tallas y las tallas no deben sobrepasar las cantidades, Ingrese una fecha de entrega");
      }

    });

  });
</script>
@endsection
