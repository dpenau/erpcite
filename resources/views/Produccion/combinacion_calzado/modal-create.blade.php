<div class="modal fade" id="modal-crear-combinacion" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    {!!Form::open(array('url'=>'Produccion/combinacion_calzado/createCombinacion/.{{$modelos->cod_modelo}}','files'=>true,'method'=>'POST','autocomplete'=>'off'))!!}
    <div class="modal-dialog" style="max-width: 80%;" role="document">
        <div class="modal-content">

            <div class="modal-header float-right">

                <h4 class="font-weight-bold">Nueva Combinación para {{$modelos->codigo}}</h4>

                <input type="hidden" id="{{$modelos->cod_modelo}}" value="{{$modelos->cod_modelo}}" name="codigo">
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i> </div>
            </div>

            <div class="modal-body">
                <div class="row" style="margin: 2% 3% 0 3%;">
                    <div class="col-4" style="text-align: left;">
                        <label>Colección:</label>
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <label>Líneas:</label>
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <label>Serie:</label>
                    </div>
                </div>
                <div class="row" style="margin: 0 3% 0 3%;">
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " name="coleccion" id="modal-crear-coleccion" readonly />
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " id="modal-crear-linea" readonly />

                    </div>
                    <div class="col-4" style="text-align: left;">
                        <label type="text" class="form-control " readonly>
                            {{$modelos->nombre_serie}}
                        </label>
                    </div>
                </div>
                <div class="row" style="margin:2% 3% 0 3%;">
                    <div class="col-4" style="text-align: left;">
                        <label>Código Modelo Base:</label>
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <label>Descripción de Modelos:</label>
                    </div>

                </div>
                <div class="row" style="margin: 0 3% 3% 3%;">
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " name="codCom" id="modal-crear-codCombin" />
                    </div>
                    <div class="col-8" style="text-align: left;">
                        <input type="text" class="form-control " name="descripCom" id="modal-crear-descripCombin" />
                    </div>
                </div>
                <div class="row" style="margin: 0 3% 3% 5%;">
                    <div class="btn-small amber darken-s">
                        <label for="exampleFormControlFile1">Imagen de modelo:</label>
                        <input type="file" class="form-control-file" id="photo" name="photo" required="required"
                            onchange="vista_preliminar(event)">
                    </div>
                    <div>
                        <img src="" alt="" id="img-foto" width="110" height="110" style="margin-left:6%;"> </img>
                    </div>
                </div>

            </div>
            <div class="modal-footer" style="text-align:left;">
                <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-1">Guardar</button>
                <button type="button" id="cancelarModal"
                    class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-1">Cancelar</button>

            </div>

        </div>
    </div>
    {{ Form::Close() }}
</div>
<style>
input.form-control:read-only {
    background-color: #fff;
}

label.form-control:read-only {
    background-color: #fff;
}
</style>