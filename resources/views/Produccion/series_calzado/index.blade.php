@extends ('layouts.admin')
@section ('contenido')
<div>
<h3 class="font-weight-bold">Series de calzado <a href="series_calzado/create">
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva Serie de calzado</button></a></h3>
</div>

<br>
<div class="x_content table-responsive" >
      <table id="example" class="display" style="width:100%">


    <thead>
      <tr align="center">
        <th align="center">Nombre Serie</th>
        <th align="center">Talla Inicial</th>
        <th align="center">Talla Final</th>
        <th align="center">Talla Media</th>
        <th align="center">Editar</th>

        <th align="center">Activar/Desactivar</th>

      </tr>
    </thead>
    <tbody>
      @foreach($Series as $op)
      @if($op->estado_serie==0)
      <tr class="bg-danger" align="center">
      @endif
        <td align="center">{{$op->nombre_serie}}</td>
        <td align="center">{{$op->tallaInicial}}</td>
        <td align="center">{{$op->tallaFinal}}</td>
        <td align="center"> {{round(($op->tallaInicial+$op->tallaFinal)/2,0)}}</td>
      <td align="center">
     <a href="" data-target="#modal-update-{{$op->cod_serie}}" data-toggle="modal">		  <button class="bttn-unite bttn-md bttn-primary"><i class="fas fa-edit"></i></button></a>

      </td>

        <td align="center">
          @if($op->estado_serie==0)
          <a href="" data-target="#modal-active-{{$op->cod_serie}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
          @else
          <a href="" data-target="#modal-delete-{{$op->cod_serie}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
          @endif

        </td>
      </tr>
      @include('Produccion.series_calzado.modal')
       @include('Produccion.series_calzado.modal_modificar')
       @include('Produccion.series_calzado.modal_activar')
      @endforeach
    </tbody>
  </table>
</div>



@endsection
