@extends ('layouts.admin')
@section('contenido')
    <div style="text-align: center;">

        <h3 class="font-weight-bold">Ficha de Producto</h3>
    </div>

    <div class="row">
        <div class="col-10" style="text-align: left;">
            <div class="row" style="margin: 2% 3% 0 3%;">
                <div class="col-4" style="text-align: left;">
                    <label>Colección:</label>
                </div>
                <div class="col-4" style="text-align: left;">
                    <label>Líneas:</label>
                </div>
                <div class="col-4" style="text-align: left;">
                    <label>Serie:</label>
                </div>
            </div>
            <div class="row" style="margin: 0 3% 0 3%;">
                @foreach ($listaComb as $modelos)
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " value="{{ $modelos->nombre_coleccion }}" readonly />
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " value="{{ $modelos->nombre_linea }}" readonly />
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " value="{{ $modelos->nombre_serie }}" readonly />
                    </div>
                @endforeach
            </div>
            <div class="row" style="margin:2% 3% 0 3%;">
                <div class="col-4" style="text-align: left;">
                    <label>Código Modelo Base:</label>
                </div>
                <div class="col-4" style="text-align: left;">
                    <label>Descripción de Modelos:</label>
                </div>

            </div>
            <div class="row" style="margin: 0 3% 3% 3%;">
                <div class="col-4" style="text-align: left;">
                    @foreach ($listaComb as $modelos)
                        <input type="text" class="form-control " value="{{ $modelos->codigo_comb }}" readonly />
                    @endforeach
                </div>
                <div class="col-8" style="text-align: left;">
                    @foreach ($listaComb as $modelos)
                        <input type="text" class="form-control " value="{{ $modelos->descripcion }}" readonly />
                    @endforeach
                </div>
            </div>


        </div>
        <div class="col-2" style="margin-top:3%;">
            @foreach ($listaComb as $modelos)
                <p> {{ Html::image('photo/modelos/' . $modelos->RUC_empresa . '/' . $modelos->image, 'alt', ['width' => 150, 'height' => 150]) }}
                </p>
            @endforeach
        </div>
    </div>
    <br>
    {{ Form::Open(['action' => ['CombinacionFichaCalzadoController@aprobar_m'], 'method' => 'POST']) }}
    <div class="row" style="margin:-1% 3% 4% 3%;">
        <div class="col-2" style="text-align: left; margin-top: 0.5%;">
            <label>Ficha Base:</label>
        </div>
        <!--COLOCAR MODELOS APROBADOS PARA SELECCIONAR SON TODOS LAS COMBINACIONES INCLUSO DE OTROS MODELOS-->
        <div class="col-6" style="margin-left: -7%;">
            <select name="modelo_activo" id="modelo_activo" class="custom-select">
                <option value="" selected disabled>Seleccionar ficha base</option>
                <option value="ninguno">Ninguno</option>
                @foreach ($listaCombinacion as $col)
                    <option value="{{ $col->cod_combinacion }}">{{ $col->codigo_comb }}-{{ $col->descripcion }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-4" style="margin-top: -0.3%;">
            <a href="#">
                <button type="submit" class="bttn-unite bttn-md bttn-success" name="ficha_base" value="2"><i
                        class="fa fa-check-circle"></i></button></a>

        </div>
    </div>
    <div class="row" style="margin: 3% 0 0 3%;">
        <div class="form-group  col-md-6  col-xs-12">
            @foreach ($listaComb as $modelos)
                <a class="a" id="{{ $modelos->cod_combinacion }}"
                    href="/Produccion/combinacion_ficha/ficha_producto/{{ $modelos->cod_combinacion }}">
                    <button id="materiales_directo" type="button" class="btn btn-lg button-mo ">Materiales Directos:
                        {{-- <strong>S/. {{ $modelos->costo_material_directo }}</strong> --}}
                    </button></a>

                <button id="mano_obra" type="button" class="btn btn-lg bttn-gray button-md">Mano de Obra Directa:
                    {{-- <strong>S/. {{ $modelos->costo_mano_directa }}</strong> --}}
                </button>
            @endforeach
        </div>

        <div class="col-md-6  col-xs-12" id="create-combinacion" style="margin: 0 0 4% 0;">



            @foreach ($listaComb as $modelos)
                <input type="hidden" name='codigo_combinacion' value="{{ $modelos->cod_combinacion }}">
                <a class="a" id="{{ $modelos->cod_combinacion }}"
                    href="/Produccion/combinacion_ficha/ficha_obra/{{ $modelos->cod_combinacion }}">

                    <button id="agregar_materiales" type="button"
                        class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Agregar Mano de Obra Directa
                    </button></a>
            @endforeach
        </div>
        <br>
    </div>

    <div style="margin: 0 4% 0 4%;">
        <!--ENVIO DE CODIGO AL CONTROLADOR-->
        @foreach ($operaciones as $op)
            <input type="hidden" name='codigo' value="{{ $op->cod_combinacion_modelo }}">
        @endforeach
        @foreach ($listaComb as $opo)
            <input type="hidden" name='codigo_modelo' value="{{ $opo->cod_modelo }}">
        @endforeach
        <input type='hidden' name='codigo_e' value=' {{ $id_comb }} '>
        <div class="x_content table-responsive">
            <table id="tablaModelos">
                <thead align="center">
                    <tr>
                        <th>N°</th>
                        <th>Proceso</th>
                        <th>Operacion</th>
                        <th>Tipo Operacion</th>
                        <th>Unidad Medida</th>
                        <th>Costo</th>
                        <th>Beneficio</th>
                        <th>Costo por Par</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody name="mano_obra_directa_tbody" id="mano_obra_directa_tbody">
                    {{-- @foreach ($operaciones as $op)
                    <tr>
                        <td> <input type="hidden" name='codigos_operacion[]' value="{{$op->cod_operacion_directa}}" >{{ $number++ }}</td>
                        <td><input type="hidden" name='codigo' value="{{ $op->cod_combinacion_modelo }}">{{ $op->nombre }}</td>
                        <td>{{ $op->operacion_nombre }}</td>
                        <td>{{ $op->nombre_pago }}</td>
                        <td>{{ $op->unidad }}</td>
                        <td>S/. {{ $op->costo }}</td>
                        @if ($op->beneficio == null)
                        <td>NA</td>
                        @else
                        <td>S/. {{ $op->beneficio }}</td>
                        @endif
                        <td>S/. {{ $op->costo_par }}</td>

                        <td> <a class="" id="" href="#" ><button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a></td>
                    </tr>

                @endforeach --}}
                </tbody>
            </table>

        </div>
    </div>
    <div class="row" style="margin: 3% 4% 2% 4%; text-align: right;">
        <div class="col-6">
        </div>
        <div class="col-4" style="text-align: right;">
            <label><strong>Costo Total de Mano de Obra Directa de Fabricación por Par:</strong></label>
        </div>
        <div class="col-2" style="text-align: center;">
                S/. <input id="total_mat" name="total_mat" readonly value=""
                    style="width:85%;margin-left: 1%; ">
        </div>
    </div>
    <div class="row" style="margin: 0 4% 0 4%; text-align: center;">
        <div class="col-12">

            <button name="boton_guardar" value="1" id="guardar_materiales" type="submit"
                class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" style="margin: 2%;"
                value="1">Guardar</button>
            @foreach ($listaComb as $lineas)
                <a class="a" href="/Produccion/combinacion_ficha/listado/{{ $lineas->cod_modelo }}">
                    <button type="button" class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-5"
                        style="margin: 2%;">Cancelar</button>
                </a>
            @endforeach
            <button type="submit" id="aprobar_materiales"
                class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" name="boton_aprobar" value="0"
                style="margin: 2%;">Aprobar <i class="fa fa-check-circle"></i></button>

        </div>
    </div>

    {{ Form::Close() }}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <style>
        .button-md {
            border-color: #212121;
            background-color: #B0BEC5;

        }

        .button-mo {
            border-color: #212121;
            background-color: #FFFFFF;
        }
    </style>
    <script type="text/javascript">
        var data = <?php echo $LineasModelos; ?>;
        var total_materiales = 0;
        var combinacionLista = <?php echo $listaComb; ?>;
        var materialesD = <?php echo $fichaMaterial; ?>;
        var materialesTodos = <?php echo $fichaMaterialTodos; ?>;
        var id_comb = <?php echo $id_comb; ?>;
        var nuevo = "prueba";
        var manos_obras_directas = "";
        var t = $("#tabla").DataTable();
        var t1 = $("#tabla").DataTable();

        var tablaModelos = $('#tablaModelos').DataTable({
            "lengthMenu": [
                [100, -1],
                [100, "All"]
            ],
            'columnDefs': [{
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });

        function getAllCombinacionManoObraDirecta() {
            $.ajax({
                type: 'GET',
                url: `{{ url('Produccion/combinacion_ficha/getmanosdirecta/${combinacionLista[0].cod_combinacion}') }}`,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    manos_obras_directas = data.mano_obra_directas;
                    $('#mano_obra_directa_tbody').empty();
                    let contador = 1;
                    $.each(manos_obras_directas, function(index, ficha_material) {
                        tablaModelos.row.add([
                            "<input type='hidden' name='codigos_operacion[]' value="+ficha_material.cod_operacion_directa+">" +contador,
                            "<input type='hidden' name='codigo' value="+ficha_material.cod_combinacion_modelo+">"+ficha_material.nombre,
                            ficha_material.operacion_nombre,
                            ficha_material.nombre_pago,
                            ficha_material.unidad,
                            ficha_material.costo,
                            ficha_material.beneficio,
                            ficha_material.costo_par,
                            "<button id="+ ficha_material.cod_mano_obra+" class='bttn-unite bttn-md bttn-danger'><i class='fas fa-trash-alt'></i></button>"
                        ]).draw();
                        contador++;
                        // "<button class='bttn-unite bttn-md bttn-danger'><i class='fas fa-trash-alt'></i></button>"
                    });
                    let total_material_directo = 0;
                    total_material_directo = sumaTotal(manos_obras_directas);
                    $('#total_mat').val( total_material_directo.toFixed(4));

                }

            })
        }

        function sumaTotal(_mano_directo) {
            let suma = 0
            _mano_directo?.map(function(element) {
                suma += (element.costo_par)
            })
            return suma;
        }

        $("#tablaModelos").DataTable();
        var listaModelos = <?php echo $LineasModelos; ?>;
        var serieModelo = <?php echo $seriemodelo; ?>;

        $("#materiales_directo").on('click', function() {
            console.log("materiales directos");
        });

        $("#mano_obra").on('click', function() {
            console.log("mano_obra");
        });


        //CIERRE MODAL CREACION COMBINACION
        $("#cancelarModal").on('click', function() {
            location.reload();
        });
        let contador = 1;

        $("#modificar_principal").on('click', function() {
            tablaModelos.rows().remove().draw();

            $.each(materialesD, function(key, value) {
                const tr = $(
                    "<tr>" +
                    "<td><input type='hidden' name='codigo_ficha[]' value=" + value.cod_ficha_m + ">" +
                    contador + "</td>" +
                    "<td><input type='hidden' name='codigo' value=" + value.cod_modelo_comb + ">" +
                    value.nombre + "</td>" +
                    "<td>" + value.descrip_material + "</td>" +
                    "<td><input name='cantidad_material[]'  type='number'  min='0' step='0.0001' placeholder='" +
                    value.consumo_real + "' id='consumo" +
                    value.cod_material + "-" + value.nombre +
                    "'style='width : 90%;' ></td>" +
                    "<td>" + value.descrip_unidad_compra + "</td>" +
                    "<td>S/." + value.costo_con_igv_material + "</td>" +
                    "<td>S/.<input name='cantidad_total[]'  type='number' value='" + value
                    .costo_por_par +
                    "' min='0' step='0.0001'  id='costo_total" +
                    value.cod_material + "-" + value.nombre +
                    "'style='width : 80%;' readonly ></td>" +
                    "</tr>"

                );
                contador++;
                tablaModelos.row.add(tr[0]).draw();
                $('#consumo' + value.cod_material + '-' + value.nombre).change(function() {

                    $('#costo_total' + value.cod_material + '-' + value.nombre)
                        .val(($(this).val() * value.costo_con_igv_material).toFixed(4));
                    total_materiales = total_materiales + $('#costo_total' + value.cod_material +
                        '-' + value.nombre).val() * 1;
                    $('#total_mat').val(total_materiales.toFixed(4));

                });
            });

        });
        //PREVISUALIZACION DE IMAGENES
        let vista_preliminar = (event) => {
            let leer_img = new FileReader();
            let id_img = document.getElementById('img-foto');

            leer_img.onload = () => {
                if (leer_img.readyState == 2) {
                    id_img.src = leer_img.result
                }
            }
            leer_img.readAsDataURL(event.target.files[0])
        }

        $("#create-combinacion").on('click', 'a.combinacion-modelo', function() {
            var nombre = "PRUEBA";
            $("#modal-crear-combinacion").modal({
                show: true
            });
            var nombre = $(this).attr("id");

            $("#modal-crear-coleccion").val(listaModelos[0].nombre_coleccion);
            $("#modal-crear-linea").val(listaModelos[0].nombre_linea);

        });
        //USO DE AJAX PARA ABRIR UNA NUEVA VENTANA CON EL ID
        $("#tablaModelos").on('click', 'a.combinacion', function() {
            var id = $(this).attr("id");
            $("#idOrdenCompra").val(id);

            $.ajax({
                url: "orden_compras/consulta/eliminar/" + id,
                success: function(html) {

                    $.each(html, function(key, value) {
                        auxiliar = 1;

                    });
                }
            });

        });
        var material_listado = "";
        $("#modelo_activo").change(function() {
            if (this.value === "ninguno") {
                window.location.reload();
            } else {
                $("#guardar_materiales").hide();
                $("#modificar_principal").hide();
                $("#agregar_materiales").hide();
                $("#aprobar_materiales").hide();

            }
            let cont = 1;
            material_listado = materialesTodos.filter(datas => datas.cod_combinacion_modelo == this.value);
            //LIMPIAR
            tablaModelos.rows().remove().draw();
            $.each(material_listado, function(key, val) {

                const tr = $(
                    "<tr>" +
                    "<td> <input type='hidden' name='codigos_operacion_d[]' value='" + val
                    .cod_operacion_directa + "'>" + cont + "</td>" +
                    "<td><input type='hidden' name='codigo' value='" + val.cod_combinacion_modelo +
                    "'>" + val.nombre + "</td>" +
                    "<td>" + val.operacion_nombre + "</td>" +
                    "<td>" + val.nombre_pago + "</td>" +
                    "<td>" + val.unidad + "</td>" +
                    "<td>S/. " + val.costo + "</td>" +

                    "<td>S/. " + val.beneficio + "</td>" +

                    "<td>S/. " + val.costo_par + "</td>" +

                    "<td><i class='fas fa-trash-alt'></i></td>" +
                    "</tr>"

                );
// "<button class='bttn-unite bttn-md bttn-danger'><i class='fas fa-trash-alt'></i></button>"
                cont++;
                tablaModelos.row.add(tr[0]).draw();

            });

        });
        $('#tablaModelos').on('click', 'button', function() {
            let _data = parseInt($(this).attr("id"))
            manos_obras_directas = eliminar_ficha_material(manos_obras_directas,_data);
            let total_material_directo = 0;
            total_material_directo = sumaTotal(manos_obras_directas);
            $('#total_mat').val( total_material_directo.toFixed(4));
            tablaModelos
                .row($(this).parents('tr'))
                .remove()
                .draw();

        });
        function eliminar_ficha_material(_ficha_materiales, _data) {
            let aux_ficha_material = []
            _ficha_materiales?.map(_ficha_material => {
                if (_ficha_material.cod_mano_obra !== _data) {
                    aux_ficha_material = [...aux_ficha_material, _ficha_material]
                }

            })
            return aux_ficha_material;
        }
        $(document).ready(function() {

            getAllCombinacionManoObraDirecta();

        })
    </script>
@endsection
