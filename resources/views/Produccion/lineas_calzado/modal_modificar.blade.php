<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-update-{{$op->cod_linea}}">
{{Form::Open(array('action'=>array('LineasCalzadoController@update',$op->cod_linea),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
          <h4 class="modal-title">Editar Línea </h4>
			</div>
			<div class="modal-body">
        <div class="x_content">
                      <div class="row ">
                      	<div class="col-md-12">
                        <input readonly maxlength="50" required="required" type="hidden" id="ruc" required="required" name="cod_linea_mod" class="form-control col-md-12" placeholder="Nombre de Linea*" value="{{$op->cod_linea}}">
                        <label for="total_orden_compra">Nombre de la línea:</label>
                          </div>
                              <div class="col-md-12">
                              <input maxlength="50" required="required" type="text" id="ruc" required="required" name="nombre_linea_mod" class="form-control col-md-12" placeholder="Nombre de Linea*" value="{{$op->nombre_linea}}">
                          </div>
                      </div>



      </div>
      <div class="modal-footer">
      <button type="submit" class="bttn-unite bttn-md bttn-primary">Confirmar</button>
        <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
      </div>
			</div>

		</div>
	</div>
	{{Form::Close()}}

</div>
