@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
      @if(count($datos))
        @foreach ($datos as $dat)
        <div class="row container">
          <div class="col-md-6">
            <h4>Datos Generales</h4>
        <div class="row">
          <div class="col-md-6">
            <p>Empresa:<input type="text" class="form-control" disabled  value="{{$dat->nom_empresa}}"></p>
            <p>Linea:<input type="text" class="form-control" name="marca" disabled required value="{{$dat->nombre_linea}}"></p>
            <p>Cod. Modelo:<input type="text" class="form-control" disabled  value="{{$dat->cod_modelo}}"> </p>
            <input type="text" class="form-control" style="display:none" name="cod_modelo"  value="{{$codigo}}">
            <p>Descripccion:<input type="text" class="form-control" disabled  value="{{$dat->descripcion}}"> </p>
          </div>
          <div class="col-md-6">
            <p>Serie: <input type="text" class="form-control" disabled  value="{{$dat->nombre_serie}}"> </p>
            <p>Tallas:<input type="text" class="form-control" disabled id="tallas"  value="{{$dat->tallaInicial}}-{{$dat->tallaFinal}}"> </p>
            <p>Talla Base:<input type="text" class="form-control" disabled id="talla_base"  value="{{number_format(($dat->tallaInicial+$dat->tallaFinal)/2,0)}}"> </p>

          </div>
        </div>
      </div>
      <div class="col-md-6">
        <h4>Norma Tecnica</h4>
        <div class="row">
          <div class="col-md-6">
            <p>Capellada:<input type="text" class="form-control" disabled  value="{{$dat->descrip_capellada}}"></p>
            <p>Forro:<input type="text" name="marca" class="form-control" disabled value="{{$dat->descrip_forro}}"></p>
            <p>Piso:<input type="text" class="form-control" disabled  value="{{$dat->descrip_piso}}"> </p>
            <p>Plantilla:<input type="text" class="form-control" disabled  value="{{$dat->descrip_plantilla}}"> </p>
          </div>
        </div>
      </div>
      @endforeach
        </div>
          <a  href="{{ url('costo/vermodelos') }}" ><button type="button" class="bttn-unite bttn-md bttn-danger">atras</button></a>
      <!--No se registro aun su costo!-->
      @else
      {!!Form::open(array('url'=>'costo/directo/norma_tecnica/create','method'=>'POST','autocomplete'=>'off'))!!}
      {{Form::token()}}
      <div class="row container">
        <div class="col-md-6">
          <h4>Datos Generales</h4>
          @foreach($modelo as $dat)
          <div class="row">
            <div class="col-md-6">
              <p>Empresa:<input type="text" class="form-control" disabled  value="{{$dat->nom_empresa}}"></p>
              <p>Linea:<input type="text" class="form-control" name="marca" disabled required value="{{$dat->nombre_linea}}"></p>
              <p>Cod. Modelo:<input type="text" class="form-control" disabled  value="{{$dat->cod_modelo}}"> </p>
              <input type="text" class="form-control" style="display:none" name="cod_modelo"  value="{{$codigo}}">
              <p>Descripcion:<input  type="text" class="form-control" disabled  value="{{$dat->descripcion}}"></p>
            </div>
            <div class="col-md-6">
              <p>Serie: <input type="text" class="form-control" name="marca" disabled required value="{{$dat->nombre_serie}}"></p>
              <p>Tallas: <input required type="text" class="form-control" disabled id="tallas"  value="{{$dat->tallaInicial}}-{{$dat->tallaFinal}}"></p>
              <p>Talla Base: <input required type="text" class="form-control" disabled id="talla_base"  value="{{number_format(($dat->tallaInicial+$dat->tallaFinal)/2,0)}}"></p>
              <p>Costeo Base:
                <select class="form-control" required name="costeo">
                  <option value="" disabled>Seleccion Costeo Base</option>
                  <option value="ninguna" selected>Ninguna</option>
                  @foreach($costos as $c)
                    <option value="{{$c->cod_costo_modelo}}">{{$c->codigo_modelo}} - {{$c->nombre_serie}}</option>
                  @endforeach
                </select>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <h4>Norma Tecnica</h4>
          <div class="row">
            <div class="col-md-6">
              <p>Capellada:<input type="text" class="form-control" disabled  value="{{$dat->descrip_capellada}}"></p>
              <p>Forro:<input type="text" name="marca" class="form-control" disabled value="{{$dat->descrip_forro}}"></p>
              <p>Piso:<input type="text" class="form-control" disabled  value="{{$dat->descrip_piso}}"> </p>
              <p>Plantilla:<input type="text" class="form-control" disabled  value="{{$dat->descrip_plantilla}}"> </p>
            </div>
          </div>
        </div>
        @endforeach
        <button type="submit" class="bttn-unite bttn-md bttn-primary" >Guardar</button>
          {!!Form::close()!!}
        <a  href="{{ url('costo/vermodelos') }}" ><button type="button" class="bttn-unite bttn-md bttn-danger">atras</button></a>
      @endif
</div>

@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
