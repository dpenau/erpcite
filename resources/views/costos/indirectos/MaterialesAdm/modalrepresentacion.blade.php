<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$rep->cod_gasrepre}}">
{!!Form::open(array('url'=>'costos_indirectos/MaterialesAdm/borrar_representacion','method'=>'POST','autocomplete'=>'off'))!!}
		{{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Gasto de representacion</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Eliminar Gasto de representacion</p>
				<input type="text" style="display:none" name="email" value="{{$rep->cod_gasrepre}}">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-slant bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-slant bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
