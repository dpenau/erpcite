@extends ('layouts.admin')
@section('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gastos Financieros</h1>
                    <div class="list-group d-inline">
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 active ml-4" id="list-home-list" data-toggle="list" href="#list-Prototipo" role="tab" aria-controls="home">Prestamos para Inversión</a>
                        <!--<a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Hormas" role="tab" aria-controls="profile">Negociaciones a Crédito</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Troqueles" role="tab" aria-controls="profile">Letras de Proveedores</a>-->
                    </div>
                    <div class="clearfix"></div>
            </div>


            <div class="row">
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">
                <h2 class="d-inline ">Intereses de Prestamos para Inversión</h2>
                <a href="financiero/crear_prestamo">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Intereses de Prestamos para Inversión</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example" class="display">
                    <thead>
                        <tr>
                        <th>Entidad Financiera</th>
                        <th>Intereses</th>
                        <th>Fecha de prestamo</th>
                        <th>Meses de pago</th>
                        <th>Meses Pagados</th>
                        <th>Meses por pagar</th>
                        <th>Costo Mensual</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>
                      <tbody>
                        @foreach($prestamo as $pres)
                        <tr>
                          <td>{{$pres->descripcion_financiera}}</td>
                          <td>{{$pres->intereses}}</td>
                          <td>{{$pres->fecha_prestamo}}</td>
                          <td>{{$pres->meses_pago}}</td>
                          <?php
                          $date1 = new DateTime($pres->fecha_prestamo);
                          $date2 = new DateTime();
                          $diff = $date1->diff($date2);
                          ?>

                          @if($pres->meses_pago-$diff->m <= 0)
                          <td>{{$pres->meses_pago}}</td>
                          <td>0</td>
                          <td>PAGADO</td>
                          @else
                          <td>{{$diff->m}}</td>
                          <td>{{$pres->meses_pago-$diff->m}}</td>
                          <td class="pgasto">{{$pres->intereses/$pres->meses_pago}}</td>
                          @endif
                          <td><a href="" data-target="#modal-active-{{$pres->cod_gasto}}" data-toggle="modal">
                           <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a></td>
                        </tr>
                        @include('costos.indirectos.Financiero.eliminar')
                        @endforeach
                      </tbody>
                    </table>
                    <h4 id="ctp">Costo Total de Desarrollo de Prototipo</h4>
                </div>
                </div>


                <div class="tab-pane fade" id="list-Hormas" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Intereses de Negociaciones a Crédito</h2>

                <a href="financiero/crear_gasto">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Intereses de Negociaciones a Crédito</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example2" class="display">
                    <thead>
                        <tr>
                        <th>Descripcion del prestamo</th>
                        <th>Interes</th>
                        <th>Meses</th>
                        <th>Gasto Mensual</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($negociacion as $pres)
                      <tr>
                        <td>{{$pres->descripcion_financiera}}</td>
                        <td>{{$pres->intereses}}</td>
                        <td>{{$pres->meses_pago}}</td>
                        <td class="hgasto">{{$pres->intereses/$pres->meses_pago}}</td>
                        <td><a href="" data-target="#modal-active-{{$pres->cod_gasto}}" data-toggle="modal">
                         <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a></td>
                      </tr>
                      @include('costos.indirectos.Financiero.eliminar')
                      @endforeach
                    </tbody>

                    </table>
                    <h4 id="cth">Costo Total de Desarrollo de Hormas</h4>
                </div>
                </div>

            <div class="tab-pane fade" id="list-Troqueles" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Intereses de Letras de Proveedores</h2>

                <a href="financiero/crear_letra">
                  <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Intereses de Letras de Proveedores</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example3" class="display">
                    <thead>
                        <tr>
                          <th>Descripcion del prestamo</th>
                          <th>Interes</th>
                          <th>Meses</th>
                          <th>Gasto Mensual</th>
                          <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($letra as $pres)
                      <tr>
                        <td>{{$pres->descripcion_financiera}}</td>
                        <td>{{$pres->intereses}}</td>
                        <td>{{$pres->meses_pago}}</td>
                        <td class="tgasto">{{$pres->intereses/$pres->meses_pago}}</td>
                        <td><a href="" data-target="#modal-active-{{$pres->cod_gasto}}" data-toggle="modal">
                         <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a></td>
                      </tr>
                      @include('costos.indirectos.Financiero.eliminar')
                      @endforeach
                    </tbody>
                    </table>
                    <h4 id="ctt">Costo Total de Intereses de Letras de Proveedores</h4>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
      var ptotal=$("td[class~='pgasto']");
      var htotales=$("td[class~='hgasto']");
      var ttotales=$("td[class~='tgasto']");
      var suma=0.00;
      var sumah=0.00;
      var sumat=0.00;
      for(var i=0;i<ptotal.length;i++)
      {
        var num=parseFloat(ptotal[i].innerText)
        suma=suma+num;
      }
      for(var i=0;i<htotales.length;i++)
      {
        var num=parseFloat(htotales[i].innerText)
        sumah=sumah+num;
      }
      for(var i=0;i<ttotales.length;i++)
      {
        var num=parseFloat(ttotales[i].innerText)
        sumat=sumat+num;
      }
      $("#ctp").html("Costo Total de Intereses de Prestamos para Inversión S/."+ suma)
      $("#cth").html("Costo Total de Intereses de Negociaciones a Crédito S/."+ sumah)
      $("#ctt").html("Costo Total de Troqueles S/."+ sumat)
    </script>

@endsection
