@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nueva Letra</h2>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
{!!Form::open(array('url'=>'costos_indirectos/financiero/guardar_gasto','method'=>'POST','autocomplete'=>'off'))!!}
    {{Form::token()}}
<div class="row">
    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
        <h6 class="my-3">Descripcion del prestamo:</h6>
        <input type="text"  name="entidad" maxlength="250" required="required" class="form-control ">
        <h6 class="my-3">Intereses:</h6>
        <input type="text" onkeypress="return isNumberKey(event)" name="intereses"  required="required" class="form-control ">
    </div>

    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
        <h6 class="my-3">Meses:</h6>
        <input type="text" onkeypress="return isNumberKey(event)"  name="meses"  required="required" class="form-control ">
        <input type="text" style="display:none" name="estado" value="3">
    </div>
</div>
<div class="form-group my-5">
    <div class="col-md-12 col-sm-6 col-xs-12">
        <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
        <a href="."><button type="button" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button></a>
    </div>
</div>
        {!!Form::close()!!}
@endsection
