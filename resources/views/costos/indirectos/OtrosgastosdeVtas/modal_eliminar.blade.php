<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-eliminar-{{$ot->id_otros}}">
{!!Form::open(array('url'=>'costos_indirectos/OtrosgastosdeVtas/eliminar_gasto','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar gasto de Ventas y Marketing</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea eliminar</p>
        <input type="text" style="display:none" name="email" value="{{$ot->id_otros}}">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
