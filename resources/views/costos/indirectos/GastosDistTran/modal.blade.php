<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$op->id_gastoDistr}}">
	{{Form::Open(array('action'=>array('GastosDistTranController@destroy',$op->id_gastoDistr
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Gasto de Distribucion y Transporte</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Eliminar</p>
				<input type="text" style="display:none" name="email" value="{{$op->id_gastoDistr}}">
        <input type="text" style="display:none" name="estado" value="0">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
