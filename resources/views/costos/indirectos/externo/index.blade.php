@extends ('layouts.admin')
@section('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
<div class="">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Personal Externo</h1>
                <a href="/costos_indirectos/externo/crear">
                    <button type="submit" class="bttn-unite float-rigth bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto externo</button>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="row">
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">
                        <div class="x_content table-responsive">
                        <table id="example" class="display">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Descripcion del puesto</th>
                                    <th>Sueldo Mensual en Planilla</th>
                                    <th>Beneficio Social en Planilla</th>
                                    <th>Otros Sueldos Mensuales</th>
                                    <th>Gasto Mensual</th>
                                    <th>Fecha de Realizacion</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($sueldo as $mano)
                                <tr>
                                    <td>
                                        @if($mano->estado==2)
                                        <div class="bg-info" style="height:20px; width:10px"></div>
                                        @endif
                                    </td>
                                    <td>{{$mano->puesto}}</td>
                                    <td>{{$mano->sueldo_mensual}}</td>
                                    <td>{{$mano->beneficios}}</td>
                                    <td>{{$mano->otros}}</td>
                                    <td class="pgasto">{{$mano->gasto_mensual}}</td>
                                    <td>{{$mano->fecha_creacion}}</td>
                                    <td>
                                        <a href="" data-target="#modal-eliminar-{{$mano->id_gastos_sueldos}}" data-toggle="modal">
                                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                    </td>
                                    </tr>
                            @include('costos.indirectos.externo.modal')
                            @endforeach
                            </tbody>
                        </table>
                        <h4 id="ctp">Costo Total de Sueldo ventas y Marketing</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    var ptotal=$("td[class~='pgasto']");
    var htotales=$("td[class~='hgasto']");
    var suma=0.00;
    var sumah=0.00;
    for(var i=0;i<ptotal.length;i++)
    {
        var num=parseFloat(ptotal[i].innerText)
        suma=suma+num;
    }
    for(var i=0;i<htotales.length;i++)
    {
        var num=parseFloat(htotales[i].innerText)
        sumah=sumah+num;
    }
    $("#ctp").html("Costo Total de Sueldo ventas y Marketing S/."+ suma.toFixed(6))
    $("#cth").html("Costo Total de Otros Gastos de Ventas S/."+ sumah.toFixed(6))
</script>

@endsection
