@extends ('layouts.admin')
@section('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Materiales Indirectos y Suministros </h1>
                <a href='material_suministro/create'>
                <button type="submit" class="bttn-unite bttn-md bttn-success ml-2"  id="sub">Nuevo</button></a>
                <div class="clearfix"></div>
            </div>
            <div id="buscando" style="display:none; margin:20px;">
              <div class="alert alert-success" role="alert">
                Actualizando Constos...
              </div>
            </div>
            <span class="badge badge-info">Precio desactualizado</span>
            <div class="x_content table-responsive">
                <table id="example" class="display">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Área</th>
                            <th>Descripción de Gasto</th>
                            <th>Unidad de C</th>
                            <th>Valor Unitario</th>
                            <th>Consumo</th>
                            <th>Meses duracion</th>
                            <th>Gasto Mensual</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($materialsuministro as $matsu)

                        <?php $n=$matsu->descrip_area; ?>
                        @if($n=="Desarrollo de Producto" || $n=="Logística" || $n=="P-Acabado"
                          || $n=="P-Alistado" || $n=="P-Aparado" || $n=="P-Corte" || $n=="P-Habilitado" || $n=="P-Montaje"
                        || $n=="Seguridad y Limpieza")
                        <td>
                          @if($matsu->estado_suministro==3 || $matsu->estado_suministro==2)
                            <div class="bg-info" style="height:20px; width:10px"></div>
                          @endif
                        </td>
                              <td>{{$matsu->descrip_area}}</td>
                              <td>{{$matsu->descrip_material}}</td>
                              <td>{{$matsu->descrip_unidad_compra}}</td>
                              <td>{{$matsu->costo_sin_igv_material}}</td>
                              <td>{{$matsu->consumo}}</td>
                              <td>{{$matsu->meses_duracion}}</td>
                              <td class="gasto">{{$matsu->gasto_mensual_suministro}}</td> 

                              <td>
                                  <a href="" data-target="#modal-eliminar-{{$matsu->codigo_suministro}}" data-toggle="modal">
                                  <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                              </td>
                          </tr>
                          @include('costos.indirectos.material_suministro.eliminar')
                        @endif
                        @endforeach
                    </tbody>
                </table>
                <h4 id="costo_total">Costo Total de Materiales Indirectos y Suministros</h4>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){

          var totales=$("td[class~='gasto']");
          var suma=0.00;
          for(var i=0;i<totales.length;i++)
          {
            var num=parseFloat(totales[i].innerText)
            suma=suma+num;
          }
          $("#costo_total").html("Costo Total de Materiales Indirectos y Suministros S/."+ suma.toFixed(5))
        });
    </script>
@endsection
