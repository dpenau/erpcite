@extends ('layouts.admin')
@section('contenido')
<div class="preloader">

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  @if(count($detalle)>0)
      {!!Form::open(array('url'=>'costo/indirecto/material_suministro/updateall','method'=>'PUT','autocomplete'=>'off'))!!}
      {{Form::token()}}
      <script type="text/javascript">
        $(document).ready(function(){
          @foreach($detalle as $mat)
            var area="{{$mat->descrip_area}}";
            <?php
            $precio=$mat->costo_sin_igv_material;
            if($mat->t_moneda==1)
            $precio=$precio*$cambio[0]->valor;
            ?>
            switch (area)
            {
              case "Logística":
              $('#Logistica> tbody:last-child').append("<tr id='{{$mat->cod_material}}Logísticav'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}Logísticac' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}Logísticat' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block logistica' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}Logísticapor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='Logística'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}Logísticab'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}Logísticat").off("focusout");
              $("#{{$mat->cod_material}}Logísticat").on("focusout",function(){
                var consumo=$("#{{$mat->cod_material}}Logísticac").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}Logísticat").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}Logísticapor").val(tot);
              });
              $("#{{$mat->cod_material}}Logísticab").off("click");
              $("#{{$mat->cod_material}}Logísticab").on("click",function(){
                $("#{{$mat->cod_material}}Logísticav").remove();
              });
              break;
              case "Seguridad y Limpieza":
              $('#Seguridad> tbody:last-child').append("<tr id='{{$mat->cod_material}}Seguridadv'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}Seguridadc' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}Seguridadt' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block seguridad' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}Seguridadpor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='Seguridad y Limpieza'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}Seguridadb'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}Seguridadt").off("focusout");
              $("#{{$mat->cod_material}}Seguridadt").on("focusout",function(){
                var consumo=$("#{{$mat->cod_material}}Seguridadc").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}Seguridadt").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}Seguridadpor").val(tot);
              });
              $("#{{$mat->cod_material}}Seguridadb").off("click");
              $("#{{$mat->cod_material}}Seguridadb").on("click",function(){
                $("#{{$mat->cod_material}}Seguridadv").remove();
              });
              break;
              case "Desarrollo de Producto":
              $('#d_producto> tbody:last-child').append("<tr id='{{$mat->cod_material}}Desarrollov'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control ' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}Desarrolloc' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}Desarrollot' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block desarrollo' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}Desarrollopor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='Desarrollo de Producto'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}Desarrollob'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}Desarrollot").off("focusout");
              $("#{{$mat->cod_material}}Desarrollot").on("focusout",function(){
                var consumo=$("#{{$mat->cod_material}}Desarrolloc").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}Desarrollot").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}Desarrollopor").val(tot);
              });
              $("#{{$mat->cod_material}}Desarrollob").off("click");
              $("#{{$mat->cod_material}}Desarrollob").on("click",function(){
                $("#{{$mat->cod_material}}Desarrollov").remove();
              });
              break;
              case "P-Acabado":
                $('#P-Acabado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Acabadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
                +"<td><input class='form-control ' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
                +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Acabadoc' value='{{$mat->consumo}}' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Acabadot' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
                +"<td><input class='form-control block acabado' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}P-Acabadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Acabado'></td>"
                +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Acabadob'>-</a></td>"
                +"</tr>");
                $("#{{$mat->cod_material}}P-Acabadot").off("focusout");
                $("#{{$mat->cod_material}}P-Acabadot").on("focusout",function(){
                  var consumo=$("#{{$mat->cod_material}}P-Acabadoc").val();
                  var valor="{{$precio}}";
                  var meses=$("#{{$mat->cod_material}}P-Acabadot").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#{{$mat->cod_material}}P-Acabadopor").val(tot);
                });
                $("#{{$mat->cod_material}}P-Acabadob").off("click");
                $("#{{$mat->cod_material}}P-Acabadob").on("click",function(){
                  $("#{{$mat->cod_material}}P-Acabadov").remove();
                });
              break;
              case "P-Aparado":
              $('#P-Aparado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Aparadov'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control ' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Aparadoc' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Aparadot' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block aparado' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}P-Aparadopor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Aparado'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Aparadob'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}P-Aparadot").off("focusout");
              $("#{{$mat->cod_material}}P-Aparadot").on("focusout",function(){
                var consumo=$("#{{$mat->cod_material}}P-Aparadoc").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}P-Aparadot").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}P-Aparadopor").val(tot);
              });
              $("#{{$mat->cod_material}}P-Aparadob").off("click");
              $("#{{$mat->cod_material}}P-Aparadob").on("click",function(){
                $("#{{$mat->cod_material}}P-Aparadov").remove();
              });
              break;
              case "P-Corte":
              $('#P-Corte> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Cortev'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control ' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Cortec' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Cortet' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block corte' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}P-Cortepor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Corte'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Corteb'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}P-Cortet").off("focusout");
              $("#{{$mat->cod_material}}P-Cortet").on("focusout",function(){

                var consumo=$("#{{$mat->cod_material}}P-Cortec").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}P-Cortet").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}P-Cortepor").val(tot);
              });
              $("#{{$mat->cod_material}}P-Corteb").off("click");
              $("#{{$mat->cod_material}}P-Corteb").on("click",function(){
                $("#{{$mat->cod_material}}P-Cortev").remove();
              });
              break;
              case "P-Habilitado":
              $('#P-Habilitado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Habilitadov'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Habilitadoc' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Habilitadot' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block habilitado' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}P-Habilitadopor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Habilitado'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Habilitadob'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}P-Habilitadot").off("focusout");
              $("#{{$mat->cod_material}}P-Habilitadot").on("focusout",function(){
                var consumo=$("#{{$mat->cod_material}}P-Habilitadoc").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}P-Habilitadot").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}P-Habilitadopor").val(tot);
              });
              $("#{{$mat->cod_material}}P-Habilitadob").off("click");
              $("#{{$mat->cod_material}}P-Habilitadob").on("click",function(){
                $("#{{$mat->cod_material}}P-Habilitadov").remove();
              });
              break;
              case "P-Alistado":
              $('#P-Alistado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Alistadov'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control ' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Alistadoc' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Alistadot' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block alistado' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}P-Alistadopor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Alistado'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Alistadob'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}P-Alistadot").off("focusout");
              $("#{{$mat->cod_material}}P-Alistadot").on("focusout",function(){
                var consumo=$("#{{$mat->cod_material}}P-Alistadoc").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}P-Alistadot").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}P-Alistadopor").val(tot);
              });
              $("#{{$mat->cod_material}}P-Alistadob").off("click");
              $("#{{$mat->cod_material}}P-Alistadob").on("click",function(){
                $("#{{$mat->cod_material}}P-Alistadov").remove();
              });
              break;
              case "P-Montaje":
              $('#P-Montaje> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Montajev'>"
              +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
              +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
              +"<td><input class='form-control ' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
              +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Montajec' value='{{$mat->consumo}}' name='cantidad[]'></td>"
              +"<td><input required class='form-control' type='text' id='{{$mat->cod_material}}P-Montajet' value='{{$mat->meses_duracion}}' name='meses[]' ></td>"
              +"<td><input class='form-control block montaje' disabled type='text' value='{{$mat->gasto_mensual_suministro}}' id='{{$mat->cod_material}}P-Montajepor' name='porcentaje[]' ></td>"
              +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Montaje'></td>"
              +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Montajeb'>-</a></td>"
              +"</tr>");
              $("#{{$mat->cod_material}}P-Montajet").off("focusout");
              $("#{{$mat->cod_material}}P-Montajet").on("focusout",function(){
                var consumo=$("#{{$mat->cod_material}}P-Montajec").val();
                var valor="{{$precio}}";
                var meses=$("#{{$mat->cod_material}}P-Montajet").val();
                var tot=0.00000;
                tot=(parseFloat(valor).toFixed(6)* parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6);
                tot=tot.toFixed(6);
                $("#{{$mat->cod_material}}P-Montajepor").val(tot);
              });
              $("#{{$mat->cod_material}}P-Montajeb").off("click");
              $("#{{$mat->cod_material}}P-Montajeb").on("click",function(){
                $("#{{$mat->cod_material}}P-Montajev").remove();
              });
              default:
            }
            var espacio=area.split(" ")
            @if($mat->estado_suministro==3 || $mat->estado_suministro==2)
            $("#{{$mat->cod_material}}"+espacio[0]+"v").addClass("bg-info")
            @endif
          @endforeach
        });
      </script>
  @else
      {!!Form::open(array('url'=>'costo/indirecto/material_suministro/create','method'=>'POST','autocomplete'=>'off'))!!}
      {{Form::token()}}
  @endif
  <div class="right_col" role="main">
    <div class="border-bottom">
      <div class="row">
        <div class="col-md-6">
          <h1>Materiales Indirectos y Suministros</h1>
        </div>
        <div class="col-md-6">
          <h3 id="material_total">Costo Total mensual: S/.</h3>
          <input style="display:none" id="mt"  name="total_materiales" class="form-control" value="">
        </div>
      </div>
      <span class="badge badge-info">Precio desactualizado</span>
      <hr>
      <div class="row">
        <div class="col-md-3">
          <h4>Area a agregar material</h4>
          <select   id="area"  class="custom-select">
            <option value="0" selected disabled>Area</option>
          </select>
        </div>
        <script type="text/javascript">
          var select=""
          @foreach($area as $ar)
            var n="{{$ar->cod_area}}"
            if(n=="83307" || n=="47812" || n=="11297"
          || n=="79770" || n=="27188" || n=="27405" || n=="30844" || n=="83205"
        || n=="63488")
        {
          select+="<option value={{$ar->descrip_area}} >{{$ar->descrip_area}}</option>"
        }
          @endforeach
          $("#area").append(select);
        </script>
        <div class="col-md-3">
          <h4>Categoria</h4>
          <select   id="categoria" class="custom-select">
            <option value="" selected disabled>Categoria</option>
            @foreach($categorias as $cat)
              <option value={{$cat->cod_categoria}} >{{$cat->nom_categoria}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3">
          <h4>Subcategoria</h4>
          <select   id="sub-categoria" class="custom-select">
            <option value="" selected disabled>Subcategoria</option>
          </select>
        </div>
        <div class="col-md-3">
          <h4>Materiales</h4>
          <div id="materiales" style="overflow-y:scroll;height:150px">
          </div>
        </div>
      </div>
      <div id="alerta" style="display:none; margin:20px;">
        <div class="alert alert-danger" role="alert">
          Ya agrego ese Material
        </div>
      </div>
      <div id="buscando" style="display:none; margin:20px;">
        <div class="alert alert-success" role="alert">
          Obteniendo materiales...
        </div>
      </div>
    </div>
    <hr>
    <div class="">
      <h2>Area: Desarrollo de Producto</h2>
      <div class="x_content table-responsive">
        <table id="d_producto" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="desarrollo_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Corte</h2>
      <div class="x_content table-responsive">
        <table id="P-Corte" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="corte_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Habilitado</h2>
      <div class="x_content table-responsive">
        <table id="P-Habilitado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="habilitado_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Aparado</h2>
      <div class="x_content table-responsive">
        <table id="P-Aparado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="aparado_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Alistado</h2>
      <div class="x_content table-responsive">
        <table id="P-Alistado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="alistado_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Montaje</h2>
      <div class="x_content table-responsive">
        <table id="P-Montaje" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="montaje_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Acabado</h2>
      <div class="x_content table-responsive">
        <table id="P-Acabado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="acabado_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Logistica</h2>
      <div class="x_content table-responsive">
        <table id="Logistica" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="logistica_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Seguridad y Limpieza</h2>
      <div class="x_content table-responsive">
        <table id="Seguridad" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo</th>
              <th>Meses de duracion</th>
              <th>Gasto mensual</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="seguridad_txt">Total:</h5>
    </div>
    <p>
      <a  id="btn_total" > <button  type="button" class="bttn-unite bttn-md bttn-primary" >Calcular Total</button></a></p>
    <button type="submit" class="bttn-unite bttn-md bttn-primary" target="_blank" id="sub">Guardar</button>
      <a  href="{{ url('costo/vermodelos') }}" ><button type="button" class="bttn-unite bttn-md bttn-danger " >Cancelar</button></a>
      {!!Form::close()!!}
  </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $( "#categoria" ).change(function() {
    if($("#area").val()==null)
    {
      alert("Escoga un Area primero")
    }
    else {
      $("#materiales").empty();
        let valor=$("#categoria").val();
        switch (valor)
        {
          case "306":
          $("#sub-categoria").empty();
          $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
            @foreach($subcategorias as $sub)
              @if($sub->cod_categoria==306)
                  $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
              @endif
            @endforeach
          break;
          case "634":
          $("#sub-categoria").empty();
          $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
            @foreach($subcategorias as $sub)
              @if($sub->cod_categoria==634)
                  $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
              @endif
            @endforeach
          break;
          case "969":
          $("#sub-categoria").empty();
          $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
            @foreach($subcategorias as $sub)
              @if($sub->cod_categoria==969)
                  $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
              @endif
            @endforeach
          break;
        }
    }
  });
  $("#sub-categoria").change(function(){
    var valor=$("#sub-categoria").val();
    $("#materiales").empty();
    obtener_materiales(valor);
  });
  function obtener_materiales(valor)
  {
    $("#buscando").toggle("slow");
    $.ajax({
      url: "/material_suministro/obtener/"+valor,
      success: function(html){
        $("#buscando").toggle("slow");
        crear_opciones(html);
      }
    });
  }
  function crear_opciones(datos)
  {
    var data=datos;
    var flag=0;
      var nombre_anterior="";
      var costo_anterior="";
    $.each(data,function(key,value){
      var sub=value.cod_subcategoria;
      var tipo=value.t_compra;
        if(tipo==1)
        {
          var nombre=value.descrip_material;
          var costo=value.costo_sin_igv_material;
          nombre=nombre.split('-');
          var nomb=nombre[0];
          if(nombre_anterior==nombre[0] && costo_anterior==costo)
          {
              flag=1;
          }
          else {
            flag=0;
            nombre_anterior=nombre[0];
            costo_anterior=costo;
          }
        }
        else {
          flag=0;
          var nomb=value.descrip_material;
        }
        if(flag==0)
        {
        $("#materiales").append("<div class='custom-control custom-checkbox'><input  type='button'  id='"+value.cod_material+"' value='"+nomb+"'></div>");
        $("#"+value.cod_material).off("click");
        $("#"+value.cod_material).on("click",function(){
          if(!$(this).checked)
          {
            let area=$("#area").val();
            if ( !$("#"+value.cod_material+area+"v").length > 0 ) {
              var precio=value.costo_sin_igv_material;
              let cambio="{{$cambio[0]->valor}}";
              if(value.t_moneda==1)
              {
                precio=parseFloat(precio)*parseFloat(cambio)
              }
              switch (area) {
                case "Logística":
                $('#Logistica> tbody:last-child').append("<tr id='"+value.cod_material+"Logísticav'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"Logísticac' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"Logísticat' name='meses[]' ></td>"
                +"<td><input class='form-control block logistica' disabled type='text' id='"+value.cod_material+"Logísticapor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='Logística'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"Logísticab'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"Logísticat").off("focusout");
                $("#"+value.cod_material+"Logísticat").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"Logísticac").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"Logísticat").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"Logísticapor").val(tot);
                });
                $("#"+value.cod_material+"Logísticab").off("click");
                $("#"+value.cod_material+"Logísticab").on("click",function(){
                  $("#"+value.cod_material+"Logísticav").remove();
                });
                break;
                case "Seguridad":
                $('#Seguridad> tbody:last-child').append("<tr id='"+value.cod_material+"Seguridadv'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"Seguridadc' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"Seguridadt' name='meses[]' ></td>"
                +"<td><input class='form-control block seguridad' disabled type='text' id='"+value.cod_material+"Seguridadpor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='Seguridad y Limpieza'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"Seguridadb'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"Seguridadt").off("focusout");
                $("#"+value.cod_material+"Seguridadt").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"Seguridadc").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"Seguridadt").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"Seguridadpor").val(tot);
                });
                $("#"+value.cod_material+"Seguridadb").off("click");
                $("#"+value.cod_material+"Seguridadb").on("click",function(){
                  $("#"+value.cod_material+"Seguridadv").remove();
                });
                break;
                case "Desarrollo":
                $('#d_producto> tbody:last-child').append("<tr id='"+value.cod_material+"Desarrollov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"Desarrolloc' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"Desarrollot' name='meses[]' ></td>"
                +"<td><input class='form-control block desarrollo' disabled type='text' id='"+value.cod_material+"Desarrollopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='Desarrollo de Producto'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"Desarrollob'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"Desarrollot").off("focusout");
                $("#"+value.cod_material+"Desarrollot").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"Desarrolloc").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"Desarrollot").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"Desarrollopor").val(tot);
                });
                $("#"+value.cod_material+"Desarrollob").off("click");
                $("#"+value.cod_material+"Desarrollob").on("click",function(){
                  $("#"+value.cod_material+"Desarrollov").remove();
                });
                break;
                case "P-Acabado":
                  $('#P-Acabado> tbody:last-child').append("<tr id='"+value.cod_material+"P-Acabadov'>"
                  +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                  +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                  +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                  +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                  +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Acabadoc' name='cantidad[]'></td>"
                  +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Acabadot' name='meses[]' ></td>"
                  +"<td><input class='form-control block acabado' disabled type='text' id='"+value.cod_material+"P-Acabadopor' name='porcentaje[]' ></td>"
                  +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Acabado'></td>"
                  +"<td><a class='btn btn-danger' id='"+value.cod_material+"P-Acabadob'>-</a></td>"
                  +"</tr>");
                  $("#"+value.cod_material+"P-Acabadot").off("focusout");
                  $("#"+value.cod_material+"P-Acabadot").on("focusout",function(){
                    var consumo=$("#"+value.cod_material+"P-Acabadoc").val();
                    var valor=precio;
                    var meses=$("#"+value.cod_material+"P-Acabadot").val();
                    var tot=0.00000;
                    tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                    tot=tot.toFixed(6);
                    $("#"+value.cod_material+"P-Acabadopor").val(tot);
                  });
                  $("#"+value.cod_material+"P-Acabadob").off("click");
                  $("#"+value.cod_material+"P-Acabadob").on("click",function(){
                    $("#"+value.cod_material+"P-Acabadov").remove();
                  });
                break;
                case "P-Aparado":
                $('#P-Aparado> tbody:last-child').append("<tr id='"+value.cod_material+"P-Aparadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Aparadoc' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Aparadot' name='meses[]' ></td>"
                +"<td><input class='form-control block aparado' disabled type='text' id='"+value.cod_material+"P-Aparadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Aparado'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"P-Aparadob'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"P-Aparadot").off("focusout");
                $("#"+value.cod_material+"P-Aparadot").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"P-Aparadoc").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"P-Aparadot").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"P-Aparadopor").val(tot);
                });
                $("#"+value.cod_material+"P-Aparadob").off("click");
                $("#"+value.cod_material+"P-Aparadob").on("click",function(){
                  $("#"+value.cod_material+"P-Aparadov").remove();
                });
                break;
                case "P-Corte":
                $('#P-Corte> tbody:last-child').append("<tr id='"+value.cod_material+"P-Cortev'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Cortec' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Cortet' name='meses[]' ></td>"
                +"<td><input class='form-control block corte' disabled type='text' id='"+value.cod_material+"P-Cortepor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Corte'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"P-Corteb'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"P-Cortet").off("focusout");
                $("#"+value.cod_material+"P-Cortet").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"P-Cortec").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"P-Cortet").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"P-Cortepor").val(tot);
                });
                $("#"+value.cod_material+"P-Corteb").off("click");
                $("#"+value.cod_material+"P-Corteb").on("click",function(){
                  $("#"+value.cod_material+"P-Cortev").remove();
                });
                break;
                case "P-Habilitado":
                $('#P-Habilitado> tbody:last-child').append("<tr id='"+value.cod_material+"P-Habilitadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Habilitadoc' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Habilitadot' name='meses[]' ></td>"
                +"<td><input class='form-control block habilitado' disabled type='text' id='"+value.cod_material+"P-Habilitadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Habilitado'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"P-Habilitadob'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"P-Habilitadot").off("focusout");
                $("#"+value.cod_material+"P-Habilitadot").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"P-Habilitadoc").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"P-Habilitadot").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"P-Habilitadopor").val(tot);
                });
                $("#"+value.cod_material+"P-Habilitadob").off("click");
                $("#"+value.cod_material+"P-Habilitadob").on("click",function(){
                  $("#"+value.cod_material+"P-Habilitadov").remove();
                });
                break;
                case "P-Alistado":
                $('#P-Alistado> tbody:last-child').append("<tr id='"+value.cod_material+"P-Alistadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Alistadoc' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Alistadot' name='meses[]' ></td>"
                +"<td><input class='form-control block alistado' disabled type='text' id='"+value.cod_material+"P-Alistadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Alistado'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"P-Alistadob'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"P-Alistadot").off("focusout");
                $("#"+value.cod_material+"P-Alistadot").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"P-Alistadoc").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"P-Alistadot").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"P-Alistadopor").val(tot);
                });
                $("#"+value.cod_material+"P-Alistadob").off("click");
                $("#"+value.cod_material+"P-Alistadob").on("click",function(){
                  $("#"+value.cod_material+"P-Alistadov").remove();
                });
                break;
                case "P-Montaje":
                $('#P-Montaje> tbody:last-child').append("<tr id='"+value.cod_material+"P-Montajev'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+value.cod_material+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Montajec' name='cantidad[]'></td>"
                +"<td><input required class='form-control' type='text' id='"+value.cod_material+"P-Montajet' name='meses[]' ></td>"
                +"<td><input class='form-control block montaje' disabled type='text' id='"+value.cod_material+"P-Montajepor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Montaje'></td>"
                +"<td><a class='btn btn-danger' id='"+value.cod_material+"P-Montajeb'>-</a></td>"
                +"</tr>");
                $("#"+value.cod_material+"P-Montajet").off("focusout");
                $("#"+value.cod_material+"P-Montajet").on("focusout",function(){
                  var consumo=$("#"+value.cod_material+"P-Montajec").val();
                  var valor=precio;
                  var meses=$("#"+value.cod_material+"P-Montajet").val();
                  var tot=0.00000;
                  tot=(parseFloat(valor).toFixed(6)*parseFloat(consumo).toFixed(6))/parseFloat(meses).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+value.cod_material+"P-Montajepor").val(tot);
                });
                $("#"+value.cod_material+"P-Montajeb").off("click");
                $("#"+value.cod_material+"P-Montajeb").on("click",function(){
                  $("#"+value.cod_material+"P-Montajev").remove();
                });
                default:
              }
            }
            else {
                $("#alerta").toggle("slow");
              setTimeout(function() {
                    $("#alerta").hide("slow");
                }, 5000);
            }
          }
        });
      }
    })
  }
  $("#sub").click(function(event){
    $("#btn_total").click();
    $(".block").prop('disabled',false);
  });
  $("#btn_total").click(function(){
    var logistica=$("input[class~='logistica']");
    var desarrollo=$("input[class~='desarrollo']");
    var seguridad=$("input[class~='seguridad']");
      var totales=$("input[class~='acabado']");
      var cortes=$("input[class~='corte']");
      var habilitado=$("input[class~='habilitado']");
      var aparado=$("input[class~='aparado']");
      var alistado=$("input[class~='alistado']");
      var montaje=$("input[class~='montaje']");
      var totallogistica=0.00,totaldesarrollo=0.00,totalseguridad=0.00,total=0.00,totalcorte=0.00,totalhabilitado=0.00,totalaparado=0.00,totalalistado=0.00,totalmontaje=0.00;
      for (var i = 0; i < logistica.length; i++) {
        var valor=logistica[i].value;
        totallogistica=parseFloat(valor)+parseFloat(totallogistica);
      }
      for (var i = 0; i < desarrollo.length; i++) {
        var valor=desarrollo[i].value;
        totaldesarrollo=parseFloat(valor)+parseFloat(totaldesarrollo);
      }
      for (var i = 0; i < seguridad.length; i++) {
        var valor=seguridad[i].value;
        totalseguridad=parseFloat(valor)+parseFloat(totalseguridad);
      }
      for (var i = 0; i < cortes.length; i++) {
        var valor=cortes[i].value;
        totalcorte=parseFloat(valor)+parseFloat(totalcorte);
      }
      for (var i = 0; i < habilitado.length; i++) {
        var valor=habilitado[i].value;
        totalhabilitado=parseFloat(valor)+parseFloat(totalhabilitado);
      }
      for (var i = 0; i < aparado.length; i++) {
        var valor=aparado[i].value;
        totalaparado=parseFloat(valor)+parseFloat(totalaparado);
      }
      for (var i = 0; i < alistado.length; i++) {
        var valor=alistado[i].value;
        totalalistado=parseFloat(valor)+parseFloat(totalalistado);
      }
      for (var i = 0; i < montaje.length; i++) {
        var valor=montaje[i].value;
        totalmontaje=parseFloat(valor)+parseFloat(totalmontaje);
      }
      for(var i=0;i<totales.length;i++)
       {
         var valor=totales[i].value;
         total=parseFloat(valor)+parseFloat(total);
      }
      $("#corte_txt").text("Total: S/."+totalcorte.toFixed(6))
      $("#habilitado_txt").text("Total: S/."+totalhabilitado.toFixed(6))
      $("#aparado_txt").text("Total: S/."+totalaparado.toFixed(6))
      $("#alistado_txt").text("Total: S/."+totalalistado.toFixed(6))
      $("#montaje_txt").text("Total: S/."+totalmontaje.toFixed(6))
      $("#acabado_txt").text("Total: S/."+total.toFixed(6))
      $("#desarrollo_txt").text("Total: S/."+totaldesarrollo.toFixed(6))
      $("#logistica_txt").text("Total S/."+totallogistica.toFixed(6))
      $("#seguridad_txt").text("Total S/."+totalseguridad.toFixed(6))
      var t=totalcorte+totalhabilitado+totalaparado+totalalistado+totalmontaje+total+totallogistica+totalseguridad+totaldesarrollo;
      $("#material_total").text("Costo Total Mensual S/. "+t.toFixed(6))
      $("#mt").val(t)
  });
  @if(count($detalle)>0)
    $("#btn_total").click();
  @endif
});

</script>
@endsection
