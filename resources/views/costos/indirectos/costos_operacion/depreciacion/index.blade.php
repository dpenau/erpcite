<div class="pb-3">
    <h3 class="font-weight-bold">Depreciación y Mantenimiento
        <a href="" data-target="#modal-create-depreciacion" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Activo</button></a>
        <a href="#">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Reporte</button></a>

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <table id="depreciacion" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Tipo Material</th>
                <th>Año de Depreciación</th>
                <th>Costo Unitario</th>
                <th>Número de Activos</th>
                <th>Años Depreciados</th>
                <th>C. Depreciación Mensual</th>
                <th>Costo por Mnto</th>
                <th>Frecuencia Anual</th>
                <th>Costo Mantenimiento</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.depreciacion.modalcrear')

</div>
