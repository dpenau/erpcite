<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-eliminar-material-{{ $item->id }}">
    <form method="POST" action="{{ url('costos/indirectos/operacion/materialindirecto/delete/' . $item->id) }}">
        <!-- CSRF Token -->
        @method('delete')
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Material Indirecto</h4>
                </div>
                <div class="modal-body">
                    <p>Confirme si desea Eliminar el Material</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
