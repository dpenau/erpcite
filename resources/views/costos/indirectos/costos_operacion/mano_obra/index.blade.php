<div class="pb-3">
    <h3 class="font-weight-bold">Mano de Obra Indirecta
        <a href="" data-target="#modal-create-mano" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Mano de Obra Directa</button></a>
        <a href="#">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Reporte</button></a>

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <table id="mano_obra" class="display">
        <thead>
            <tr>
                <th>Proceso/Área</th>
                <th>Descripción de Puesto</th>
                <th>Sueldo Mensual Planilla</th>
                <th>Beneficio Social</th>
                <th>Otros Sueldos Mensuales</th>
                <th>Costo Mensual</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.mano_obra.modalcrear')

</div>
