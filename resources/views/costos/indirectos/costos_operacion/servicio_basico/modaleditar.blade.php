<div id="modal-editar-servicio" class="modal fade modal-slide-in-right" aria-hidden="true"
    role="dialog" tabindex="-1">
    <!-- CSRF Token -->
    @method('PUT')
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar Servicio Básico</h4>
                <input type="hidden" id="idserviciobasico" value="">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-12 ">
                        <label for="proceso">Proceso: </label>
                        <select id="proceso_id" name="proceso_idedit" class="custom-select" required>
                            <option value="" selected disabled>Procesos</option>
                            @foreach ($procesos as $proceso)
                                    <option value="{{ $proceso->cod_proceso }}">
                                        {{ $proceso->nombre }}
                                    </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-12 ">
                        <label for="descripcionedit">Descripcion del Servicio: </label>
                        <input id="descripcionedit" type="text" class="form-control"name="descripcion"
                            required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="costo_mensual_edit">Costo Mensual:</label>
                        <div class=" input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">S/.</span>
                            </div>
                            <input type="number" id="costomensualedit" name="costo_mensual_edit" class="form-control"
                                step="0.0001" min="0"  required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="bttn-unite bttn-md bttn-primary" data-dismiss="modal"
                    onclick="editServicioBasico()">Guardar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
