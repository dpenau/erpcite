<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-imagen-{{$mat->cod_modelo}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">{{$mat->cod_modelo}}</h4>
			</div>
			<div class="modal-body">
        <div class="border border-dark" align="center">
          {{Html::image('photo/modelos/'.$empresa.'/'.$mat->imagen,$mat->nombre,array( 'width' => 400, 'height' => 400 ))}}
        </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
