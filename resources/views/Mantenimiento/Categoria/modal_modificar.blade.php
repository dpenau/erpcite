<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$clas->cod_categoria}}">
	{{Form::Open(array('action'=>array('CategoriaController@update',$clas->cod_categoria
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Categoria</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_categoria_editar" value="{{$clas->cod_categoria}}">
				<div class="form-group  ">
          <label for="total_orden_compra">Codigo de Categoria:</label>
           <input type="text" class="form-control" disabled value="{{$clas->cod_categoria}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion de Categoria:</label>
           <input type="text" class="form-control" name="descripcion" value="{{$clas->nom_categoria}}">
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
