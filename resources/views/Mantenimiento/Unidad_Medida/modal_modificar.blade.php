<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-modificar-{{$lab->cod_unidad_medida}}">
	{{Form::Open(array('action'=>array('UnidadMedidaController@update',$lab->cod_unidad_medida
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Categoria</h4>
			</div>
			<div class="modal-body">
        <input type="text" name="cod" style="display:none" value="{{$lab->cod_unidad_medida}}">
        <div class="form-group row"  >
            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre:</label>
            <div class="col-md-6">
                <input id="nombre" type="text" class="form-control" name="nombre" value="{{$lab->unidad}}" required>
            </div>
        </div>
        <div class="form-group row"  >
            <label for="nombre" class="col-md-4 col-form-label text-md-right">Descripción:</label>
            <div class="col-md-6">
                <input id="nombre" type="text" class="form-control" name="descrip" value="{{$lab->descrip_unidad_medida}}" required>
            </div>
        </div>
        <div class="form-group row"  >
            <label for="nombre" class="col-md-4 col-form-label text-md-right">Magnitud:</label>
            <div class="col-md-6">
                <input id="nombre" type="text" class="form-control" name="magni" value="{{$lab->magnitud_unidad_medida}}" required>
            </div>
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
