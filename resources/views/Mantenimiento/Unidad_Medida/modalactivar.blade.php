<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-active-{{$lab->cod_unidad_medida}}">
	{{Form::Open(array('action'=>array('UnidadMedidaController@destroy',$lab->cod_unidad_medida),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Activar Unidad de medida</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">x
                </button>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Activar la unidad de medida</p>
        <input type="text" style="display:none" name="unidad" value="{{$lab->cod_unidad_medida}}">
        <input type="text" style="display:none" name="accion" value="1">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
