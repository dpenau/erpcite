@extends('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Subcategorias <a href="Subcategoria/create">
      <button class="bttn-unite bttn-md bttn-success ">Nueva Subcategoria</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Editar</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($clasificacion as $clas)
                    @if($clas->estado_subcategoria==0)
                    <tr class="bg-danger">
                    @endif
                    	<td>{{$clas->nom_subcategoria}}</td>
                      <td>
                         <a href="" data-target="#modal-edit-{{$clas->cod_subcategoria}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
                      </td>
                      <td>
                        @if($clas->estado_subcategoria==1)
                         <a href="" data-target="#modal-delete-{{$clas->cod_subcategoria}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-danger"><i class="far fa-trash-alt"></i></button></a>
                        @else
                        <a href="" data-target="#modal-delete-{{$clas->cod_subcategoria}}" data-toggle="modal">
                         <button class="bttn-unite bttn-md bttn-success"><i class="far fa-check-circle"></i></button></a>
                        @endif
                      </td>
                    </tr>
                    @include('Mantenimiento.Subcategoria.modalactivar')
                    @include('Mantenimiento.Subcategoria.modaleditar')
                    @include('Mantenimiento.Subcategoria.modaleliminar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
