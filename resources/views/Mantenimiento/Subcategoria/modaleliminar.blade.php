<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$clas->cod_subcategoria}}">
	{{Form::Open(array('action'=>array('ClasificacionController@destroy',$clas->cod_subcategoria
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar Subcategoria</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_eliminar" value="{{$clas->cod_subcategoria}}">
				        <input type="text" style="display:none" name="accion" value="0">
				<p>Esta seguro que desea desactivar la subcategoria: {{$clas->nom_subcategoria}}</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
