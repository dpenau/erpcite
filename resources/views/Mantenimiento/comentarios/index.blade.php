@extends('layouts.app')
@section ('content')
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Comentarios <a href="{!! route('descargar_comentario') !!}" target="_blank">
      <button class="bttn-unite bttn-md bttn-success ">Exportar</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="table_mp" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Empresa</th>
                      <th>Titulo</th>
                      <th>Comentario</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($comentario as $clas)
                    <tr>
                    	<td>{{$clas->razon_social}}</td>
                      <td>{{$clas->titulo}}</td>
                      <td>{{$clas->comentario}}</td>
                      <td>
                         <a href="" data-target="#modal-delete-{{$clas->codigo_comentario}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-danger"><i class="far fa-trash-alt"></i></button></a>
                      </td>
                   	</tr>
                    @include('Mantenimiento.comentarios.modaleliminar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
