<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$contr->cod_tipo_contribuyente}}">
	{{Form::Open(array('action'=>array('TipoContribuyenteController@destroy',$contr->cod_tipo_contribuyente
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar Tipo de Contribuyente</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_contribuyente_eliminar" value="{{$contr->cod_tipo_contribuyente}}">
				<input type="text" style="display:none" name="accion" value="0">
				<p>Esta seguro que desea desactivar el tipo de contribuyente: {{$contr->descrip_tipo_contribuyente}}</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
