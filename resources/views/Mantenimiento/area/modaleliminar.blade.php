<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$ar->cod_area}}">
	{{Form::Open(array('action'=>array('AreaController@destroy',$ar->cod_area
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar Area</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="cod_area_eliminar" value="{{$ar->cod_area}}">
				<input type="text" style="display:none" name="accion" value="0">
        <p>Esta seguro que dese eliminar el area: {{$ar->descrip_area}}?</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
