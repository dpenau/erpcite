<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$rent->cod_regimen_renta}}">
	{{Form::Open(array('action'=>array('RegimenRentaController@destroy',$rent->cod_regimen_renta
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar regimen renta</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_regimenr_eliminar" value="{{$rent->cod_regimen_renta}}">
				<input type="text" style="display:none" name="accion" value="0">
				<p>Esta seguro que desea desactivar el regimen renta: {{$rent->descrip_regimen_renta}}</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
