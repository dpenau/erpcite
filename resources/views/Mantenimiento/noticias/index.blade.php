@extends ('layouts.app')
@section ('content')
<div class="preloader">
</div>
<div class="container">
  <div>
  <h3 class="font-weight-bold">Listado de Noticias <a href="noticias/create"><button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva Noticia</button></a></h3>
  </div>

  <div class="x_content table-responsive">
    <table id="noticias" class="display">
      <thead>
        <tr>
          <th>Imagen</th>
          <th>Titulo de evento</th>
          <th>Descripcion</th>
          <th>Categoria</th>
          <th>Editar</th>
          <th>Eliminar</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar Noticia</h4>
            </div>
            <div class="modal-body">
              {!!Form::open(array('url'=>'Mantenimiento/noticias/update','files'=>true,'method'=>'PUT','autocomplete'=>'off'))!!}
                    {{Form::token()}}
              <input type="text" id="cod_evento" style="display:none" name="cod_evento">
              <div class="form-group">
                <label for="total_orden_compra">Nombre del material:</label>
                 <input type="text" id="nombre_evento" class="form-control" maxlength="70" name="nombre_evento">
              </div>
              <div class="form-group">
                <label for="total_orden_compra">Descripcion del material:</label>
                 <input type="text" id="descripcion" class="form-control" maxlength="70" name="descripcion">
              </div>
              <div class="row">
                <div class="form-group  col-md-12">
                  <label for="">Categoria--Subcategoria</label>
                  <select name="subcategoria" id="cat" class="custom-select" required>
                    <option value="" disabled>Escoja uno</option>
                    @foreach($categoria as $cat)
                      <option value="{{$cat->cod_categoria}}">{{$cat->nombre_categoria}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="">
                <label for="exampleFormControlFile1">Imagen de evento:</label>
                <input type="file" class="form-control-file"  name="photo" >
              </div>
            </div>
            <div class="modal-footer">
      				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
      				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
      			</div>
            {{Form::Close()}}
        </div>
    </div>
</div>
<div class="modal fade" id="modal_accion" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo"></h4>
            </div>
            <div class="modal-body">
              {!!Form::open(array('url'=>'Mantenimiento/noticias/delete','method'=>'DELETE','autocomplete'=>'off'))!!}
                    {{Form::token()}}
              <p id="mensaje"></p>
              <input type="text" id="cod_ev" style="display:none" name="email">
              </div>
            <div class="modal-footer">
      				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
      				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
      			</div>
            {{Form::Close()}}
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready( function () {
  var data =<?php echo $noticias;?>;
  var t=$("#noticias").DataTable();
  llenado_tabla()
  function llenado_tabla()
  {
    $.each(data,function(key,value){
      t.row.add( [
        '<img id="i'+value.cod+'" src="../photo/noticias/'+value.url_imagen+'" class="img-fluid" alt="Responsive image">',
        value.nombre_evento,
        value.descripcion,
        value.nombre_categoria,
        '<a class="openBtn" id="e'+value.cod+'"><button class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button></a>',
        '<a class="openBtn" id="b'+value.cod+'"><button class="bttn-unite bttn-md bttn-danger "><i class="fa fa-trash"></i></button></a>'
    ] ).draw();
      $("#e"+value.cod).off('click');
      $("#e"+value.cod).on('click',function(){
        var id = $(this).attr("id");
        id=id.substring(1,id.length)
        var index=buscar(id);
        modal_editar(index);
      });
      $("#b"+value.cod).off('click');
      $("#b"+value.cod).on('click',function(){
        var id = $(this).attr("id");
        id=id.substring(1,id.length)
        var index=buscar(id);
        modal_accion(index);
      });
    });
  }
  function modal_editar(index)  {
    $("#cod_evento").val(data[index].cod)
    $("#nombre_evento").val(data[index].nombre_evento)
    $("#descripcion").val(data[index].descripcion);
    var sub=data[index].cod_categoria
    $("#cat option[value='"+sub+"']").prop("selected",true)
    $("#myModal").modal({show:true});
  }
  function modal_accion(index)
  {
    $("#cod_ev").val(data[index].cod)
    $("#titulo").text('Eliminar Evento')
    $("#mensaje").text("Seguro que desea eliminar el evento?")
    $("#modal_accion").modal({show:true});
  }
  function buscar(id)  {
    var index = -1;
    var filteredObj = data.find(function(item, i){
      if(item.cod == id){
        index = i;
        return index;
      }
    });
    return index;
  }
});
</script>
@endsection
