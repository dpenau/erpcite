<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ERP CITECCAL</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/multi.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.css')}}">
    <link rel="stylesheet" href="{{asset('css/estilos.css')}}">
    <link rel="stylesheet" href="{{asset('css/tabs.css')}}">
    <link rel="stylesheet" href="{{asset('css/loader.css')}}">
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <link rel="image_src" href="{{asset('img/cite-logo.png')}}">
    <!--
    <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
    <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css')}}">
    -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/shCore.css')}}">

    <style type="text/css" class="init">

    </style>
  </head>
  <body class="hold-transition skin-alfa sidebar-mini ">
    <div class="wrapper">
      <nav class="navbar navbar-static-top main-header" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        </a>

        <a href="#" class="h3 font-weight-bold text-white">ERP CITECCAL</a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu ">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="nav-item dropdown ">
            <a class="bttn-unite bttn-md bttn-danger float-right dropdown-toggle" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Salir') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
              <a href="" class="bttn-unite bttn-md bttn-danger float-right dropdown-toggle" style="visibility:hidden;width:3px">a</a>
              <a id="navbarDropdown" class="bttn-unite bttn-md bttn-primary float-right dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

          </li>
          </ul>
        </div>
      </nav>
    </div>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar mt-3">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar ">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu ">
          <!-- INICIO -->
          <li class="treeview">
            <a href="{{url('bienvenida')}}">
              <i class="fa fa-home"></i>
              <span>Inicio</span>
            </a>
          </li>

          <!-- CONFIGURACION INICIAL -->
          <li class="treeview">
            <a href="#">
            <i class="fa fa-gear"></i>
              <span> Configuración Inicial</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{url('configuracion/empresa')}}"><i class="fas fa-store-alt"></i> Empresa</a></li>
              <li><a href="{{url('logistica/almacen')}}"><i class="fas fa-store-alt"></i> Almacenes</a></li>
              <li><a href="{{url('Produccion/series_calzado')}}"><i class="fa fa-shoe-prints"></i> Serie</a></li>
              <li><a href="{{url('configuracion/merma')}}"><i class="fas fa-store-alt"></i> Mermas</a></li>
              <li><a href="{{url('configuracion/operacion_directa')}}"><i class="fas fa-user-edit"></i>Operaciones Directas</a></li>
            </ul>
          </li>

          <!-- LOGISTICA -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-warehouse"></i>
              <span>Logistica</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu ">
              <li><a href="{{url('logistica/proveedores')}}"><i class="fa fa-handshake"></i> Proveedores</a></li>
              <li><a href="{{url('logistica/articulo')}}"><i class="fas fa-truck-loading"></i> Materiales</a></li>
              <li><a href="{{url('logistica/orden_compras')}}"><i class=" fa fa-shopping-cart "></i> Abastecimiento</a></li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-calculator"></i>
                  <span>Inventario</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu ">
                  <li><a href="{{url('logistica/kardex')}}"><i class="fas fa-cube"></i>Inventario Normal</a></li>
                  <li><a href="{{url('logistica/kardex/talla')}}"><i class="fas fa-cube"></i>Inventario por Tallas</a></li>
                </ul>
              </li>


              <!--
              <li><a href="{{url('logistica/orden_compra')}}"><i class="fas fa-shopping-cart"></i> Orden de Compra</a></li>
              <li><a href="{{url('logistica/ingreso_salida')}}"><i class="fas fa-door-open"></i> Recepcion de Material</a></li>
              <li><a href="{{url('logistica/kardex')}}"><i class="fas fa-receipt"></i> Kardex</a></li>
              <li><a href="{{url('logistica/articulo')}}"><i class="fas fa-box"></i> Materiales</a></li>
              <li><a href="{{url('logistica/proveedores')}}"><i class="fas fa-truck-loading"></i> Proveedores</a></li>
              <li><a href="{{url('logistica/almacen')}}"><i class="fas fa-boxes"></i> Almacen</a></li>
              <li><a href="{{url('logistica/tiendas')}}"><i class="fas fa-store-alt"></i> Tiendas</a></li>
              <li><a href="{{url('logistica/ordenes_salida')}}"><i class="fas fa-dolly-flatbed"></i> Ordenes de Salida</a></li>
              -->
            </ul>
          </li>
          <!-- RECURSOS HUMANOS -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Recursos Humanos</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('recursos_humanos/trabajador')}}"><i class="fa fa-user"></i> Personal</a></li>
              <li><a href="{{url('recursos_humanos/escala_salarial')}}"><i class="fas fa-chart-line"></i></i> Escala Salarial</a></li>
              <li><a href="{{url('recursos_humanos/beneficios')}}"><i class="fas fa-search-dollar"></i> Beneficios</a></li>
              <li><a href="{{url('recursos_humanos/configuracion')}}"><i class="fas fa-cogs"></i> Configuracion</a></li>
              <li><a href="{{url('recursos_humanos/Usuarios')}}"><i class="fa fa-user-cog"></i> Usuarios</a></li>
            </ul>
          </li>
          <!-- DESARROLLO DE PRODUCTO -->
          <li class="treeview">
            <a href="#">
              <i class="fas fa-pencil-ruler"></i>
              <span> Desarrollo de Producto</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">

              <li><a href="{{url('Produccion/coleccion')}}"><i class="fa fa-shoe-prints"></i> Colecciones y Líneas</a></li>
              <li><a href="{{url('Produccion/modelos_calzado')}}"><i class="fa fa-boxes"></i> Modelos</a></li>
              <li><a href="{{url('Produccion/ficha_calzado')}}"><i class="fa fa-clipboard-list "></i>  Ficha de Producto</a></li>
            </ul>
          </li>
          <!-- COSTOS -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-file-invoice-dollar"></i>
              <span>Gestión de Costos</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu ">
                <li><a href="{{url('costos/directos')}}"><i class="fa fa-calculator"></i> Costos Directos</a></li>

             <!--   <li class="treeview">
                <a href="#">
                  <i class="fa fa-calculator"></i>
                  <span>Costos Directos</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                 <ul class="treeview-menu ">
                  <li><a href="{{url('costo/vermodelos')}}"><i class="fas fa-cube"></i>Costos Directos</a></li>
                </ul>
                -->
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-money-check-alt"></i>
                  <span>Costos Indirectos</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu ">
                    <li><a href="{{url('costos/indirectos/operacion')}}"><i class="fas fa-cube"></i> Costo de Operación</a></li>
                    <li><a href="{{url('costo/indirectos/material_suministro')}}"><i class="fas fa-cube"></i> Gastos de Produccion</a></li>
                    <!--
                    <li><a href="{{url('costo/indirectos/material_suministro')}}"><i class="fas fa-cube"></i> Materiales Indirectos<br> y Suministros</a></li>
                    <li><a href="{{url('costo/indirectos/mano_obra')}}"><i class="fas fa-cube"></i> Mano de Obra Indirecta<br> (Productiva)</a></li>
                    <li><a href="{{url('costo/indirectos/depreciacion')}}"><i class="fas fa-cube"></i> Depreciación -<br> Mantenimiento</a></li>
                    <li><a href="{{url('costo/indirectos/servicio_basico')}}"><i class="fas fa-cube"></i> Servicio Basico</a></li>
                    <li><a href="{{url('costo/indirectos/gastos')}}"><i class="fas fa-cube"></i> Desarrollo Producto</a></li>
                    <li><a href="{{url('costos_indirectos/MaterialesAdm')}}"><i class="fa fa-cube"></i>Administrativos</a></li>
                    <li><a href="{{url('costos_indirectos/GastosDistTran')}}"><i class="fa fa-cube"></i>Distribucion <BR>y Transporte</a></li>
                    <li><a href="{{url('costos_indirectos/OtrosgastosdeVtas')}}"><i class="fa fa-cube"></i>Ventas y Marketing</a></li>
                    <li><a href="{{url('costos_indirectos/financiero')}}"><i class="fas fa-cube"></i> Financieros</a></li>
                    <li><a href="{{url('costos_indirectos/externo')}}"><i class="fas fa-cube"></i> Externos de la empresa</a></li>
                    -->
                </ul>
              </li>
              <li>
                <a href="{{url('costo/precio_venta')}}">
                <i class="fas fa-dollar-sign"></i> Precio de Venta</a>
              </li>
              <!-- <li>
                <a href="{{url('Configuracion/Costos_Directos')}}">
                <i class="fas fa-cogs"></i> Configuracion</a>
              </li>
              <li>
                <a href="{{url('costo/indicadores')}}">
                <i class="fas fa-chart-bar"></i> Historial de Costos</a>
              </li>
              -->
            </ul>
          </li>
          <!-- VENTAS -->
          <li class="treeview">
            <a href="#">
            <i class="fas fa-shopping-cart"></i>
              <span> Ventas</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('Ventas/clientes')}}"><i class="fas fa-user-friends"></i> Clientes</a></li>
              <li><a href="{{url('Ventas/pedidos')}}"><i class="fas fa-people-carry"></i> Pedidos</a></li>
              <li><a href="#"><i class="fas fa-calendar-alt"></i>Reportes<i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="{{url('Ventas/Reportes/ReporteProductos')}}"><i class="fas fa-calendar-alt"></i> Reporte de Productos</a></li>
                  <li><a href="{{url('Ventas/Reportes/ReporteVentas')}}"><i class="fas fa-calendar-alt"></i> Reporte de Ventas</a> </li>
                  <li><a href="{{url('Ventas/Reportes/ReporteClientes')}}"><i class="fas fa-calendar-alt"></i> Reporte de Clientes</a></li>
                </ul>
              </li>
              <li><a href="{{url('configuracion/Ventas')}}"><i class="fas fa-percent"></i> Descuentos</a></li>
            </ul>
          </li>
          <!-- PRODUCCION -->
          <li class="treeview">
            <a href="#">
              <i class="fas fa-industry"></i>
              <span>Produccion</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('Produccion/Proceso/create')}}"><i class="fas fa-calendar-alt"></i>Seguimiento a la Produccion</a></li>
            </ul>
          </li>
          <!-- PLANEACION -->
          <li class="treeview">
            <a href="#">
              <i class="fas fa-project-diagram"></i>
              <span> Planeacion</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('planeacion/formas_trabajo')}}"><i class="fas fa-users-cog"></i> Formas de Trabajo </a></li>
            </ul>
          </li>
          <!-- REPORTES POSIBLE DESAPARAICION -->
          <li class="treeview">
            <a href="#">
              <i class="fas fa-tasks"></i>
              <span>Reportes</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('Produccion/reportes/ReporteTiempo')}}"><i class="fas fa-tasks"></i> Reporte de Tiempos </a></li>
            </ul>
          </li>

          <!-- ACTUALIZACIONES -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-question-circle"></i>
              <span>Actualizaciones</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{url('actualizaciones/comentarios')}}">
                <i class="fas fa-comment"></i> Dejar Comentario</a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-question-circle"></i>
                  <span> Manuales</span>
                  <i class="fa fa-angle-left pull-right"></i>
                  <ul class="treeview-menu">
                    <li>
                      <a href="{{url('photo/manuales/Manual_de_usuario.pdf')}}" target="_blank">
                      <i class="fa fa-book"></i>Manual RRHH</a>
                    </li>
                    <li>
                      <a href="{{url('photo/manuales/Manual_de_logistica.pdf')}}" target="_blank">
                      <i class="fa fa-book"></i>Manual Logistica</a>
                    </li>
                    <li>
                      <a href="{{url('photo/manuales/Manual_de_desarrollo.pdf')}}" target="_blank">
                      <i class="fa fa-book"></i>Manual Desarrollo de Producto</a>
                    </li>
                  </ul>
                </a>
              </li>
            </ul>
          </li>




          <!-- NOVEDADES -->
          <li>
            <a href="{{url('/novedades/')}}" >
              <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
              <span class="text-yellow">Novedades </span>
              <span class="bg-warning text-dark float-right"><strong> NEW</strong></span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
    <!--Contenido-->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12" style="height:20%; overflow-x:scroll">
                    <!--Contenido-->
                    @include('flash-message')
                    <div class="bg-light" id="actualizar_datos" align="center" style="display:none">
                      <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i><span></span>
                    </div>
                    <!--MANTENIMIENTO
                    <div class="bg-warning"  align="center" >
                      <span>NOS ENCONTRAMOS EN MANTENIMIENTO EN SALIDAS DE MATERIAL MUCHAS GRACIAS.</span>
                    </div>
                    -->
                    @yield('contenido')
                    <!--Fin Contenido-->
                  </div>
                </div>
              </div>
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b>
        1.1.2
      </div>
      Copyright © Todos los Derechos Reservados<b> - Instituto Tecnologico de la Produccion</b>
    </footer>
    <!--Fin-Contenido-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- jQuery 2.1.4 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
    <!-- eventos -->
    <script src="{{asset('js/eventos.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/ajaxOP.js')}}"></script>
    <script src="{{asset('js/validacion.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('js/loader.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('js/shCore.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('js/demo.js')}}"></script>
    <script type="text/javascript" language="javascript" class="init">
      $(document).ready(function() {

        $('#example').DataTable( {
          'columnDefs': [
          {
            'targets': 0,
            'checkboxes': {
              'selectRow': true
            }
          }
          ],
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });

        $('#material_normal').DataTable( {
            "lengthMenu": [[100, -1], [100, "All"]],
          'columnDefs': [
          {
            'targets': 0,
            'checkboxes': {
              'selectRow': true
            }
          }
          ],
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
        $('#operacion_mano').DataTable( {
            "lengthMenu": [[100, -1], [100, "All"]],
          'columnDefs': [
          {
            'targets': 0,
            'checkboxes': {
              'selectRow': true
            }
          }
          ],
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });


        $('#example3').DataTable( {
          'columnDefs': [
          {
            'targets': 0,
            'checkboxes': {
            'selectRow': true
            }
          }
          ],
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
        $('#example2').DataTable( {
          'columnDefs': [
          {
            'targets': 0,
            'checkboxes': {
              'selectRow': true
            }
          }
          ],
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });

      });
      /*window.onbeforeunload = preguntarAntesDeSalir;

      function preguntarAntesDeSalir()
      {

          return "¿Seguro que quieres salir?";
      }*/
    </script>
  </body>
</html>
