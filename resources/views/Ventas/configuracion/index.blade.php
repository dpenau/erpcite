@extends ('layouts.admin')
@section ('contenido')

<div class="preloader"></div>
<div>
<h3 class="font-weight-bold">Descuento en ventas</h3> <a>
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5" id="add">Agregar Descuento</button></a>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Clientes:</label>
            <select name="" class="custom-select" id="cliente">
                <option value="" disabled selected>Seleccionar un Cliente</option>
                @foreach($cliente as $cli)
                  <option value="{{$cli->codigo}}">{{$cli->nombre}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<h4>Descuento en Materiales</h4>
<div class="x_content table-responsive">
  <table id="tabla_materiales" class="display">
    <thead>
      <tr>
        <th>Nombre de Material</th>
        <th>Porcentaje de Descuento</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
<h4>Descuento en Modelos</h4>
<div class="x_content table-responsive">
  <table id="tabla_modelo" class="display">
    <thead>
      <tr>
        <th>Modelo - Serie</th>
        <th>Porcentaje de Modelo</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  let tabla_materiales=$("#tabla_materiales").DataTable()
  let tabla_modelo=$("#tabla_modelo").DataTable()
  $("#cliente").change(function(){
    let valor=$("#cliente").val();
    $.ajax({
      url:"/ventas/obt_descuento/"+valor,
      success:function(data){
        cargar_materiales(data);
        cargar_modelos(data);
      }
    })
  })
  function cargar_materiales(data)
  {
    let datos=data["materiales"];
    console.log(datos)
  }
  function cargar_modelos(data)
  {
    let datos=data["modelo"];
    console.log(datos)
  }
  $("#add").click(function(){
    let cliente=$("#cliente").val()
    if(cliente!=null)
    {
      let url="/ventas/nuevo_descuento/"+cliente
      window.location.replace(url)
    }
    else
    {
      alert("seleccione un cliente")
    }
  })
})
</script>
@endsection
