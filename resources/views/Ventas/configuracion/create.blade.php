@extends ('layouts.admin')
@section ('contenido')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="font-weight-bold">Registrar Descuento al cliente: {{$cliente[0]->nombre}}</h2>
                <div class="clearfix"></div>                    
            </div>
            {!!Form::open(array('url'=>'Ventas/clientes','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <input type="text" style="display:none" value="{{$cliente[0]->codigo}}">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-6">
                    <div class="row">
                        <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                            <label for="total_orden_compra">Tipo de descuento:</label>
                            <select required id="tipo" class="custom-select">
                                <option value="" disabled selected>Seleccione el tipo de descuento</option>
                                <option value="1">Materiales</option>
                                <option value="2">Modelos</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 col-xs-12">
                            <label for="total_orden_compra">Seleccione producto:</label>
                            <div id="cont_producto">
                                <select id='productos' multiple='multiple'>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                            <label for="total_orden_compra">% de Descuento:</label>
                            <input type="text" onkeypress="return isNumberKey(event)" name="descuento" required="required" class="form-control ">
                      </div>
                    </div>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                    <br>
                    <small class="text-danger">*Si el cliente posee ya un descuento en algun material o modelo este se sobreescribira</small>
                </div>
            </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/js/multi.min.js"></script>
<script>
$('#productos').multi();
$('#productos').change(()=>{
    let lista_escogidos=$("#productos").val()
});
$("#tipo").change(function(){
    let tipo=$("#tipo").val()
        $.ajax({
        url:"/ventas/tipo_descuento/"+tipo,
        success:function(data){
                agregar_data(data)
        }
    })
})
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
    return false;
    return true;
}
function agregar_data(data)
{
    $("#productos").empty()
    let tipo=$("#tipo").val();
    if(tipo==1)
    {
        let nombre_anterior=""
        let costo_anterior=0
        var cod_material=[]
        var nombre_material=[]
        var costo_material=[]
        $.each(data,function(i,val){
            let nombre=val.descrip_material.split("-");
            let costo=val.costo_sin_igv_material;
            if(nombre_anterior==nombre[0])
            {
                if(costo_anterior!=costo)
                {
                    costo_anterior=costo
                    $("#productos").append("<option class='text-dark' value='"+val.cod_material+"'>"+val.descrip_material+" - S/."+costo+"</option>")
                }
            }
            else
            {
                nombre_anterior=nombre[0]
                costo_anterior=costo
                $("#productos").append("<option class='text-dark' value='"+val.cod_material+"'>"+val.descrip_material+" - S/."+costo+"</option>")
            }
        })
    }
    else{
        $.each(data,function(i,val){
            $("#productos").append("<option class='text-dark' value='"+val.cod_articulo+"'>"+val.codigo_modelo+" - S/."+val.precio+"</option>")
        })
    }
    $(".non-selected-wrapper").empty()
    $(".selected-wrapper").empty()
    $(".search-input").addClass("bg-gray")
    $(".search-input").attr("placeholder","Buscando...")
    setInterval(() => {
    $(".search-input").removeClass("bg-gray")
    $(".search-input").attr("placeholder","Buscar...")
    }, 1500);
    
}
</script>
@endsection
