@extends ('layouts.admin')
@section ('contenido')
  <div>
    <h3 class="font-weight-bold">Reporte de Productos</h3>
  </div>
  <div style="width:25%">
    <label for="">Modelos de la Coleccion:</label>
    <select id="linea" class="form-control" name="">
      <option value="" selected disabled>Coleccion:</option>
      <option value="t">Todo</option>
      @foreach($coleccion as $lin)
        <option value="{{$lin->codigo_coleccion}}">{{$lin->nombre_coleccion}}</option>
      @endforeach
    </select>
  </div>
  <br>
  <div>
    <section>
      <table id="tabla" class="display" style="width:100%">
        <thead>
          <tr>
            <th>Codigo Modelo</th>
            <th>Cantidad</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </section>
  </div>
  <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-imagen">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header">
  				<h4 class="modal-title" id="titulo_imagen"></h4>
  			</div>
  			<div class="modal-body">
          <div class="border border-dark" align="center" id="foto_imagen">
          </div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
  			</div>
  		</div>
  	</div>
  </div>
  <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-update">
    {{Form::Open(array('action'=>array('ModelosCalzadoController@update','1'),'files'=>true,'method'=>'patch'))}}
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="titulo_mod">Modificar Modelo: </h4>
          </div>
          <div class="modal-body">
            <div class="x_content">
              <div class="col-md-12">
                <label>Coleccion:</label>
                <select name="linea" required='required' id="linea_mod" class="custom-select">
                  <option value="" disabled>--- Seleccionar Coleccion ---</option>
                </select>
              </div>
              <div class="col-md-12">
                Codigo de Modelo<input readonly="" maxlength="20" required="required" type="text" id="codmodelo_mod_2" required="required"  class="form-control col-md-12" placeholder="Codigo de Modelo*" >
                <input  maxlength="20" required="required" type="text" id="codmodelo_mod" required="required" name="cods" style="display:none" class="form-control col-md-12" placeholder="Codigo de Modelo*" >
              </div>
              <div class="col-md-12">
                Nombre de Modelo<input maxlength="20" required="required" type="text" id="nombre_mod" required="required" name="nombre_modelo_mod" class="form-control col-md-12" placeholder="Nombre de Modelo*" >
              </div>
              <div class="col-md-12">
                Descripcion de Modelo <input maxlength="30" required="required" type="text" id="descrip_mod" required="required" name="descripcion_modelo_mod" class="form-control col-md-12" placeholder="Descripcion de Modelo*" >
              </div>
              <div class="col-md-12" id="serie_mod">
                Series:
                <br>
              </div>
              <div class="col-md-12">
                <label>Horma:</label>
                <select name="horma" required='required' id="horma_md" class="custom-select">
                  <option value="" disabled>--- Seleccionar Horma ---</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="">
                  <label for="">Capellada:</label>
                  <select name="capellada" id="capellada_mod" required='required' class="custom-select">
                    <option value="" selected disabled>--- Seleccionar Capellada ---</option>
                  </select>
                </div>
                <div class="">
                  <label for="">Forro:</label>
                  <select name="forro" id="forro_mod" required='required' class="custom-select">
                    <option value="" selected disabled>--- Seleccionar Forro ---</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="">
                  <label for="">Plantilla:</label>
                  <select name="plantilla" id="plantilla_mod" required='required' class="custom-select">
                    <option value="" selected disabled>--- Seleccionar Plantilla ---</option>
                  </select>
                </div>
                <div class="">
                  <label for="">Piso:</label>
                  <select name="piso" id="piso_mod" required='required' class="custom-select">
                    <option value="" selected disabled>--- Seleccionar Piso ---</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <label for="exampleFormControlFile1">Imagen de modelo:</label>
                <input type="file" class="form-control-file"  name="photo" >
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="bttn-unite bttn-md bttn-primary">Confirmar</button>
            <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
          </div>
          </div>
        </div>
      </div>
  	{{Form::Close()}}
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
  <script type="text/javascript">
  var cantidad =<?php echo $cantidades;?>;
  var groupBy = function (cantidad, prop) {
      return cantidad.reduce(function(groups, item) {
          var val = item[prop];
          groups[val] = groups[val] || {codigo_serie_articulo: item.codigo_serie_articulo, cantidades: 0, codigo_coleccion:item.codigo_coleccion, codigo_modelo:item.codigo_modelo, nombre_coleccion:item.nombre_coleccion};
          groups[val].cantidades += ','+item.cantidades;
          return groups;
      }, {});
  }
    //console.log(cantidad);
    //console.log(groupBy(cantidad,'codigo_serie_articulo'));
    cantidad = groupBy(cantidad,'codigo_serie_articulo')
    //console.log(cantidad);
  var t=$("#tabla").DataTable();
  $("#linea").on("change",()=>{
    let nombre_linea=$("#linea").val()
    filtrar(nombre_linea)
  })
  function filtrar(nombre_linea){
    t
    .rows()
    .remove()
    .draw();
    let filtro=nombre_linea;
    //console.log(nombre_linea)
    var ser =<?php echo $seriemodelo;?>;
    if(filtro=="t")
    {
      $.each(cantidad,(id,val)=>{
        var cantidades= val.cantidades
        var total = 0;
            for (var i = 0; i < cantidades.length; i++) {
                total += cantidades[i] << 0;
              }
        var codigo_coleccion= val.codigo_coleccion
        var codigo_modelo= val.codigo_modelo
        var codigo_serie_articulo= val.codigo_serie_articulo
        var nombre_coleccion= val.nombre_coleccion
        console.log(cantidades)
        console.log(total)
        console.log(codigo_coleccion)
        console.log(codigo_serie_articulo)
        console.log(nombre_coleccion)
        t
        .row
        .add([
          val["codigo_modelo"],
          total
        ]).draw();
      })
    }
    else {
      $.each(cantidad,(id,val)=>{
        console.log(val)
        if (val.codigo_coleccion==filtro) {
          var cantidades= val.cantidades
          var total = 0;
              for (var i = 0; i < cantidades.length; i++) {
                  total += cantidades[i] << 0;
                }
          var codigo_coleccion= val.codigo_coleccion
          var codigo_modelo= val.codigo_modelo
          var codigo_serie_articulo= val.codigo_serie_articulo
          var nombre_coleccion= val.nombre_coleccion
          t
          .row
          .add([
            val["codigo_modelo"],
            total
          ]).draw();

        }
      })
    }
  }
  function buscar(id)
  {
    var index = -1;
    var filteredObj = data.find(function(item, i){
      if(item.cod_modelo == id){
        index = i;
        return index;
      }
    });
    return index;
  }
  function modal_editar(index)
  {
    limpiar();
    $("#titulo_mod").val("Modificar Modelo "+data[index].cod_modelo)
    var coleccion="";
    @foreach ($coleccion as $col)
      var col="{{$col->codigo_coleccion}}"
      var nom="{{$col->nombre_coleccion}}"
      if(data[index].codigo_coleccion==col)
      {
        coleccion=coleccion+"<option value='"+col+"'>"+nom+"</option>"
      }
      else
      {
        coleccion=coleccion+"<option value='"+col+"' >"+nom+"</option>"
      }
     @endforeach
     var linea="";
     @foreach ($Lineas as $linea)
       var col="{{$linea->cod_linea}}"
       var nom="{{$linea->nombre_linea}}"
       if(data[index].cod_linea==col)
        linea+="<option value='"+col+"' selected >"+nom+"</option>"
      else
        linea+="<option value='"+col+"' >"+nom+"</option>"
      @endforeach
    $("#codmodelo_mod").val(data[index].cod_modelo)
    $("#nombre_mod").val(data[index].nombre)
    var series=""
    @foreach($todaserie as $ser)
      var cod_ser="{{$ser->cod_serie}}"
      var nomb_ser="{{$ser->nombre_serie}}"
      var c=0;
      @foreach($seriemodelo as $series)
        var cod="{{$series->codigo_modelo}}"
        var cod_s="{{$series->codigo_serie}}"
        if(cod==data[index].cod_modelo && cod_s==cod_ser)
        {
          series+="<input type='checkbox' name='serie[]' value='"+cod_ser+"' checked>"+nomb_ser+"<br>"
          c=1
        }
      @endforeach
      if(c==0)
      {
        series+="<input type='checkbox' name='serie[]' value='"+cod_ser+"' >"+nomb_ser+"<br>"
      }
    @endforeach
    var mat="";
    var ho=""

     $("#horma_md").append(ho)
    $("#serie_mod").append(series)
    $("#capellada_mod").append(cap)
    $("#forro_mod").append(fo)
    $("#plantilla_mod").append(pla)
    $("#piso_mod").append(pi)
    $("#modal-update").modal({show:true});
  }
  </script>
@endsection
