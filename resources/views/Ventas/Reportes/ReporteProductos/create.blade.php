@extends ('layouts.admin')
@section ('contenido')
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nuevo Modelo de Calzado</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'Produccion/modelos_calzado','files'=>true,'method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                <div class="row">
                  <div class="form-group  col-md-4  col-xs-12">
                    <div class="">
                      <label>Coleccion:</label>
                       <select name="coleccion" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Coleccion ---</option>
                        @foreach ($coleccion as $col)
                         <option value="{{$col->codigo_coleccion}}" >{{$col->nombre_coleccion}}</option>
                         @endforeach
                         </select>
                      </div>
                      <div class="">
                        <label>Linea:</label>
                         <select name="linea" required='required' class="custom-select">
                          <option value="" selected disabled>--- Seleccionar Linea ---</option>
                          @foreach ($Lineas as $linea)
                           <option value="{{$linea->cod_linea}}" >{{$linea->nombre_linea}}</option>
                           @endforeach
                           </select>
                        </div>
                        <div class="">
                            <label for="codigo_calzado">Codigo de Modelo</label>
                             <input type="text" id="codigo_modelo" name="codigo_modelo" required="required" maxlength="12" class="form-control ">
                        </div>
                      <div class="">
                            <label for="nombre_modelo">Nombre de Modelo</label>
                             <input type="text" id="nombre_modelo" name="nombre_modelo" required="required" maxlength="50" class="form-control ">
                        </div>
                      <div class="">
                            <label for="descripcion_modelo">Descripcion de Modelo</label>
                             <input type="text" id="descripcion_modelo" name="descripcion_modelo" required="required" maxlength="50" class="form-control ">
                        </div>
                        <div class="">
                            <label>Series:</label>
                            <br>
                            @foreach($serie as $ser)
                              <input type="checkbox" name="serie[]" value="{{$ser->cod_serie}}">{{$ser->nombre_serie}}<br>
                            @endforeach
                        </div>
                  </div>
                  <div class="form-group  col-md-4 offset-md-2 col-xs-12">
                    <div class="">
                      <label for="">Horma:</label>
                       <select name="horma" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Horma ---</option>
                        <?php $mat="";?>
                        @foreach ($horma as $ho)
                          <?php $ind=strpos($ho->descrip_material,"-");
                            $act=substr($ho->descrip_material,0,$ind++);
                          ?>
                          @if($mat!=$act)
                            <?php
                              $mat=$act;
                              echo "<option value='".$ho->cod_material."' >".$act."</option>"
                            ?>
                          @endif
                         @endforeach
                         </select>
                      </div>
                      <div class="">
                        <label for="">Capellada:</label>
                         <select name="capellada" required='required' class="custom-select">
                          <option value="" selected disabled>--- Seleccionar Capellada ---</option>
                          @foreach ($capellada as $linea)
                           <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                           @endforeach
                           </select>
                        </div>
                        <div class="">
                          <label for="">Forro:</label>
                           <select name="forro" required='required' class="custom-select">
                            <option value="" selected disabled>--- Seleccionar Forro ---</option>
                            @foreach ($capellada as $linea)
                             <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                             @endforeach
                             </select>
                          </div>
                          <div class="">
                            <label for="">Plantilla:</label>
                             <select name="plantilla" required='required' class="custom-select">
                              <option value="" selected disabled>--- Seleccionar Plantilla ---</option>
                              @foreach ($capellada as $linea)
                               <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                               @endforeach
                               </select>
                            </div>
                            <div class="">
                              <label for="">Piso:</label>
                               <select name="piso" required='required' class="custom-select">
                                <option value="" selected disabled>--- Seleccionar Piso ---</option>
                                @foreach ($capellada as $linea)
                                 <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                                 @endforeach
                                 </select>
                              </div>
                              <div class="">
                                <label for="exampleFormControlFile1">Imagen de modelo:</label>
                                <input type="file" class="form-control-file"  name="photo" required>
                              </div>
                  </div>
                </div>
                          <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                            </div>
                          </div>
          {!!Form::close()!!}
        </div>
      </div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
