@extends ('layouts.admin') 
@section ('contenido')
  <div class="preloader">  </div>
  <div>
        <h1 class="font-weight-bold">Reporte de Ventas </h1>
        <hr style="border:3px solid:blank">
  </div>
  <div id="tablas">
    <div id="filtros" class="row" style="width:100%">
      <div class="col-md-4 col-xs-12 col-sm-12" style="border:0px solid black"><br>
        <div class='custom-control custom-checkbox'>
          <input type='checkbox' class='custom-control-input' value="1"  onclick="desactivar()" id='anioreporte'>
          <label class='custom-control-label' for='anioreporte'>Año</label>
        </div>
        <br>
        <div class='custom-control custom-checkbox'>
          <input type='checkbox' class='custom-control-input' value="1" onclick="desactivar()" id='temporadareporte'>
          <label class='custom-control-label' for='temporadareporte'>Temporada</label>
        </div>
        <br>
        <div class='custom-control custom-checkbox'>
          <input type='checkbox' class='custom-control-input' value="1" onclick="desactivar()" id='mesreporte'>
          <label class='custom-control-label' for='mesreporte'>Mes</label>
        </div>
        <br>
        <div class='custom-control custom-checkbox'>
          <input type='checkbox' class='custom-control-input' value="1" onclick="desactivar()" id='semanareporte'>
          <label class='custom-control-label' for='semanareporte'>Semana</label>
        </div>
        <br>
      </div>
    </div>
    <hr>
    <table id="example" class="display">
      <thead>
        <tr>
          <th>Codigo de pedido</th>
          <th>Nombre</th>
          <th>Ganancia</th>
          <th>Fecha</th>
                                           
        </tr>
                               
      </thead>
      <tbody>
      @foreach($ordenVentas as $ordenVenta)
        <tr>
          <td> {{$ordenVenta->codigo_pedido}}</td>
          <td> {{$ordenVenta->nombre}}</td>
          <td> {{$ordenVenta->total_pedido}} </td>
          <td> {{$ordenVenta->fecha}} </td> 
        </tr>
      @endforeach
      </tbody>
    </table>
    <div>
      <label class='custom-control-label'> TOTAL DE GANANCIA: {{$SumaTotales}}</label>         
      <a href="{!! route('ventas_descargar') !!}" target="_blank">
      <button class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Exportar PDF</button><a href="#"><a href="#" target="_blank"></a>
    </div>
  </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
function desactivar() {
    if($("#anioreporte:checked").val()==1) {
        $("#anioreporte").attr('disabled', 'disabled'); }
    else {
        $("#anioreporte").removeAttr("disabled"); }

 if($("#temporadareporte:checked").val()==1) {
        $("#temporadareporte").attr('disabled', 'disabled'); }
    else {
        $("#temporadareporte").removeAttr("disabled"); }

if($("#mesreporte:checked").val()==1) {
        $("#mesreporte").attr('disabled', 'disabled'); }
    else {
        $("#mesreporte").removeAttr("disabled"); }

        if($("#semanareporte:checked").val()==1) {
        $("#semanareporte").attr('disabled', 'disabled'); }
    else {
        $("#semanareporte").removeAttr("disabled"); }
}
/*
  $(document).ready(function() {
   $("#anioreporte").on("click", function() {
      $('td:nth-child(2)').toggle();
      $(".").toggle();
    });
    $("#temporadareporte").on("click", function() {
      $('td:nth-child(3)').toggle();
      $(".").toggle();
    });
    $("#mesreporte").on("click", function() {
      $('td:nth-child(4)').toggle();
      $(".").toggle();
    });
    $("#semanareporte").on("click", function() {
      $('td:nth-child(5)').toggle();
      $(".").toggle();
    });
    var t = $("#tabla_reporte_clientes").DataTable();
    $(document).on('click', '#exportar_reporte_ventas', function() {
      var anioreporte = $('#anioreporte').prop('checked');
      var temporadareporte = $('#temporadareporte').prop('checked');
      var mesreporte = $('#mesreporte').prop('checked');
      var semanareporte = $('#semanareporte').prop('checked');
      var data=anioreporte+"-"+temporadareporte+"-"+mesreporte+"-"+semanareporte;
    window.open('/Ventas/Reportes/ReporteClientes/exportar/'+data)
    });
  })*/
</script>
@endsection
