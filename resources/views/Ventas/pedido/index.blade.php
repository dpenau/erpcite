@extends ('layouts.admin')
@section ('contenido')

<div class="preloader"></div>
<div>
<h3 class="font-weight-bold">Listado de Pedidos <a href="pedidos/create">
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva Orden de Pedido</button></a>
</h3>
</div>

<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr>
        <th>Codigo</th>
        <th>Cliente</th>
        <th>Fecha</th>
        <th>Deuda</th>
        <th>Total</th>
        <th>Ordenes de Produccion</th>
        <th>Detalle de Pedido</th>
      </tr>
    </thead>
    <tbody>
      @foreach($ordenes as $ord)
        <tr>
          <td>{{$ord->codigo_pedido}}</td>
          <td>{{$ord->nombre}}</td>
          <td>{{$ord->fecha}}</td>
          <td>{{$ord->deuda}}</td>
          <td>{{$ord->total_pedido}}</td>
          <td><a href="/Produccion/orden_produccion/{{$ord->codigo_pedido}}" class="alert-link">
            <button type="button" class="bttn-unite bttn-md bttn-warning" name="button"><i class="fas fa-file-invoice"></i></button>
          </a>
          </td>
          <td><a class='modal_activar' id="{{$ord->codigo_pedido}}"> 
            <button  type="button" class="bttn-unite bttn-md bttn-primary" name="button"><i class="far fa-eye"></i></button>
          </a></td>
        </tr>
      @endforeach
    </tbody>
  </table>

  <div class="modal fade" tabindex="-1" role="dialog" id="detalle_modal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="titulo_modal">Orden de Pedido </h5>
        </div>
        <div class="modal-body" style='overflow-x: scroll;'>
          <table id="cuerpo_modal" class="display" >
            <thead>
              <th>Modelo</th>
              <th>Tallas Totales</th>
              <th>Tallas Faltantes O.P.</th>
              <th>Anular</th>
            </thead>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>  
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabla_cuerpo=$("#cuerpo_modal").DataTable();
    $(".modal_activar").on("click",function(){
      var codigo=$(this).attr("id");
      consultar_detalle(codigo)
    })
    function consultar_detalle(codigo)
    {
      $.ajax({
        url:"/pedido/obtener_detalle/"+codigo,
        success:function(datos){
          llenar_detalle(datos)
        }
      })
    }
    function llenar_detalle(datos)
    {
      tabla_cuerpo.rows()
      .remove()
      .draw()
      $.each(datos,function(i,val){
      var tab=generar_tallas(val.cantidades,val.tallaInicial,val.tallaFinal,val.cantidades_ord_prod)
        tabla_cuerpo.row.add( [
              val.cod_modelo,
              tab[0],
              tab[1],
              '<a class="openBtn" id="el'+val.cod_modelo+'"><button class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button></a>'
            ] ).draw();
      })
      
    $("#detalle_modal").modal({show:true});
    }

    function generar_tallas(cantidad,tallai,tallaf,cantidades_ord_prod){
        var tabs=[]
        var cabecera="<table><thead><tr>"
        var cuerpo="<tr>";
        var cuerpo_op="<tr>";
        var tallas=cantidad.split(",");
        var tallas_op=cantidades_ord_prod
        if(tallas_op!=null)
        {
          tallas_op=cantidades_ord_prod.split(",");
        }
        $.each(tallas,function(ind,value){
          cabecera+="<td>"+tallai+"</td>"
          cuerpo+="<td>"+value+"</td>"
          let nuevo=0
          if(tallas_op!=null)
          {
            nuevo=parseInt(value)-parseInt(tallas_op[ind])
          }
          else{
            nuevo=value
          }
          cuerpo_op+="<td>"+nuevo+"</td>"
          tallai++;
        })
        cabecera+="</tr></thead>"
        cuerpo_op+="</tr></table>"
        cuerpo+="</tr></table>"
        tabs[0]=cabecera+cuerpo
        tabs[1]=cabecera+cuerpo_op
        return tabs;
    }
  })
</script>
@endsection
