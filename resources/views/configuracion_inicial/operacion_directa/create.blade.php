@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">

    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nueva Operacion Directa</h2>
                    <div class="clearfix"></div>
                </div>

                {!! Form::open(['url' => 'configuracion/operacion_directa', 'method' => 'POST', 'autocomplete' => 'off']) !!}
                {{ Form::token() }}

                <div class="row">
                    <div class="form-group  col-md-8">
                        <label for="">Proceso</label>
                        <select id="proceso" required="required" name="proceso" class="custom-select">
                            <option value="" selected disabled>Seleccionar Proceso</option>
                            @foreach ($proceso as $proc)
                                <option value="{{ $proc->cod_proceso }}">{{ $proc->codigo }} {{ $proc->nombre }}
                                </option>
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-8">
                        <label for="total_orden_compra">Operación:</label>
                        <input type="text" id="operacion" name="operacion" required="required" maxlength="100"
                            class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4">
                        <label for="">Tipo de Operación</label>
                        <select id="tipo_operacio" name="tipo_operacion" class="custom-select">
                            <option value="" selected disabled>Seleccionar Tipo Operacion</option>
                            @foreach ($tipo as $tip)
                                <option value="{{ $tip->cod_pago }}">{{ $tip->nombre_pago }} </option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group  col-md-4">
                        <label for="total_orden_compra">Unidad de Medida:</label>
                        <input type="text" id="unidad_medida" name="unidad_medida" maxlength="100" class="form-control "
                            readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-3">
                        <label for="total_orden_compra">Costo:</label>
                        S/.<input type="number" step="0.0001" id="costos" name="costo" required="required" maxlength="100"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-2">
                        <label for="total_orden_compra">Beneficio:</label>
                        S/.<input type="text" step="0.0001" id="beneficio" name="beneficio" maxlength="100"
                            class="form-control " readonly>
                    </div>
                    <div class="form-group  col-md-3">
                        <label for="total_orden_compra">Otros Costos Mensuales:</label>
                        S/.<input type="number" step="0.0001" id="otros_costos" name="otros_costos" required="required"
                            maxlength="100" class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-3">
                        <label for="total_orden_compra">Costo por par:</label>
                        S/. <input type="text" step="0.0001" id="costo_par" name="costo_par" class="form-control "
                            readonly>
                    </div>

                </div>


                <div class="ln_solid"></div>
                <div class="form-group" style="margin: 5% 3% 3% 18%">
                    <div class="col-md-12 col-sm-8 col-xs-12">
                        <a href="{{ url('configuracion/operacion_directa') }}">
                            <button type="submit"
                                class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>

                            <button type="button" class="bttn-unite bttn-md bttn-danger col-md-2  ">Cancelar</button></a>

                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>


    <script type="text/javascript">
        var tipo = <?php echo $tipo; ?>;
        var empresaP = <?php echo $empresaP; ?>;
        var proceso = <?php echo $proceso; ?>;
        let idtipo = 0;
        let tipoOperacion=$("#tipo_operacio").val();
console.log(empresaP);
        $("#costos").keyup(function() {

            //DESTAJO
            if ($("#tipo_operacio").val() == 1) {
                $("#beneficio").val(0);
                destajo();

            }
            //JORNAL
            else if (idtipo == 2) {
                $("#beneficio").val(0);
                jornal();
            }
            //PLANTILLA
            else if (idtipo == 3) {
                let costos = $("#costos").val();
                $("#beneficio").val(costos * (empresaP[0].porcentaje_beneficio / 100));
                planilla();
            }


        });

        $("#otros_costos").keyup(function() {

            //DESTAJO
            if ($("#tipo_operacio").val() == 1) {
                $("#beneficio").val(0);
                destajo();

            }
            //JORNAL
            else if (idtipo == 2) {
                $("#beneficio").val(0);
                jornal();
            }
            //PLANTILLA
            else if (idtipo == 3) {
                let costos = $("#costos").val();

                planilla();
            }


        });

        function destajo() {

            var costos = $("#costos").val();
            console.log($("#costos").val())
            let otros_costos = $("#otros_costos").val();
            let costo_par_total = (costos / 12) + (otros_costos / empresaP[0].produccion_promedio)
            $("#costo_par").val((costo_par_total).toFixed(4));
        }

        function jornal() {
            let costos = $("#costos").val();
            let otros_costos = $("#otros_costos").val();
            let costo_par_total = (costos * 4 / empresaP[0].produccion_promedio) + (otros_costos /
                empresaP[0].produccion_promedio)
            $("#costo_par").val((costo_par_total).toFixed(4));
        }

        function planilla() {

            let costos = $("#costos").val();
            let otros_costos = $("#otros_costos").val();
            $("#beneficio").val((costos * (empresaP[0].porcentaje_beneficio / 100)).toFixed(4));
            var beneficio =  $("#beneficio").val()*1;
            var partei = costos*1 + beneficio*1;
            var parteii = empresaP[0].produccion_promedio;
            let otros_costos_t =  (otros_costos*1 / empresaP[0].produccion_promedio*1)
            $("#costo_par").val((partei / parteii+otros_costos_t).toFixed(4));
        }


        $("#tipo_operacio").change(function() {
            valorid = this.value;
            idtipo = this.value;

            $.each(tipo, function(key, valu) {

                if (valu.cod_pago == valorid) {

                    $("#unidad_medida").val(valu.descrip_unidad_medida + " (" + valu.unidad + ")");
                    if (valorid == 3) {
                        $("#beneficio").val($("#costos").val() * (empresaP[0].porcentaje_beneficio / 100));
                        planilla();

                    } else if (valorid == 2) {
                        $("#beneficio").val(0);
                        jornal();
                    } else if (valorid == 1) {
                        $("#beneficio").val(0);
                        destajo();
                    }
                }
            });
        });
    </script>
@endsection
