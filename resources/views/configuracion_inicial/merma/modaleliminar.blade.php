<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-eliminar-{{ $merma->id }}">
    <form method="POST" action="{{ url('configuracion/merma/delete/' . $merma->id) }}">
        <!-- CSRF Token -->
        @method('delete')
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Desactivar Almacen</h4>
                </div>
                <div class="modal-body">
                    <p>Confirme si desea Eliminar la merma?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
