<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use erpCite\Modelo;
use erpCite\Coleccion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class ReporteTiempoController extends Controller
{

	public function __construct()
  {
    $this->middleware('jefe');
  }
	public function index()
  {

	$grupo_data = DB::table('grupo_trabajo','serie_modelo','orden_pedido_produccion')
     ->join('orden_pedido_produccion','grupo_trabajo.codigo_orden_pedido_produccion','=','orden_pedido_produccion.codigo_orden_pedido_produccion')
     ->join('serie_modelo','orden_pedido_produccion.codigo_serie_articulo','=','serie_modelo.codigo')
     ->select('grupo_trabajo.codigo_grupo_trabajo','grupo_trabajo.especialidad','grupo_trabajo.tiempo','grupo_trabajo.proceso','serie_modelo.codigo_modelo')
     ->get();

		$modelos = Modelo::select('cod_modelo','cod_coleccion','coleccion.nombre_coleccion')
		->join('coleccion','modelo.cod_coleccion','=','coleccion.codigo_coleccion')
		->where('modelo.RUC_empresa', Auth::user()->RUC_empresa )
		->get();

		$grupos = DB::table('grupo_trabajo')->select('codigo_grupo_trabajo')->get();

		$procesos = DB::table('grupo_trabajo')->distinct()->select('proceso')->get();
		//Code by Mario_Huaypuna ---------------------------------------------------------------------

		$models_seasons_times = DB::table('orden_pedido_produccion')  //'modelo', 'coleccion'
		->join('grupo_trabajo', 'grupo_trabajo.codigo_orden_pedido_produccion', "=", "orden_pedido_produccion.codigo_orden_pedido_produccion" )
		->join('serie_modelo', 'serie_modelo.codigo', '=', 'orden_pedido_produccion.codigo_serie_articulo')
		->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
		->join('coleccion','modelo.cod_coleccion','=','coleccion.codigo_coleccion')
		->select('orden_pedido_produccion.codigo_orden_pedido_produccion',
			'orden_pedido_produccion.codigo_serie_articulo',
			'modelo.cod_modelo',
			'modelo.cod_coleccion',
			'coleccion.nombre_coleccion',
			DB::raw('COUNT(*) as num_ordenes_produccion'),
			DB::raw('SUM(grupo_trabajo.tiempo) as total_tiempos'))
		->where('orden_pedido_produccion.RUC_empresa', Auth::user()->RUC_empresa )
		->groupBy('orden_pedido_produccion.codigo_orden_pedido_produccion',
			'orden_pedido_produccion.codigo_serie_articulo',
			'modelo.cod_modelo',
			'modelo.cod_coleccion',
			'coleccion.nombre_coleccion'
			)
		->get();

    	$colecciones = Coleccion::where('RUC_empresa', Auth::user()->RUC_empresa)->get();
		// Fin de codigo
		//,compact($ordenVenta,$SumaTotales)
		return view('Produccion.Reportes.ReporteTiempo.index',['grupo_data'=>$grupo_data, 'modelos'=>$modelos, 'grupos'=>$grupos, 'procesos'=>$procesos, "models_seasons_times"=>$models_seasons_times, "colecciones"=>$colecciones ]);
	}

	public function create()
	{

	}
public function store(Request $data)
	{

	}
public function show()
	{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
	}
public function edit()
	{
 /* return Redirect::to('logistica/clasificacion');*/
	}
public function update()
  	{
  	}
public function destroy()
	{

	}


}
