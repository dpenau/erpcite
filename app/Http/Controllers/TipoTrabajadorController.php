<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\TipoTrabajadorModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class TipoTrabajadorController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        if($request)
        {
            $trabajadores=DB::table('tipo_trabajador')->get();
            return view('Mantenimiento.Tipo_Trabajador.index',["trabajadores"=>$trabajadores]);
        }


    }
    public function create(Request $request)
    {
        if($request)
        {
            return view("Mantenimiento\Tipo_Trabajador\create");
        }
    }
    public function store()
    {
        $identificador=rand(10000,99999);
        $trabajador=new TipoTrabajadorModel;
        $trabajador->cod_tipo_trabajador=$identificador;
        $trabajador->descrip_tipo_trabajador=Input::get('descripcion');
        $trabajador->estado_tipo_trabajador=1;
        $trabajador->save();
        session()->flash('success','Tipo de Trabajador Registrado');
        return Redirect::to('Mantenimiento/Tipo_Trabajador');
    }
    public function show()
    {
        return view('Mantenimiento\Tipo_Trabajador\index');
    }
    public function edit($id)
    {
        return Redirect::to('Mantenimiento/Tipo_Trabajador');
    }
    public function update()
    {
      $cod=Input::get('cod_tipo_trabajador_editar');
      $nombre=Input::get('nombre');
      $act=TipoTrabajadorModel::where('cod_tipo_trabajador',$cod)
      ->update(['descrip_tipo_trabajador'=>$nombre]);
      session()->flash('success','Tipo de Trabajador Actualizado');
        return Redirect::to('Mantenimiento/Tipo_Trabajador');
    }
    public function destroy($id)
    {
      $cod=Input::get('cod_tipo_trabajador_eliminar');
      $accion=Input::get('accion');
      if($accion==0)
      {
        $mensaje="Desactivado";
      }
      else {
        $mensaje="Activado";
      }
      $act=TipoTrabajadorModel::where('cod_tipo_trabajador',$cod)
      ->update(['estado_tipo_trabajador'=>$accion]);
      session()->flash('success','Tipo de Trabajador '.$mensaje);
        return Redirect::to('Mantenimiento/Tipo_Trabajador');
    }
}
