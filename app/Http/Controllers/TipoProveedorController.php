<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\TipoPorveedorModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class TipoProveedorController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function index(Request $request)
  {
      if($request)
      {
          $tipo=DB::table('tipo_proveedor')->paginate(20);
          $rubro=DB::table('subcategoria')->get();
          return view('Mantenimiento.tipo_proveedor.index',["tipo"=>$tipo,"rubro"=>$rubro]);
      }


  }
  public function create(Request $request)
  {
      if($request)
      {
          $rubro=DB::table('subcategoria')->get();
          return view("Mantenimiento.tipo_proveedor.create",["rubro"=>$rubro]);
      }
  }
  public function store()
  {
      $identificador=rand(10000,99999);
      $contribuyente=new TipoPorveedorModel;
      $contribuyente->cod_tipo_proveedor=$identificador;
      $contribuyente->descrip_tipo_proveedor=Input::get('descripcion');
      $contribuyente->rubro_proveedor=Input::get('rubro');
      $contribuyente->save();
      session()->flash('success','Tipo de proveedor Registrado');
      return Redirect::to('Mantenimiento/tipo_proveedor');
  }
  public function show()
  {
      return view('Mantenimiento.tipo_proveedor.index');
  }
  public function edit($id)
  {
      return Redirect::to('Mantenimiento/tipo_proveedor');
  }
  public function update()
  {
    $cod=Input::get('cod');
    $decrip=Input::get('descripcion');
    $rubro=Input::get('rubro');
    $act=TipoPorveedorModel::where('cod_tipo_proveedor',$cod)
    ->update(['descrip_tipo_proveedor'=>$decrip,'rubro_proveedor'=>$rubro]);
      session()->flash('success','Tipo de proveedor Actualizado');
      return Redirect::to('Mantenimiento/tipo_proveedor');
  }
  public function destroy($id)
  {
      return Redirect::to('Mantenimiento/tipo_proveedor');
  }
}
