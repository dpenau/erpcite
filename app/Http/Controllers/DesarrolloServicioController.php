<?php

namespace erpCite\Http\Controllers;

use erpCite\DesarrolloServicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesarrolloServicioController extends Controller
{
    public function create(Request $request)
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        //dd($ruc_empresa);
        $hormas = DesarrolloServicio::create(
            [
                'proceso_id'=>$request->proceso_id,
                'RUC_empresa'=>$ruc_empresa,
                'descripcion_material'=>$request->descripcion_material,
                'importe'=>$request->importe,
                'estado_registro'=>'A',
            ]
            );

        return redirect('costos/indirectos/operacion');
    }
    public function update($idDesarrolloservicio, Request $request)
    {
        $desarrollo_material =  DesarrolloServicio::find($idDesarrolloservicio);
        $desarrollo_material->fill([
            'proceso_id'=>$request->proceso_id,
            'descripcion_material'=>$request->descripcion_material,
            'importe'=>$request->importe,
            'estado_registro'=>'A',
        ])->save();
        return redirect('costos/indirectos/operacion');
    }
}
