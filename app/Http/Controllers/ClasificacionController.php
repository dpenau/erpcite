<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;

use erpCite\SubcategoriaModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\ClasificacionFormRequest;
use DB;
class ClasificacionController extends Controller
{
    public function __construct()
  {
    $this->middleware('admin');
  }
  public function index(Request $request)
  {
    if ($request) {
      $clasificacion=DB::table('subcategoria')->get();
      return view('Mantenimiento.Subcategoria.index',["clasificacion"=>$clasificacion]);
    }
  }
  public function create(Request $request)
  {
    if($request)
    {
      $categoria=DB::table('categoria')->get();
      return view("Mantenimiento.Subcategoria.create",["categoria"=>$categoria]);
    }

  }
  public function store()
  {
    $identificador=rand(100,999);
    $subcategoria=new SubcategoriaModel;
    $subcategoria->cod_subcategoria=$identificador;
    $subcategoria->cod_categoria=Input::get('categoria');
    $subcategoria->nom_subcategoria=Input::get('subcategoria');
    $subcategoria->estado_subcategoria=1;
    $subcategoria->save();
    session()->flash('success','Subcategoría Registrado');
    return Redirect::to('Mantenimiento/Subcategoria');
  }
  public function show()
  {
    return view('Mantenimiento.Subcategoria.index',["clasificacion"=>$clasi]);
  }
  public function edit($id)
  {
    return Redirect::to('Mantenimiento/Subcategoria');
  }
  public function update()
  {
    $id=Input::get("cod");
    $nombre=Input::get("nombre");
    $act=SubcategoriaModel::where('cod_subcategoria',$id)
    ->update(['nom_subcategoria'=>$nombre]);
    session()->flash('success','Subcategoria Actualizada');
    return Redirect::to('Mantenimiento/Subcategoria');
  }
  public function destroy($id)
  {
    $id=Input::get("cod_eliminar");
    $accion=Input::get("accion");
    if($accion==0)
    {
      $mensaje="Desactivado";
    }
    else {
      $mensaje="Activado";
    }
    $act=SubcategoriaModel::where('cod_subcategoria',$id)
    ->update(['estado_subcategoria'=>$accion]);
    session()->flash('success','Subcategoria '.$mensaje);
    return Redirect::to('Mantenimiento/Subcategoria');
  }
}
