<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Serie;
use erpCite\Empresa;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class SeriesCalzadoController extends Controller
{
  public function __construct()
{
  $this->middleware('desarrollo');
}
public function index(Request $request)
{
if ($request) {
    $Series=DB::table('serie')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
 /*$linea=DB::table('orden_produccion')->join('cod_linea','orden_produccion.cod_linea','=','linea.cod_linea')->where('RUC_empresa',Auth::user()->RUC_empresa)->paginate(10);
*/

    return view('Produccion.series_calzado.index',["Series"=>$Series]);
  }
//return view('Produccion.orden_produccion.index');
}
public function create(Request $request)
{

    return view("Produccion.series_calzado.create");

}
public function store()
{

        $tallaInicial=Input::get('tallaInicial');
        $tallaFinal=Input::get('tallaFinal');
        $resta=$tallaFinal-$tallaInicial;

        if($resta<9&&$tallaInicial<$tallaFinal)
        {

            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $SerieCalzados=new Serie;
            $empresa=Auth::user()->RUC_empresa;
            $identificador=rand(10000,99999);

            $SerieCalzados->cod_serie=$identificador;
            $SerieCalzados->nombre_serie=Input::get('nombreSerie');
            $SerieCalzados->tallaInicial=$tallaInicial;
            $SerieCalzados->tallaFinal=$tallaFinal;
            $SerieCalzados->RUC_empresa=$empresa;
            $SerieCalzados->estado_serie=1;
            $SerieCalzados->save();
            session()->flash('success','Serie de tallas creada');
              return Redirect::to('Produccion/series_calzado');
        }

          if($tallaInicial>$tallaFinal){
             session()->flash('error','Talla Inicial tiene que ser menor a talla final');
            return Redirect::to('Produccion/series_calzado/create');
          }
          if($resta>9){
          session()->flash('error','Rango maximo de serie de tallas es 9');
            return Redirect::to('Produccion/series_calzado/create');
        }





}
public function show()
{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
}
public function edit($id)
{
 /* return Redirect::to('logistica/clasificacion');*/
}
public function update($data)
  {
       $tallaInicial=Input::get('tallaInicial');
        $tallaFinal=Input::get('tallaFinal');
                $resta=$tallaFinal-$tallaInicial;

if($resta<9&&$tallaInicial<$tallaFinal)
        {

    $clasificacion=Serie::findOrFail(Input::get('cod_serie_mod'));
    //$clasificacion->rta=Input::get('cod_serie_mod');
    $clasificacion->nombre_serie=Input::get('nombre_serie_mod');
    $clasificacion->tallaInicial=Input::get('tallaInicial');
    $clasificacion->tallaFinal=Input::get('tallaFinal');

    $clasificacion->update();
    session()->flash('success','Series Actualizadas');
    return Redirect::to('Produccion/series_calzado');

}
        if($tallaInicial>$tallaFinal){
             session()->flash('error','Talla Inicial tiene que ser menor a talla final');
            return Redirect::to('Produccion/series_calzado');
          }
          if($resta>9){
          session()->flash('error','Rango maximo de serie de tallas es 9');
            return Redirect::to('Produccion/series_calzado');
        }

  }
public function destroy()
{
  $email=Input::get('email');
  $estado=Input::get('estado');
  if($estado==0){$mensaje="Eliminada";}
  else{$mensaje="Activado";}
  $act=Serie::where('cod_serie',$email)
  ->update(['estado_serie'=>$estado]);
    session()->flash('success','Serie'.$mensaje);
  return Redirect::to('Produccion/series_calzado');
}
}
