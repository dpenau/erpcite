<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\Coleccion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class ColeccionCalzadoController extends Controller
{
  public function __construct()
  {
    $this->middleware('desarrollo');
  }
  public function index(Request $request)
  {
  if ($request) {
      $Lineas=DB::table('linea')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
      $coleccion=DB::table('coleccion')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
      return view('Produccion.lineas_calzado.index',["Lineas"=>$Lineas,"coleccion"=>$coleccion]);
    }
  }
  public function create(Request $request)
  {
    return view("Produccion.coleccion.create");
  }
  public function store()
  {
    $empresa=Auth::user()->RUC_empresa;
    $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
    $siglax = $sigla[0]->siglas;
    $nombre=Input::get('nombre');
    $coleccion=new Coleccion;
    $coleccion->codigo_coleccion=$siglax."-".rand(10000,99999);
    $coleccion->nombre_coleccion=$nombre;
    $coleccion->RUC_empresa=$empresa;
    $coleccion->estado_coleccion=1;
    $coleccion->save();
    session()->flash('success','Coleccion de calzado registrada');
    return Redirect::to('Produccion/coleccion');
  }
  public function show()
  {
   /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
  }
  public function edit($id)
  {
   /* return Redirect::to('logistica/clasificacion');*/
  }
  public function update()
  {
    $codigo=Input::get("cod_coleccion");
    $nombre=Input::get('nombre');
    $act=Coleccion::Where('codigo_coleccion',$codigo)
    ->update(['nombre_coleccion'=>$nombre]);
    session()->flash('success','Coleccion de calzado Actualizado');
    return Redirect::to('Produccion/coleccion');
  }
  public function destroy()
  {
    $codigo=Input::Get('codigo');
    $accion=Input::Get('accion');
    if($accion==1)
    {
      $mensaje="Activado";
    }
    else {
      $mensaje="desactivado";
    }
    $act=Coleccion::Where('codigo_coleccion',$codigo)
    ->update(['estado_coleccion'=>$accion]);
    session()->flash('success','Coleccion '.$mensaje);
    return Redirect::to('Produccion/coleccion');
  }
}
