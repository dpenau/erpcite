<?php

namespace erpCite\Http\Controllers;

use DB;
use erpCite\FichaMateriales;
use erpCite\Modelo;
use erpCite\ModeloCombinacion;
use erpCite\OperacionManoObra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class EliminacionCombinacionFichaController extends Controller
{
    var $id_ficha_producto_nueva =0;
    public function __construct()
    {
        $this->middleware('desarrollo');
    }
   


    
   
    public function destroy()
    {
        $material_F = Input::get('material_ficha');
        $email = Input::get('email');
        $estado = Input::get('estado');
        $cod_modelo = Input::get('codigo_material');
        if($material_F==10){
            $cod_combina = Input::get('cod_combina');
           // $act = FichaMateriales::where('cod_ficha_m', $cod_modelo)->delete();
            session()->flash('success', 'Material en Ficha de Producto eliminado satisfactoriamente' );
            return Redirect::to('Produccion/combinacion_ficha/ficha_producto/'.$cod_combina);
        }else{
            if ($estado == 0) {$mensaje = "Desactivado";} else {
                $mensaje = "Activado";
            }
            $act = ModeloCombinacion::where('cod_combinacion', $email)
                ->update(['estado_modelo' => $estado]);
            session()->flash('success', 'Codigo de Combinación de Modelo ' . $mensaje);
            return Redirect::to('/Produccion/combinacion_calzado/listado/' . $cod_modelo);
        }
    }
  

}
