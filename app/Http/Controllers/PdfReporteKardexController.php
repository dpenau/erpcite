<?php

namespace erpCite\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PdfReporteKardexController extends Controller
{
    public function material_normal()
    {
        $kind_subcategoria = Input::Get('tipoReporte');

        $lista_materiales = "";
        // recorrer materiales normales
        if ($kind_subcategoria == "1") {
            $lista_subcategoria = Input::Get('subcatego');
            $fecha_inicio = Input::Get('desde_fecha');
            $fecha_final = Input::Get('hasta_fecha');

            $pdf = \App::make('dompdf.wrapper');
            //$pdf->setPaper('a4','landscape');
            $pdf->loadHTML($this->convert_data_tienda_normal($lista_subcategoria, $fecha_inicio, $fecha_final));
        } // recorrer materiales por tallas
        else {
            $lista_subcategoria_t = Input::Get('subcatego_t');
            $fecha_inicio = Input::Get('desde_fecha');
            $fecha_final = Input::Get('hasta_fecha');

            $pdf = \App::make('dompdf.wrapper');
            //$pdf->setPaper('a4','landscape');
            $pdf->loadHTML($this->convert_data_tienda_talla($lista_subcategoria_t, $fecha_inicio, $fecha_final));
        }

        return $pdf->stream();

    }

    public function store(Request $request)
    {
        if ($request) {
            $accion = Input::Get('accion');
            $area = Input::Get('area_reporte');
            $material = Input::Get('material_reporte');
            $fecha_inicio = Input::Get('fecha_inicio');
            $fecha_final = Input::Get('fecha_final');
            $subcategoria = Input::get('subcategoria');
            $almacen = Input::Get('almacen_reporte');
            $pdf = \App::make('dompdf.wrapper');
            //$pdf->setPaper('a4','landscape');
            $pdf->loadHTML($this->convert_data($almacen, $accion, $area, $material, $fecha_inicio, $fecha_final, $subcategoria));
            return $pdf->stream();
        }
    }

    public function get_cod_material($lista_subcategoria)
    {
        $empresa = Auth::user()->RUC_empresa;
        $lista_cod_materiales = [];
        $idx = 0;
        for ($i = 0; $i < count($lista_subcategoria); ++$i) {
            $aux = DB::table('material')
                ->where('material.RUC_empresa', '=', $empresa)
                ->where('material.cod_subcategoria', '=', $lista_subcategoria[$i])
                ->where('material.estado_material', '=', 1)
                ->get();
            for ($j = 0; $j < count($aux); ++$j) {
                $lista_cod_materiales[$idx] = $aux[$j]->cod_material;
                $idx += 1;
            }
        }

        return $lista_cod_materiales;
    }

    public function get_data_kardex_normal_movements($cod_material, $fecha_inicio, $fecha_final)
    {
        $from = date($fecha_inicio);
        $to = date($fecha_final);
        $empresa = Auth::user()->RUC_empresa;
        return DB::table('kardex_cab')
            ->join('material', 'kardex_cab.cod_material', '=', 'material.cod_material')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->join('categoria', 'subcategoria.cod_categoria', '=', 'categoria.cod_categoria')
            ->join('almacen', 'material.cod_almacen', '=', 'almacen.cod_almacen')
            ->join('unidad_compra', 'material.unidad_compra', '=', 'unidad_compra.cod_unidad_medida')
            ->whereDate('kardex_cab.fecha', '>=', $from)
            ->whereDate('kardex_cab.fecha', '<=', $to)
            ->where('kardex_cab.RUC_empresa', '=', $empresa)
            ->where('kardex_cab.cod_material', '=', $cod_material)
            ->where('kardex_cab.cantidad', '!=', 0)
            ->get();
    }

    public function get_data_kardex_talla_movements($cod_material, $fecha_inicio, $fecha_final)
    {
        $from = date($fecha_inicio);
        $to = date($fecha_final);
        $empresa = Auth::user()->RUC_empresa;

        //Retorna todos los registros de kardex que coincidan con el cod_material ingresado
        return DB::table('kardex_cab_talla')
            ->join('material', 'kardex_cab_talla.cod_material', '=', 'material.cod_material')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->join('categoria', 'subcategoria.cod_categoria', '=', 'categoria.cod_categoria')
            ->join('almacen', 'material.cod_almacen', '=', 'almacen.cod_almacen')
            ->join('unidad_compra', 'material.unidad_compra', '=', 'unidad_compra.cod_unidad_medida')
            ->whereDate('kardex_cab_talla.fecha', '>=', $from)
            ->whereDate('kardex_cab_talla.fecha', '<=', $to)
            ->where('kardex_cab_talla.RUC_empresa', '=', $empresa)
            ->where('kardex_cab_talla.cod_material', '=', $cod_material)
            ->where('kardex_cab_talla.cantidad', '!=', 0)
            ->get();
    }

    public function get_data_detail_kardex_talla($arr_entrada_kardex)
    {
        $list_kardex_talla = [];
        $idx = 0;
        foreach ($arr_entrada_kardex as $index) {
            $aux = DB::table('kardex_cab_talla_tallas')
                ->where('kardex_cab_talla_tallas.cod_kardex', '=', $index->cod_kardex)
                ->get();
            $list_kardex_talla[$idx] = $aux;
            $idx += 1;
        }
        return $list_kardex_talla;
    }

    public function get_cabecera()
    {
        $cabecera = DB::table('empresa')
            ->where('RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->get();
        return $cabecera;
    }

    public function get_names_subcategorias($lista_subcategoria)
    {
        $nombres_subcategorias = [];
        $idx = 0;
        foreach ($lista_subcategoria as $cod_subcat) {
            $aux = DB::table('subcategoria')
                ->where('subcategoria.cod_subcategoria', '=', $cod_subcat)
                ->get();
            $nombres_subcategorias[$idx] = $aux[0]->nom_subcategoria;
            $idx += 1;
        }
        return $nombres_subcategorias;
    }

    public function get_name_categoria($subcategoria_cod)
    {
        $aux = DB::table('subcategoria')
            ->join('categoria', 'categoria.cod_categoria', '=', 'subcategoria.cod_categoria')
            ->where('subcategoria.cod_subcategoria', '=', $subcategoria_cod)
            ->get();
        return $aux[0]->nom_categoria;
    }

    public function convert_data_tienda_normal($lista_subcategoria, $fecha_inicio, $fecha_final)
    {
        $nombres_subcategorias = $this->get_names_subcategorias($lista_subcategoria);
        $categoria_nombre = $this->get_name_categoria($lista_subcategoria[0]);
        $cod_materiales = $this->get_cod_material($lista_subcategoria);
        $detalle_body = [];
        for ($i = 0; $i < count($cod_materiales); ++$i) {
            $detalle_body[$i] = $this->get_data_kardex_normal_movements($cod_materiales[$i], $fecha_inicio, $fecha_final);
        }
        $img = $this->get_cabecera();
        $photo = "";
        $output = '<html><head>


        <style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    .column {
      float: left;
      width: 50%;
      padding: 5px;
    }
    .row::after {
      content: "";
      clear: both;
      display: table;
    }
    .caja {
        background-color: #fff;
        border-style: solid;
        border-width: 1px;
        border-color: #ccc;
        border-radius: 2px;
        border-top: solid 0px;
    }
    </style>

    </head><body>';
        foreach ($img as $i) {
            if ($i->imagen != "") {
                $photo = $i->imagen;
            }
        }
        if ($photo == "") {
            $output .= '
                <h3>Sin foto</h3>
            ';
        } else {
            $output .= '
                <header>
                <div class="row">
                    <div class="column">
                    <img src="photo/' . $photo . '" alt="" style="width:120px;" class="img-rounded">
                    </div>
                    <div class="column" style="margin-top:3%;">
                    <img src="photo/pie3.png" width="100%"/>
                    </div>
                </div>
                </header>
                <br>
            ';
        }
        $output .= '<h2 style="text-align: center;">Movimiento de Material Normal</h2>
        <fieldset>

            <div class="row">
                <div class="column" >
                    <table  style="text-align:center; border-collapse: collapse; border: 1px solid black;  width: 100%;">
                        <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">Categoria</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $categoria_nombre . '</td>
                        </tr>
                    </table>
                </div>
                <div class="column">
                   <table style="width: 100%; text-align:center; ">
                        <tr>
                        <td>Fecha</td>
                        <td>' . $fecha_inicio . ' - ' . $fecha_final . '</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="column">
                    <table  style="text-align:center; border-collapse: collapse; border: 1px solid black;  width: 100%;">
                    ';
        $vari = 0;
        foreach ($nombres_subcategorias as $subcat) {
            if ($vari == 0) {
                $output .= '  <tr>
                    <td style="border-collapse: collapse; border: 1px solid black;">Subcategoría (s): </td>
                ';
            } else {
                $output .= '  <tr>
                    <td style="border-collapse: collapse; border: 0px solid black;"></td>
                ';
            }
            $vari = $vari + 1;
            $output .= '<td style="border-collapse: collapse; border: 1px solid black;">' . $subcat . '</td>
            </tr>';
        }
        $vari = 0;
        $output .= '  </table></div></div></fieldset>';
        $table_ingresos = '
            <h3>Ingresos</h3>
            <table style="text-align:center; border-collapse: collapse; border: 1px solid black; margin-left: 15px;  margin-right: 10px; font-size: 12px; width: 100%;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Fecha</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Código</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoría</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Descripción</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Cantidad</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Almacen</th>
            </tr>
        ';
        $table_salidas = '
            <h3>Salidas</h3>
            <table style="text-align:center; border-collapse: collapse; border: 1px solid black; margin-left: 15px;  margin-right: 10px; font-size: 12px; width: 100%;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Fecha</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Código</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoría</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Descripción</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Cantidad</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Almacen</th>
            </tr>
        ';
        $table_devoluciones = '
            <h3>Devoluciones</h3>
            <table style="text-align:center; border-collapse: collapse; border: 1px solid black; margin-left: 15px;  margin-right: 10px; font-size: 12px; width: 100%;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Fecha</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Código</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoría</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Descripción</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Cantidad</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Almacen</th>
            </tr>
        ';

        foreach ($detalle_body as $material_item) {
            foreach ($material_item as $item) {
                $fecha_split = explode(" ", $item->fecha);
                $anio_mes_dia = explode("-", $fecha_split[0]);
                $fecha_format = $anio_mes_dia[2] . '/' . $anio_mes_dia[1] . '/' . $anio_mes_dia[0];
                if ($item->movimiento == 1) {
                    $table_ingresos .= '
                    <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $fecha_format . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cod_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_subcategoria . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->descrip_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cantidad . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->magnitud_unidad_medida . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_almacen . '</td>
                    </tr>
                    ';
                } elseif ($item->movimiento == 2) {
                    $table_salidas .= '
                    <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $fecha_format . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cod_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_subcategoria . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->descrip_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cantidad . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->magnitud_unidad_medida . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_almacen . '</td>
                    </tr>
                    ';
                } elseif ($item->movimiento == 0) {
                    $table_devoluciones .= '
                    <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $fecha_format . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cod_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_subcategoria . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->descrip_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cantidad . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->magnitud_unidad_medida . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_almacen . '</td>
                    </tr>
                    ';
                }
            }
        }

        $output .= $table_ingresos . '</table>';
        $output .= $table_salidas . '</table>';
        $output .= $table_devoluciones . '</table>';

        $output .= '</body></html>';
        return $output;
    }

    public function convert_data_tienda_talla($lista_subcategoria, $fecha_inicio, $fecha_final)
    {
        $nombres_subcategorias = $this->get_names_subcategorias($lista_subcategoria);
        $categoria_nombre = $this->get_name_categoria($lista_subcategoria[0]);
        $cod_materiales = $this->get_cod_material($lista_subcategoria);
        $detalle_body = [];
        for ($i = 0; $i < count($cod_materiales); ++$i) {
            $detalle_body[$i] = $this->get_data_kardex_talla_movements($cod_materiales[$i], $fecha_inicio, $fecha_final);
        }
        $detalle_body_por_talla = [];
        for ($i = 0; $i < count($detalle_body); ++$i) {
            $detalle_body_por_talla[$i] = $this->get_data_detail_kardex_talla($detalle_body[$i]);
        }
        $img = $this->get_cabecera();
        $photo = "";
        $output = '<html><head>


        <style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {
          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    .column {
      float: left;
      width: 50%;
      padding: 5px;
    }

    .row::after {
      content: "";
      clear: both;
      display: table;
    }
    .caja {
        background-color: #fff;
        border-style: solid;
        border-width: 1px;
        border-color: #ccc;
        border-radius: 2px;
        border-top: solid 0px;
    }
    </style>

    </head><body>';
        foreach ($img as $i) {
            if ($i->imagen != "") {
                $photo = $i->imagen;
            }
        }
        if ($photo == "") {
            $output .= '
      <h3>Sin foto</h3>
      ';
        } else {
            $output .= '
      <header>
      <div class="row">
        <div class="column">
          <img src="photo/' . $photo . '" alt="" style="width:120px;" class="img-rounded">
        </div>
        <div class="column" style="margin-top:3%;">
          <img src="photo/pie3.png" width="100%"/>
        </div>
      </div>
      </header>
      <br>
      ';
        }
        $output .= '<h2 style="text-align: center;">Movimiento de Material Por Tallas</h2>
        <fieldset>
        <div class="row">
            <div class="column" >
                <table  style="text-align:center; border-collapse: collapse; border: 1px solid black;  width: 100%;">
                    <tr>
                    <td style="border-collapse: collapse; border: 1px solid black;">Categoria</td>
                    <td style="border-collapse: collapse; border: 1px solid black;">' . $categoria_nombre . '</td>
                    </tr>
                </table>
            </div>
            <div class="column" >
            <table style="width: 100%; text-align:center; ">
                    <tr>
                    <td>Fecha</td>
                    <td>' . $fecha_inicio . ' - ' . $fecha_final . '</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
        <div class="column">
            <table  style=" text-align:center; border-collapse: collapse; border: 1px solid black;  width: 100%;">
            ';

                    $vari = 0;
                    foreach ($nombres_subcategorias as $subcat) {
                        if ($vari == 0) {
                            $output .= '  <tr>
                                <td style="border-collapse: collapse; border: 1px solid black;">Subcategoría (s): </td>
                            ';
                        } else {
                            $output .= '  <tr>
                                <td style="border-collapse: collapse; border: 0px solid black;"></td>
                            ';
                        }
                        $vari = $vari + 1;
                        $output .= '<td style="border-collapse: collapse; border: 1px solid black;">' . $subcat . '</td>
                        </tr>';
                    }
                    $vari = 0;
                    $output .= '  </table></div></div></fieldset>';

        $table_ingresos = '
            <h3>Ingresos</h3>
            <table style="text-align:center; border-collapse: collapse; border: 1px solid black; margin-left: 15px;  margin-right: 10px; font-size: 12px; width: 100%;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Fecha</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Código</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoría</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Descripción</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T1</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T2</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T3</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T4</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T5</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T6</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T7</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Almacen</th>
            </tr>
        ';
        $table_salidas = '
            <h3>Salidas</h3>
            <table style="text-align:center; border-collapse: collapse; border: 1px solid black; margin-left: 15px;  margin-right: 10px; font-size: 12px; width: 100%;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Fecha</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Código</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoría</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Descripción</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T1</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T2</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T3</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T4</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T5</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T6</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T7</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Almacen</th>
            </tr>
        ';
        $table_devoluciones = '
            <h3>Devoluciones</h3>
            <table style="text-align:center; border-collapse: collapse; border: 1px solid black; margin-left: 15px;  margin-right: 10px; font-size: 12px; width: 100%;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Fecha</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Código</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoría</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Descripción</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T1</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T2</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T3</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T4</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T5</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T6</th>
                <th style="border-collapse: collapse; border: 1px solid black;">T7</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Almacen</th>
            </tr>
        ';

        for ($i = 0; $i < count($detalle_body); ++$i) {
            $idx = 0;
            foreach ($detalle_body[$i] as $item) {
                $fecha_split = explode(" ", $item->fecha);
                $anio_mes_dia = explode("-", $fecha_split[0]);
                $fecha_format = $anio_mes_dia[2] . '/' . $anio_mes_dia[1] . '/' . $anio_mes_dia[0];
                if ($item->movimiento == 1) {
                    $table_ingresos .= '
                    <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $fecha_format . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cod_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_subcategoria . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->descrip_material . '</td>';
                    for ($j = 0; $j < 7; ++$j) {
                        if ($j < count($detalle_body_por_talla[$i][$idx])) {
                            $table_ingresos .= '
                                <td style="border-collapse: collapse; border: 1px solid black;"><small>'
                            . $detalle_body_por_talla[$i][$idx][$j]->talla . '</small><br /> ' . $detalle_body_por_talla[$i][$idx][$j]->cantidad .
                                '</td>
                            ';
                        } else {
                            $table_ingresos .= '
                                <td style="border-collapse: collapse; border: 1px solid black;"> - <br /> - </td>
                            ';
                        }
                    }
                    $table_ingresos .= '<td style="border-collapse: collapse; border: 1px solid black;">' . $item->magnitud_unidad_medida . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_almacen . '</td>
                    </tr>
                    ';
                } elseif ($item->movimiento == 2) {
                    $table_salidas .= '
                    <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $fecha_format . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cod_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_subcategoria . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->descrip_material . '</td>';
                    for ($j = 0; $j < 7; ++$j) {
                        if ($j < count($detalle_body_por_talla[$i][$idx])) {
                            $table_salidas .= '
                                <td style="border-collapse: collapse; border: 1px solid black;"><small>'
                            . $detalle_body_por_talla[$i][$idx][$j]->talla . '</small><br /> ' . $detalle_body_por_talla[$i][$idx][$j]->cantidad .
                                '</td>
                            ';
                        } else {
                            $table_salidas .= '
                                <td style="border-collapse: collapse; border: 1px solid black;"> - <br /> - </td>
                            ';
                        }
                    }
                    $table_salidas .= '<td style="border-collapse: collapse; border: 1px solid black;">' . $item->magnitud_unidad_medida . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_almacen . '</td>
                    </tr>
                    ';
                } elseif ($item->movimiento == 0) {
                    $table_devoluciones .= '
                    <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $fecha_format . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->cod_material . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_subcategoria . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->descrip_material . '</td>';
                    for ($j = 0; $j < 7; ++$j) {
                        if ($j < count($detalle_body_por_talla[$i][$idx])) {
                            $table_devoluciones .= '
                                <td style="border-collapse: collapse; border: 1px solid black;"><small>'
                            . $detalle_body_por_talla[$i][$idx][$j]->talla . '</small><br /> ' . $detalle_body_por_talla[$i][$idx][$j]->cantidad .
                                '</td>
                            ';
                        } else {
                            $table_devoluciones .= '
                                <td style="border-collapse: collapse; border: 1px solid black;"> - <br /> - </td>
                            ';
                        }
                    }
                    $table_devoluciones .= '<td style="border-collapse: collapse; border: 1px solid black;">' . $item->magnitud_unidad_medida . '</td>
                        <td style="border-collapse: collapse; border: 1px solid black;">' . $item->nom_almacen . '</td>
                    </tr>
                    ';
                }
                $idx += 1;
            }
        }

        $output .= $table_ingresos . '</table>';
        $output .= $table_salidas . '</table>';
        $output .= $table_devoluciones . '</table>';

        $output .= ' </body></html>';
        return $output;
    }

}
