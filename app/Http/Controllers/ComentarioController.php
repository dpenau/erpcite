<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;

use erpCite\comentario;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\ClasificacionFormRequest;
use DB;
class ComentarioController extends Controller
{
    public function __construct()
  {
    $this->middleware('auth');
  }
  public function index()
  {
    return view('actualizaciones.comentarios.index');
  }
  public function store()
  {
    $idempresa=Auth::user()->RUC_empresa;
    $comentario=new comentario;
    $identificador=rand(100000,999999);
    $comentario->codigo_comentario=$identificador;
    $comentario->titulo=Input::get('titulo');
    $comentario->comentario=Input::get('comentario');
    $comentario->RUC_empresa=$idempresa;
    $comentario->save();
    session()->flash('success','Comentario enviado satisfactoriamente');
    return Redirect::to('actualizaciones/comentarios');
  }
}
