<?php

namespace erpCite\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\ReporteVentasModel;
use erpCite\ClienteModel;
use erpCite\OrdenPedidoModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;



class ReporteVentasController extends Controller
{
    public function __construct()
  {
    $this->middleware('jefe');
  }
public function index(Request $request)
  {
    
    $ordenVentas = DB::table('cliente','orden_pedido')
     ->select('orden_pedido.codigo_pedido','cliente.nombre','orden_pedido.total_pedido','fecha')
    ->join('orden_pedido','orden_pedido.codigo_cliente','=','cliente.codigo')
    ->get();

    $SumaTotales = DB::table('cliente','orden_pedido')
    ->join('orden_pedido','orden_pedido.codigo_cliente','=','cliente.codigo')
    ->sum('orden_pedido.total_pedido');

    $fechaEntera = DB::table('cliente','orden_pedido')
     ->select('fecha')
    ->join('orden_pedido','orden_pedido.codigo_cliente','=','cliente.codigo')
    ->get();

  return view('Ventas.Reportes.ReporteVentas.index',['ordenVentas'=>$ordenVentas,'SumaTotales'=>$SumaTotales]); 
    
    }
  }
    


