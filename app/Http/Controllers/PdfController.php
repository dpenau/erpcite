<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
class PdfController extends Controller
{
  public function index(Request $request){
    if ($request) {
      $query=trim($request->get('codigo'));
      $pdf=\App::make('dompdf.wrapper');
            //$pdf->setPaper('a4','landscape');
      $pdf->loadHTML($this->convert_data($query));
      return $pdf->stream();
    }
  }
  function get_data($query)
  {
    $orden_data=DB::table('detalle_orden_compra')
    ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
    ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
    ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
    ->where('detalle_orden_compra.cod_orden_compra','=',$query)
    ->orderBy('material.descrip_material','asc')
    ->get();
    return $orden_data;
  }
  function get_cabecera($query)
  {
    $cabecera=DB::table('orden_compra')
    ->join('proveedor','orden_compra.RUC_proveedor','=','proveedor.RUC_proveedor')
    ->where('cod_orden_compra','=',$query)
    ->limit(1)
    ->get();
    return $cabecera;
  }
  function get_imagen($query)
  {
    foreach ($query as $q) {
        $imagen=DB::table('empresa')->where('RUC_empresa','=',$q->RUC_empresa)->limit(1)->get();
        return $imagen;
    }
  }
  function convert_data($query)
  {
    $total=0;
    $credito=0;
    $contado=0;
    $detalle=$this->get_data($query);
    $cabecera=$this->get_cabecera($query);
    $img=$this->get_imagen($cabecera);
    $photo="";
    $output='<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {

          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {
          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body><footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>';
    foreach ($img as $i) {
        if($i->imagen!="")
        {
          $photo=$i->imagen;
        }
    }
    foreach($cabecera as $cab)
    {

      if ($photo=="") {
        $output.='
        <h1>Orden de Compra N°'.$cab->cod_orden_compra.'</h1>
        ';
      }
      else {
        $output.='
        <header>
        <div class="row">
          <div class="col-md-12">
            <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
          </div>
        </div>
        </header>
        <br>
        <br>
        <table style="border-collapse: collapse; border: 1px solid black;margin-left: auto;  margin-right: auto; font-size: 12px;">
          <tr>
            <th colspan="9" style="text-align: center;border-collapse: collapse; border: 1px solid black;">Orden de Compra N°'.$cab->cod_orden_compra.'</th>
          </tr>
          <tr>
            <td colspan="9" style="border-collapse: collapse; border: 1px solid black;">Señores: '.$cab->nom_proveedor.'</td>
          </tr>
          <tr>
            <td colspan="6" style="border-collapse: collapse; border: 1px solid black;">RUC: '.$cab->RUC_proveedor.'</td>
            <td colspan="3" style="border-collapse: collapse; border: 1px solid black;">Fecha de Entrega Aproximada:'.$cab->fecha_entrega.'</td>
          </tr>
          <tr>
            <td colspan="6" style="border-collapse: collapse; border: 1px solid black;">Direccion:'.$cab->direc_proveedor.'</td>
            <td colspan="3" style="border-collapse: collapse; border: 1px solid black;">Cta. Cte:______________</td>
          </tr>
        </table>
        ';
      }

    }
    $output.='
      <h2>Materias Primas</h2>
      <table  style="border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
          <tr>
            <th style="border-collapse: collapse; border: 1px solid black;">Subcategoria</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Detalle</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Cantidad</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Unidad Medida</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Costo sin IGV</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Total</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Tipo de Compra</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Fecha de pago</th>
          </tr>

    ';
    foreach ($detalle as $dat) {
      if($dat->cod_categoria==969)
      {
        if($dat->pago_contado==2)
        {
          $tipo="contado";
          $contado=$contado+($dat->costo_sin_igv_material*$dat->cantidad);
        }
        else {
          $tipo="credito";
          $credito=$credito+($dat->costo_sin_igv_material*$dat->cantidad);
        }
        $output.='
        <tr>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->nom_subcategoria.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">S/.'.$dat->costo_sin_igv_material.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">S/.'.$dat->costo_sin_igv_material*$dat->cantidad.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$tipo.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_deposito.'</td>
        </tr>
        ';
      }

    }
    $output.='</table>';
    $output.='
    <h2>Insumos</h2>
    <table  style="border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
        <tr>
          <th style="border-collapse: collapse; border: 1px solid black;">Subcategoria</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Detalle</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Cantidad</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Unidad Medida</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Costo sin IGV</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Total</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Tipo de Compra</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Fecha de pago</th>
        </tr>
    ';
    foreach ($detalle as $dat) {
      if($dat->cod_categoria==306)
      {
        if($dat->pago_contado==2)
        {
          $tipo="contado";
          $contado=$contado+($dat->costo_sin_igv_material*$dat->cantidad);
        }
        else {
          $tipo="credito";
          $credito=$credito+($dat->costo_sin_igv_material*$dat->cantidad);
        }
        $output.='
        <tr>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->nom_subcategoria.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">S/.'.$dat->costo_sin_igv_material.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">S/.'.$dat->costo_sin_igv_material*$dat->cantidad.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$tipo.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_deposito.'</td>
        </tr>
        ';
      }
    }
    $output.='</table>';
    $output.='
    <h2>Suministros</h2>
    <table  style="border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
        <tr>
          <th style="border-collapse: collapse; border: 1px solid black;">Subcategoria</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Detalle</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Cantidad</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Unidad Medida</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Costo sin IGV</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Total</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Tipo de Compra</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Fecha de pago</th>
        </tr>
    ';
    foreach ($detalle as $dat) {
      if($dat->cod_categoria==634)
      {
        if($dat->pago_contado==2)
        {
          $tipo="contado";
          $contado=$contado+($dat->costo_sin_igv_material*$dat->cantidad);
        }
        else {
          $tipo="credito";
          $credito=$credito+($dat->costo_sin_igv_material*$dat->cantidad);
        }
        $output.='
        <tr>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->nom_subcategoria.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">S/.'.$dat->costo_sin_igv_material.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">S/.'.$dat->costo_sin_igv_material*$dat->cantidad.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$tipo.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_deposito.'</td>
        </tr>
        ';
      }
    }
    $output.='</table>';
    $output.='<h2>Comentarios</h2>';
    foreach($cabecera as $cab)
    {
      $output.='<table style="border-collapse: collapse; border: 1px solid black; font-size:12px; width:100%;"><th><p>'.$cab->comentario_oc.'</p></th></table><br>';
      $output.='<table style="border-collapse: collapse; border: 1px solid black; font-size:12px;">';
      $output.='<tr>';
      $output.="<th colspan='3' style='border-collapse: collapse; border: 1px solid black;'>Subtotal:S/.".number_format($cab->costo_total_oc/1.18,2)."</th>";
      $output.='</tr>';
      $output.='<tr>';
      $output.=" <th colspan='3' style='border-collapse: collapse; border: 1px solid black;'>IGV:S/.".number_format(($cab->costo_total_oc/1.18)*0.18,2)."</th>";
      $output.='</tr>';
      $output.='<tr>';
      $output.=" <th colspan='3' style='border-collapse: collapse; border: 1px solid black;'>Total:S/.".number_format($cab->costo_total_oc,2)."</th>";
      $output.='</tr>';
      $output.='</table>';
    }
    $output.=' </body></html>';
    return $output;
  }


}
