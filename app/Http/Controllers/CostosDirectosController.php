<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CostosDirectosController extends Controller
{
    public function index(Request $request)
    {
        if ($request) {
            $LineasModelos = DB::table('modelo')
                ->where('modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
                ->join(
                    'coleccion',
                    'modelo.cod_coleccion',
                    '=',
                    'coleccion.codigo_coleccion'
                )
                ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
                ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
                //->join('material','modelo.cod_horma','=','material.cod_material')
                //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
                //->where(function($query){
                //  $query->orwhere('modelo.estado_modelo','=','0')
                //  ->orWhere('modelo.estado_modelo','=','1');
                //})
                ->get();
            $serie = DB::table('serie')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('estado_serie', '=', 1)
                ->get();
            $seriemodelo = DB::Table('serie_modelo')
                ->join(
                    'serie',
                    'serie_modelo.codigo_serie',
                    '=',
                    'serie.cod_serie'
                )
                ->where(
                    'serie_modelo.RUC_empresa',
                    '=',
                    Auth::user()->RUC_empresa
                )
                ->where('serie_modelo.estado', '=', '1')
                ->get();
            $todaserie = DB::table('serie')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->get();
            $capellada = DB::table('capellada')->get();
            $Lineas = DB::table('linea')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('estado_linea', '=', 1)
                ->get();
            $coleccion = DB::Table('coleccion')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('estado_coleccion', '=', 1)
                ->get();
            $horma = DB::table('material')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('cod_subcategoria', '=', '969')
                ->where('t_compra', '=', '1')
                ->orderBy('descrip_material', 'asc')
                ->get();
            return view('costos.directos.index', [
                'horma' => $horma,
                'coleccion' => $coleccion,
                'Lineas' => $Lineas,
                'LineasModelos' => $LineasModelos,
                'seriemodelo' => $seriemodelo,
                'todaserie' => $todaserie,
                'serie'=>$serie,
                'capellada' => $capellada,
            ]);
        }
        //return view('Produccion.modelo.index');
    }
}
