<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\DetalleCostoModeloMaterial;
use erpCite\CostoModelo;
use DB;
class CostosDirectosMaterialController extends Controller
{
  public function __construct()
  {
    $this->middleware('costo');
  }
  public function index( $var)
  {
    $model=explode("+",$var);
    $empresa=Auth::user()->RUC_empresa;
    $codigo_costo=DB::table('costo_modelo')
    ->where('costo_modelo.RUC_empresa','=',$empresa)
    ->where('costo_modelo.modelo_serie','=',$model[1])
    ->get();
    $detalle=[];
      for($i=0;$i<count($codigo_costo);$i++)
      {
        $detalle=DB::table('detalle_costo_modelo_materiales')
        ->join('area','detalle_costo_modelo_materiales.cod_area','=','area.cod_area')
        ->join('material','detalle_costo_modelo_materiales.cod_material','=','material.cod_material')
        ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
        ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
        ->where('detalle_costo_modelo_materiales.cod_costo_modelo','=',$codigo_costo[$i]->cod_costo_modelo)
        ->get();
      }
    $categorias=DB::table('categoria')->where('cod_categoria','<>','634')->get();
    $subcategorias=DB::table('subcategoria')
    ->where('cod_categoria','<>','634')
    ->where('estado_subcategoria','=','1')
    ->orderby('nom_subcategoria','asc')->get();
    $cambio=DB::table("configuracion_valores")
    ->where('codigo_valor','=',1)
    ->get();
    $area=DB::table("area")
    ->where('descrip_area','LIKE','P-%')
    ->where('estado_area','=','1')
    ->get();
    return view('costos.directos.costos_directos_material.index',["cambio"=>$cambio,"codigo_costo"=>$codigo_costo,"detalle"=>$detalle,"area"=>$area,"var"=>$model[0],"categorias"=>$categorias,"subcategorias"=>$subcategorias]);
  }
  public function store()
  {
    $areatabla=DB::table("area")
    ->where('descrip_area','LIKE','P-%')
    ->get();

    $identificador=Input::get('costo_codigo');
    $seriemodelo=DB::Table('costo_modelo')
    ->where('cod_costo_modelo','=',$identificador)
    ->get();
    $materiales=Input::get('id');
    $areas=Input::get('area');
    $consumo=Input::get('cantidad');
    $valor=Input::get('costo');
    $total_detalle=Input::get('total');
    $total_material=Input::get('total_materiales');
    $codareas;
    for($i=0;$i<count($areas);$i++)
    {
      for($j=0;$j<count($areatabla);$j++)
      {
          if($areatabla[$j]->descrip_area==$areas[$i])
          {
            $codareas[$i]=$areatabla[$j]->cod_area;
            break;
          }
      }
    }
    for($in=0;$in<count($materiales);$in++)
    {
      $articulo=new DetalleCostoModeloMaterial;
      $articulo->cod_detalle_costo_material=$seriemodelo[0]->modelo_serie."-".rand(100000,999999);
      $articulo->cod_costo_modelo=$identificador;
      $articulo->cod_material=$materiales[$in];
      $articulo->cod_area=$codareas[$in];
      $articulo->consumo_por_par=$consumo[$in];
      $articulo->valor_unitario=$valor[$in];
      $articulo->total=$total_detalle[$in];
      $articulo->estado_material_costos=1;
      $articulo->save();
    }
    $act=CostoModelo::where('cod_costo_modelo',$identificador)
    ->update(['total_materiales'=>$total_material]);
    session()->flash('success','Costo de Materiales Registrado Satisfactoriamente');
    return Redirect::to('costo/vermodelos');
  }
  public function update()
  {
    $areatabla=DB::table("area")
    ->where('descrip_area','LIKE','P-%')
    ->get();
    $identificador=Input::get('costo_codigo');
    $seriemodelo=DB::Table('costo_modelo')
    ->where('cod_costo_modelo','=',$identificador)
    ->get();
    $materiales=Input::get('id');
    $areas=Input::get('area');
    $consumo=Input::get('cantidad');
    $valor=Input::get('costo');
    $total_detalle=Input::get('total');
    $total_material=Input::get('total_materiales');
    $codareas;
    for($i=0;$i<count($areas);$i++)
    {
      for($j=0;$j<count($areatabla);$j++)
      {
          if($areatabla[$j]->descrip_area==$areas[$i])
          {
            $codareas[$i]=$areatabla[$j]->cod_area;
            break;
          }
      }
    }
    $detalle=DB::table('detalle_costo_modelo_materiales')
    ->join('material','detalle_costo_modelo_materiales.cod_material','=','material.cod_material')
    ->where('detalle_costo_modelo_materiales.cod_costo_modelo','=',$identificador)
    ->get();
    for($i=0;$i<count($detalle);$i++)
    {
      $f=0;
      for($j=0;$j<count($materiales);$j++)
      {
          if($detalle[$i]->cod_material==$materiales[$j]&& $detalle[$i]->cod_area==$codareas[$j])
          {
            $f=1;
            $act=DetalleCostoModeloMaterial::where('cod_costo_modelo',$identificador)
            ->where('cod_material',$materiales[$j])->where('cod_area',$codareas[$j])
            ->update(['consumo_por_par'=>$consumo[$j],
                      'total'=>$total_detalle[$j],
                      'estado_material_costos'=>1
                    ]);
            break;
          }
      }
      if($f==0)
      {
        $act=DetalleCostoModeloMaterial::where('cod_detalle_costo_material',$detalle[$i]->cod_detalle_costo_material)
        ->where('cod_material',$detalle[$i]->cod_material)->where('cod_area',$detalle[$i]->cod_area)
        ->delete();
      }
    }
    for($i=0;$i<count($materiales);$i++)
    {
      $f=0;
      for($j=0;$j<count($detalle);$j++)
      {
          if($detalle[$j]->cod_material==$materiales[$i] && $detalle[$j]->cod_area==$codareas[$i])
          {
            $f=1;
            break;
          }
      }
      if($f==0)
      {
        $articulo=new DetalleCostoModeloMaterial;
        $articulo->cod_detalle_costo_material=$seriemodelo[0]->modelo_serie."-".rand(100000,999999);
        $articulo->cod_costo_modelo=$identificador;
        $articulo->cod_material=$materiales[$i];
        $articulo->cod_area=$codareas[$i];
        $articulo->consumo_por_par=$consumo[$i];
        $articulo->valor_unitario=$valor[$i];
        $articulo->total=$total_detalle[$i];
        $articulo->estado_material_costos=1;
        $articulo->save();
      }
    }
    $modelo=DB::table('costo_modelo')
    ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
    ->where('cod_costo_modelo',$identificador)
    ->select('serie_modelo.estado')
    ->get();
    $act=CostoModelo::where('cod_costo_modelo',$identificador)
    ->update(['total_materiales'=>$total_material,'estado'=>$modelo[0]->estado]);
    session()->flash('success','Costo de Materiales Actualizado Satisfactoriamente');
    return Redirect::to('costo/vermodelos');
  }
  public function obt_material($var)
  {
      $empresa=Auth::user()->RUC_empresa;
    $materiales=DB::table('material')
    ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
    ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
    ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
    ->where('material.RUC_empresa','=',$empresa)
    ->where('material.estado_material','=','1')
    ->where('material.cod_subcategoria','=',$var)
    ->orderBy('material.descrip_material','asc')
    ->get();
    return $materiales;
  }
}
