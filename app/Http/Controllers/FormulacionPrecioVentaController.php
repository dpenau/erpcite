<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\Area;
use erpCite\CatalogoPreciosModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class FormulacionPrecioVentaController extends Controller
{
  public function __construct()
  {
    $this->middleware('jefe');
  }
  public function index()
  {
    $mes_actual=date("m");

    $modelos=DB::table('serie_modelo')
    ->join("modelo",'serie_modelo.codigo_modelo','=','modelo.cod_modelo')
    ->join("serie",'serie_modelo.codigo_serie','=','serie.cod_serie')
    ->leftJoin('costo_modelo','serie_modelo.codigo','=','costo_modelo.modelo_serie')
    ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where(function($query){
      $query->orWhere('serie_modelo.estado','=','1');
    })
    ->get();
    $empresa=DB::table('empresa')
    ->join('regimen_renta','empresa.cod_regimen_renta','=','regimen_renta.cod_regimen_renta')
    ->where('empresa.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->get();
    $suministros=DB::table('gasto_suministro')
    ->join('area','gasto_suministro.cod_area','=','area.cod_area')
    ->where('gasto_suministro.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where(function($query){
      $query->orWhere('area.descrip_area','=','Desarrollo de Producto')
      ->orWhere('area.descrip_area','=','Logistica')
      ->orWhere('area.descrip_area','=','P-Acabado')
      ->orWhere('area.descrip_area','=','P-Alistado')
      ->orWhere('area.descrip_area','=','P-Aparado')
      ->orWhere('area.descrip_area','=','P-Corte')
      ->orWhere('area.descrip_area','=','P-Habilitado')
      ->orWhere('area.descrip_area','=','P-Montaje')
      ->orWhere('area.descrip_area','=','Seguridad y Limpieza');
    })
    ->sum('gasto_mensual_suministro');
    $mobra=DB::table('gasto_sueldos')
    ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
    ->where('gasto_sueldos.es_externo','=',0)
    ->where('gasto_sueldos.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->whereMonth('gasto_sueldos.fecha_creacion', '=', $mes_actual)
    ->where(function($query){
      $query->orWhere('area.descrip_area','=','Desarrollo de Producto')
      ->orWhere('area.descrip_area','=','Desarrollo de Producto')
      ->orWhere('area.descrip_area','=','Logistica')
      ->orWhere('area.descrip_area','=','Produccion')
      ->orWhere('area.descrip_area','=','Seguridad y Limpieza');
    })
    ->sum('gasto_mensual');
    $externo=DB::table('gasto_sueldos')
    ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
    ->where('gasto_sueldos.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('gasto_sueldos.es_externo','=',1)
    ->whereMonth('gasto_sueldos.fecha_creacion', '=', $mes_actual)
    ->sum('gasto_mensual');
    $depreciacion=DB::Table('gasto_depreciacion_mantenimiento')
    ->join('tipo_activo','gasto_depreciacion_mantenimiento.cod_tipo_activo','=','tipo_activo.cod_tipo_activo')
    ->where('gasto_depreciacion_mantenimiento.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->get();
    $generales=DB::table('datos_generales')
    ->join('retencion_utilidades','datos_generales.cod_retencion_utilidades','=','retencion_utilidades.cod_retencion_utilidades')
    ->where('datos_generales.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->get();
    $servicio=DB::table('gasto_servicios_basicos')
    ->where('gasto_servicios_basicos.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->whereMonth('gasto_servicios_basicos.fecha_creacion', '=', $mes_actual)
    ->sum('gasto_mensual');
    $today=date("Y-m-d");
    $prototipo=DB::table('gasto_desarrollo_producto')
    ->where('gasto_desarrollo_producto.estado','=','1')
    ->where('gasto_desarrollo_producto.cod_tipo_desarrollo','=','1')
    ->where('gasto_desarrollo_producto.RUC_empresa',Auth::user()->RUC_empresa)
    ->whereDate(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$generales[0]->politica_desarrollo_producto." month)"),">=",$today)
    ->sum('importe');
    $hormas=DB::table('gasto_desarrollo_producto')
    ->where('gasto_desarrollo_producto.estado','=','1')
    ->where('gasto_desarrollo_producto.cod_tipo_desarrollo','=','2')
    ->where('gasto_desarrollo_producto.RUC_empresa',Auth::user()->RUC_empresa)
    ->whereDate(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$generales[0]->politica_desarrollo_horma." month)"),">=",$today)
    ->sum('importe');
    $troqueles=DB::Table('gasto_desarrollo_producto')
    ->where('gasto_desarrollo_producto.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('gasto_desarrollo_producto.cod_tipo_desarrollo','=',3)
    ->whereDate(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$generales[0]->politica_desarrollo_troqueles." month)"),">=",$today)
    ->sum('importe');
    $sueldoadministrativo=DB::table('gasto_sueldos')
    ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
    ->where('gasto_sueldos.es_externo','=',0)
    ->where('gasto_sueldos.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->Where('area.descrip_area','=','Administracion')
    ->whereMonth('gasto_sueldos.fecha_creacion', '=', $mes_actual)
    ->sum('gasto_mensual');
    $matadministrativo=DB::table('gasto_suministro')
    ->join('area','gasto_suministro.cod_area','=','area.cod_area')
    ->where('gasto_suministro.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->Where('area.descrip_area','=','Administracion')
    ->sum('gasto_mensual_suministro');
    $administrativo=DB::Table('gasto_representacion')
    ->where('gasto_representacion.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->whereMonth('gasto_representacion.fecha_creacion', '=', $mes_actual)
    ->select(DB::raw('gasto as total'))
    ->get();
    $distribucion=DB::table('gasto_distribucion')
    ->where('gasto_distribucion.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->whereMonth('gasto_distribucion.fecha_creacion', '=', $mes_actual)
    ->sum('gasto_mensual');
    $g_repre=0;
    for ($i=0; $i < count($administrativo); $i++) {
      $g_repre=$g_repre+$administrativo[$i]->total;
    }
    $administracion=$g_repre+$matadministrativo+$sueldoadministrativo;
    $sueldo_marketing=DB::table('gasto_sueldos')
    ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
    ->where('gasto_sueldos.es_externo','=',0)
    ->where('gasto_sueldos.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->Where('area.descrip_area','=','Ventas y Marketing')
    ->whereMonth('gasto_sueldos.fecha_creacion', '=', $mes_actual)
    ->sum('gasto_mensual');
    $otrosmarketing=DB::table('gasto_otros')
    ->where('gasto_otros.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->whereMonth('gasto_otros.fecha_creacion', '=', $mes_actual)
    ->select(DB::raw('gasto as total'))
    ->get();
    $o_mark=0;
    for ($i=0; $i < count($otrosmarketing); $i++) {
      $o_mark=$o_mark+$otrosmarketing[$i]->total;
    }
    $marketing=$o_mark+$sueldo_marketing;
    $prestamo=DB::Table('gasto_financiero')
    ->where('gasto_financiero.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('gasto_financiero.estado','=',1)
    ->get();
    $letras=DB::table('gasto_financiero')
    ->where('gasto_financiero.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->Where('gasto_financiero.estado','=','3')
    ->select(DB::raw('(intereses/meses_pago) as total'))
    ->get();
    $clientes=DB::table('gasto_financiero')
    ->where('gasto_financiero.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->Where('gasto_financiero.estado','=','2')
    ->select(DB::raw('(intereses/meses_pago) as total'))
    ->get();
    $financiero=0;
    if(count($letras)!=0 && count($clientes)!=0)
    {
      $financiero=$letras[0]->total+$clientes[0]->total;
    }
    else {
      if(count($letras)!=0)
      {
        $financiero=$financiero+$letras[0]->total;
      }
      else {
        if(count($clientes)!=0)
        {
          $financiero=$financiero+$clientes[0]->total;
        }
      }
    }


    return view('costos.precio_venta',["modelos"=>$modelos,"empresa"=>$empresa,"suministros"=>$suministros
  ,"generales"=>$generales,"mobra"=>$mobra,"depreciacion"=>$depreciacion,"servicio"=>$servicio
,"prototipo"=>$prototipo,"hormas"=>$hormas,"troqueles"=>$troqueles,'administracion'=>$administracion
,"distribucion"=>$distribucion,"marketing"=>$marketing,"financiero"=>$financiero,"prestamo"=>$prestamo,"externo"=>$externo]);
}
  public function obtarticulo($var)
  {
    $array=explode("+",$var);
    $articulos=DB::table('serie_modelo')
    ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where(function($query){
      $query->orWhere('serie_modelo.estado','=','3');
    })
    ->where('serie_modelo.codigo_modelo','LIKE','%'.$array[0].'%')
    ->where('serie_modelo.codigo_serie','=',$array[1])
    ->get();
    return $articulos;
  }
  public function obtdatos($var)
  {
    $array=explode("+",$var);
    $articulos=DB::table('serie_modelo')
    ->join("modelo",'serie_modelo.codigo_modelo','=','modelo.cod_modelo')
    ->join("serie",'serie_modelo.codigo_serie','=','serie.cod_serie')
    ->join("linea",'modelo.cod_linea','=','linea.cod_linea')
    ->join("capellada",'modelo.cod_capellada','capellada.cod_capellada')
    ->join("forro",'modelo.cod_forro','forro.cod_forro')
    ->join("piso",'modelo.cod_piso','piso.cod_piso')
    ->join("plantilla",'modelo.cod_plantilla','plantilla.cod_plantilla')
    ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('serie_modelo.codigo_modelo','=',$array[0])
    ->get();
    $costo=DB::Table('costo_modelo')
    ->join('detalle_costo_modelo_materiales','costo_modelo.cod_costo_modelo','=','detalle_costo_modelo_materiales.cod_costo_modelo')
    ->join('area','detalle_costo_modelo_materiales.cod_area','=','area.cod_area')
    ->where('costo_modelo.modelo_serie',$array[1])
    ->get();
    $mano=DB::table('costo_modelo')
    ->join('detalle_costo_modelo_manoobra','costo_modelo.cod_costo_modelo','=','detalle_costo_modelo_manoobra.cod_costo_modelo')
    ->join('area','detalle_costo_modelo_manoobra.cod_area','=','area.cod_area')
    ->where('costo_modelo.modelo_serie',$array[1])
    ->get();
    $json=["datos"=>$articulos,"datos2"=>$costo,"datos3"=>$mano];
    return json_encode($json);
  }
  public function guardar(Request $request)
  {
    $articulos=Input::get('articulos');
    $cod_articulo=explode("+",$articulos);
    $preciofinal=Input::get('precio_final');
    $utilidad=Input::get('utilidad');
    $costo=Input::get('costo');
    $usuario=Auth::user()->id;
    $ruc=Auth::user()->RUC_empresa;
    $db=DB::table('catalogo_precios')
    ->where('RUC_empresa','=',$ruc)
    ->where('cod_articulo','=',$cod_articulo[1])
    ->get();
    if(count($db)>0)
    {
      $precio=CatalogoPreciosModel::where('cod_articulo',$cod_articulo[1])
      ->update(['utilidad'=>$utilidad,'costo_venta'=>$costo,'precio'=>$preciofinal,"usuario"=>$usuario]);
      session()->flash('success','Precio para modelo Actualizado');
    }
    else {
      $precio=new CatalogoPreciosModel;
      $precio->cod_articulo=$cod_articulo[1];
      $precio->precio=$preciofinal;
      $precio->usuario=$usuario;
      $precio->utilidad=$utilidad;
      $precio->costo_venta=$costo;
      $precio->RUC_empresa=$ruc;
      $precio->save();
      session()->flash('success','Precio para modelo Guardado');
    }
    return Redirect::to('costo/vermodelos');
  }
}
