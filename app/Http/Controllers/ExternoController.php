<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\ManoObraModel;
use erpCite\Empresa;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class ExternoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jefe');
    }
    public function index(Request $request)
    {
        if ($request) {
            $mes_actual=date("m");
            $sueldo=DB::Table('gasto_sueldos')
            ->where('es_externo','=',1)
            ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
            ->whereMonth('fecha_creacion', '=', $mes_actual)
            ->get();
            return view('costos.indirectos.externo.index',["sueldo"=>$sueldo]);
        }
    }
    public function create()
    {
        $area=DB::table('area')
        ->orderBy('descrip_area','asc')->get();
        return view("costos.indirectos.externo.create",["area"=>$area]);      
    }
    public function store()
    {
        //Se Registra el campo detalle_orden_compra
        $identificador=rand(100000,999999);
        $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
        $siglax = $sigla[0]->siglas;
        $res=$siglax.'-'.$identificador;
        $idempresa=Auth::user()->RUC_empresa;
        $manoobra=new ManoObraModel;
        $manoobra->id_gastos_sueldos=$res;
        $manoobra->beneficios=0;
        $manoobra->otros=Input::get('otros_sueldos');
        $manoobra->gasto_mensual=Input::get('gasto_mensual');
        $manoobra->cod_area=Input::get('cod_area');
        $manoobra->RUC_empresa=$idempresa;
        $manoobra->sueldo_mensual=0;
        $manoobra->puesto=Input::get('puesto');
        $manoobra->fecha_creacion=Input::get('fecha_creacion');
        $manoobra->es_externo=1;
        $manoobra->estado=1;
        $manoobra->save();
        session()->flash('success','Sueldo de marketing registrado satisfactoriamente');
        return Redirect::to('costos_indirectos/externo');
    }
    public function delete()
    {
        $email=Input::get('email');
        $act=ManoObraModel::where('id_gastos_sueldos',$email)
        ->delete();
        session()->flash('success','Sueldo Eliminado');
        return Redirect::to('costos_indirectos/externo');
    }
}
