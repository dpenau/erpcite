<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Empresa;
use erpCite\DatosGeneralesModel;
use erpCite\DetalleCostoModeloMano;
use erpCite\CostoModelo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class ConfCostosDirectosController extends Controller
{
  public function __construct()
{
  $this->middleware('jefe');
}
public function index(Request $request)
{

if ($request) {
    $datosGenerales=DB::table('datos_generales')
    ->join("retencion_utilidades","datos_generales.cod_retencion_utilidades","=","retencion_utilidades.cod_retencion_utilidades")
    ->where('datos_generales.RUC_empresa',Auth::user()->RUC_empresa)
    ->get();

    return view('costos.Configuracion.index',["datosGenerales"=>$datosGenerales]);
  }
}
public function create(Request $request)
{
  $reparticion=DB::Table("retencion_utilidades")
  ->get();
  $datos=DB::table('datos_generales')
  ->where('datos_generales.RUC_empresa',Auth::user()->RUC_empresa)
  ->get();
    return view('costos.Configuracion.create',["reparticion"=>$reparticion,"datos"=>$datos]);



}
public function store()
{
  $accion=Input::Get('accion');
  $empresa=Auth::user()->RUC_empresa;
  $utilidades=Input::Get('Repartición');
  $TCEACA=Input::get('TCEACA');
  $leadtime=Input::get('leadtime');
  $reproceso=Input::get('reproceso');
  $TCEACR=Input::get('TCEACR');
  $comision=Input::get('comision');
  $prodmensual=Input::get('prodmensual');
  $prodmensualbase=Input::get('prodmensual_base');
  $prodtotal=Input::get('totalprodanual');
  $pdp=Input::get('pdp');
  $pdh=Input::get('pdh');
  $pdt=Input::get('pdt');
  if ($accion==1) {
    try {
    $entrar=DatosGeneralesModel::where("RUC_empresa",$empresa)
    ->update(["cod_retencion_utilidades"=>$utilidades,  "TCEA_capital"=>$TCEACA,
"lead_time"=>$leadtime,
"porcentaje_reproceso"=>$reproceso,
"TCEA_credito"=>$TCEACR,
"porcentaje_comision_ventas"=>$comision,
"politica_desarrollo_producto"=>$pdp,
"politica_desarrollo_horma"=>$pdh,
"politica_desarrollo_troqueles"=>$pdt,
"produccion_promedio"=>$prodmensual,"total_produccion_anual"=>$prodtotal]);
      session()->flash('success','Configuracion Actualizada');
      if($prodmensualbase!=$prodmensual)
      {
        $act=DetalleCostoModeloMano::where('cod_tipo_trabajador','1')->orwhere('cod_tipo_trabajador','2')
        ->update(['estado_trabajador'=>2]);
        $actualizar=DetalleCostoModeloMano::where('cod_tipo_trabajador','1')->orwhere('cod_tipo_trabajador','2')->select('cod_costo_modelo')->get();
        $anterior="";
        for($i=0;$i<count($actualizar);$i++)
        {
          if($anterior!=$actualizar[$i]->cod_costo_modelo)
          {
            $anterior=$actualizar[$i]->cod_costo_modelo;
            $modelo=DB::table('costo_modelo')
            ->where('cod_costo_modelo','=',$anterior)
            ->select('estado')
            ->get();
            if($modelo[0]->estado==9)
            {
              $act=CostoModelo::where('cod_costo_modelo',$anterior)
              ->update(['estado'=>"8"]);
            }
            else {
              if ($modelo[0]->estado==1 || $modelo[0]->estado==3) {
                $act=CostoModelo::where('cod_costo_modelo',$anterior)
                ->update(['estado'=>"7"]);
              }
            }
          }
        }
      session()->flash('warning','Precio de costos mano de obra desactualizado');
      }
    } catch (\Exception $e) {
      	 session()->flash('error','Ocurrio un error '.$e);
    }
  }
  else {
    $entrar=new DatosGeneralesModel;
    $entrar->RUC_empresa=$empresa;
    $entrar->cod_retencion_utilidades=$utilidades;
    $entrar->TCEA_capital=$TCEACA;
    $entrar->lead_time=$leadtime;
    $entrar->porcentaje_reproceso=$reproceso;
    $entrar->TCEA_credito=$TCEACR;
    $entrar->porcentaje_comision_ventas=$comision;
    $entrar->politica_desarrollo_producto=$pdp;
    $entrar->politica_desarrollo_horma=$pdh;
    $entrar->politica_desarrollo_troqueles=$pdt;
    $entrar->produccion_promedio=$prodmensual;
    $entrar->total_produccion_anual=$prodtotal;
    try {
      $entrar->save();
      session()->flash('success','Configuracion Registrada');
    } catch (\Exception $e) {
      	 session()->flash('error','Ocurrio un error');
    }
  }
	 return Redirect::to('Configuracion/Costos_Directos');
}
public function show()
{
  return view('logistica.clasificacion.index');
}
public function edit($id)
{
}
public function update()
{

}
public function destroy()
{

}





}
