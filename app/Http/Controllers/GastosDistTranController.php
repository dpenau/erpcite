<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Serie;
use erpCite\Empresa;

use erpCite\GastosDistribucion;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class GastosDistTranController extends Controller
{
  public function __construct()
{
  $this->middleware('jefe');
}
public function index(Request $request)
{

if ($request) {
  $mes_actual=date("m");
    $distribucion=DB::table('gasto_distribucion')
    ->where('estado','=','1')->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->whereMonth('fecha_creacion', '=', $mes_actual)
    ->get();
    return view('costos.indirectos.GastosDistTran.index',["distribucion"=>$distribucion]);

  }

}
public function create(Request $request)
{
  if($request)
  {
    $area=DB::table('area')
    ->orderBy('descrip_area','asc')->get();
    return view("costos.indirectos.GastosDistTran.create",["area"=>$area]);
  }
}
public function store()
{
  $identificador=rand(100000,999999);
  $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
  $siglax = $sigla[0]->siglas;
  $res=$siglax.'-'.$identificador;
  $idempresa=Auth::user()->RUC_empresa;
  $articulo=new GastosDistribucion;
  $articulo->id_gastoDistr=$res;
  $articulo->fecha_creacion=Input::get('fecha_creacion');
  $articulo->area=Input::get('area');
  $articulo->gasto_mensual=Input::get('gasto_mensual');
  $articulo->descripcion=Input::get('descripcion');
  $articulo->estado=1;
  $articulo->RUC_empresa=$idempresa;
  $articulo->save();
  session()->flash('success','Gasto ingresado satisfactoriamente');
  return Redirect::to('costos_indirectos/GastosDistTran');



}
public function show()
{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
}
public function edit($id)
{
 /* return Redirect::to('logistica/clasificacion');*/
}
public function update($data)
  {


  }
public function destroy()
{
  $email=Input::get('email');
  $estado=Input::get('estado');
  $act=GastosDistribucion::where('id_gastoDistr',$email)
  ->delete();
    session()->flash('success','Gasto Eliminado');
  return Redirect::to('costos_indirectos/GastosDistTran');
}
}
