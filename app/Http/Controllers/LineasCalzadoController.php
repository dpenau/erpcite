<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\OrdenProduccion;
use erpCite\Linea;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class LineasCalzadoController extends Controller
{
  public function __construct()
{
  $this->middleware('desarrollo');
}
public function index(Request $request)
{
if ($request) {
    $Lineas=DB::table('linea')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    $coleccion=DB::table('coleccion')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    
 /*$linea=DB::table('orden_produccion')->join('cod_linea','orden_produccion.cod_linea','=','linea.cod_linea')->where('RUC_empresa',Auth::user()->RUC_empresa)->paginate(10);
*/

    return view('Produccion.lineas_calzado.index',["Lineas"=>$Lineas,"coleccion"=>$coleccion]);
  }
//return view('Produccion.orden_produccion.index');
}
public function create(Request $request)
{
  if($request)
  {

    return view("Produccion.lineas_calzado.create");
  }

}
public function store()
{

////
  DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    $LineaCalzado=new Linea;
    $identificador=rand(10000,99999);
    $empresa=Auth::user()->RUC_empresa;

    $LineaCalzado->nombre_linea=Input::get('nombreLinea');
    $LineaCalzado->cod_linea=$identificador;
    $LineaCalzado->RUC_empresa=$empresa;
    $LineaCalzado->estado_linea=1;
    $LineaCalzado->save();



session()->flash('success','Línea de calzado creada');
    return Redirect::to('Produccion/lineas_calzado');

}
public function show()
{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
}
public function edit($id)
{
 /* return Redirect::to('logistica/clasificacion');*/
}
public function update($data)
  {

    $clasificacion=Linea::findOrFail(Input::get('cod_linea_mod'));
    $clasificacion->nombre_linea=Input::get('nombre_linea_mod');

    $clasificacion->update();
    session()->flash('success','Linea Actualizada');
    return Redirect::to('Produccion/lineas_calzado');
  }
public function destroy()
{
  $email=Input::get('email');
  $estado=Input::get('estado');
  if($estado==0){$mensaje="Eliminada";}
  else{$mensaje="Acivado";}
  $act=linea::where('cod_linea',$email)
  ->update(['estado_linea'=>$estado]);
    session()->flash('success','Orden de Produccion '.$mensaje);
  return Redirect::to('Produccion/lineas_calzado');
}
}
