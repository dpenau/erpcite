<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\InportacionModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
class InportacionController extends Controller
{
  public function __construct()
{
  $this->middleware('logistica');
}
public function index($var)
{
  if ($var) {
    $n=explode('+',$var);
    $soles=DB::table('importaciones')
    ->where('id_detalle_oc','=',$n[0])
    ->where('tipo_gasto_moneda','=','0')
    ->get();
    $dolares=DB::table('importaciones')
    ->where('id_detalle_oc','=',$n[0])
    ->where('tipo_gasto_moneda','=','1')
    ->get();
    return view('logistica.importacion.index',['nombre'=>$n[1],'codigo_detalle'=>$n[0],'soles'=>$soles,'dolares'=>$dolares]);
  }
}
public function store()
{
  $cod=Input::Get('detalle');
  $nombre=Input::Get('descrip');
  $descripcion_Gasto=Input::get('descripcion');
  $moneda=Input::get('moneda');
  $importe=Input::Get('importe');
  $importacion=new InportacionModel;
  $importacion->codigo_importaciones=rand(1,999999);
  $importacion->id_detalle_oc=$cod;
  $importacion->tipo_gasto_moneda=$moneda;
  $importacion->descripcion_gasto=$descripcion_Gasto;
  $importacion->estado_gasto=1;
  $importacion->importe_gasto=$importe;
  $a=$importacion->save();
  session()->flash('success','Costo Agregado con exito');
  return Redirect::to('logistica/importacion/'.$cod."+".$nombre);
}
public function update()
{
  $cod=Input::Get('detalle');
  $nombre=Input::Get('descrip');
  $descripcion_Gasto=Input::get('descripcion');
  $moneda=Input::get('moneda');
  $importe=Input::Get('importe');
  $cod_importacion=Input::Get('codigo');
  $act=InportacionModel::where('codigo_importaciones',$cod_importacion)
  ->update(["tipo_gasto_moneda"=>$moneda,"descripcion_gasto"=>$descripcion_Gasto,"importe_gasto"=>$importe]);
  session()->flash('success','Costo Editado con exito');
  return Redirect::to('logistica/importacion/'.$cod."+".$nombre);
}
public function destroy()
{
  $cod=Input::Get('detalle');
  $nombre=Input::Get('descrip');
  $descripcion_Gasto=Input::get('descripcion');
  $estado=Input::get('estado');
  $cod_importacion=Input::Get('codigo');
  if($estado==1)
  {
    $mensaje="Activado";
  }
  else {
    $mensaje="Desactivado";
  }
  $act=InportacionModel::where('codigo_importaciones',$cod_importacion)
  ->update(["estado_gasto"=>$estado]);
  session()->flash('success','Costo '.$mensaje.' con exito');
  return Redirect::to('logistica/importacion/'.$cod."+".$nombre);
}
}
