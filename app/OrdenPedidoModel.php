<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenPedidoModel extends Model
{
  protected $table='orden_pedido';

  protected $primaryKey="codigo_pedido";

  protected $keyType = "string";

  public $timestamps=false;


  protected $fillable=['codigo_cliente','RUC_empresa','total_pedido','fecha','fecha_entrega','deuda','estado_orden_pedido'];

  protected $guarded=[];
}
