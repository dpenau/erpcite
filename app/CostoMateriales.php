<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class CostoMateriales extends Model
{
  protected $table='gasto_material';

protected $primaryKey="cod_matadmi";

public $timestamps=false;


protected $fillable=['area','tipo_gasto','descripcion','unidad_compra','valor_unitario','consumo','meses_duracion','gasto_mensual','estado','RUC_empresa'];

protected $guarded=[];
}
