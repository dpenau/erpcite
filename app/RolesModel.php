<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class RolesModel extends Model
{
    protected $table='roles';

    protected $primaryKey="id";
  
    public $timestamps=false;
  
  
    protected $fillable=['name', 'description','created_at','updated_at','estado_rol'];
  
    protected $guarded=[];
}
