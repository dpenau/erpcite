<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
  protected $table='orden_compra';

  protected $primaryKey="cod_orden_compra";

  public $timestamps=false;


  protected $fillable=['fecha_entrega','RUC_proveedor','costo_total_oc','comentario_oc','RUC_empresa','estado_orden_compra','tipo_moneda_orden','t_cambio'];

  protected $guarded=[];
}
