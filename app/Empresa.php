<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
  protected $table='empresa';

  protected $primaryKey="RUC_empresa";

  public $timestamps=false;


  protected $fillable=[
    'RUC_empresa',
    'nom_empresa',
    'razon_social',
    'representante_legal',
    'nombre_comercial',
    'domicilio',
    'correo',
    'telefono',
    'pagina_web',
    'imagen',
    'estado_empresa',
    'cod_tipo_contribuyente',
    'cod_regimen_laboral',
    'cod_regimen_renta',
    'siglas',
    'reparticion_utilidades',
    'tipo_rubro',
    'politica_desarrollo_id'];

  protected $guarded=[];
}
