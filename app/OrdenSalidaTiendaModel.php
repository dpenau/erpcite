<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenSalidaTiendaModel extends Model
{
    protected $table='orden_salida_tienda';

  protected $primaryKey="cod_orden_salida";

  protected $keyType="string";

  public $timestamps=false;

  protected $fillable=['fecha_creacion','observacion','area_dirigida','RUC_empresa','usuario_salida'];

  protected $guarded=[];
}
